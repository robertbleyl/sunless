package org.sunless;

import java.util.HashMap;
import java.util.Map;

import org.mockito.Mockito;
import org.sunless.server.gameplay.ServerPlayerFactory;
import org.sunless.server.gameplay.ServerPlayersContainer;
import org.sunless.server.network.ServerState;
import org.sunless.shared.entity.EntityAttacher;
import org.sunless.shared.entity.EntityFactory;
import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.json.Faction;
import org.sunless.shared.json.FactionConfig;
import org.sunless.shared.loading.EnqueueHelper;

import com.jme3.network.HostedConnection;

public class AbstractNetworkTest extends AbstractTest {

	protected GameInfo gameInfo;
	protected ServerPlayersContainer playerManager;
	protected ServerPlayerFactory playerFactory;
	protected EntityFactory entityFactory;
	protected ServerState serverState;
	protected TeamsContainer teamsContainer;
	protected EnqueueHelper enqueueHelper;
	protected EntityAttacher entityAttacher;
	protected Map<Faction, FactionConfig> factionConfigs;

	protected void setup() {
		gameInfo = new GameInfo();
		playerManager = Mockito.mock(ServerPlayersContainer.class);
		playerFactory = Mockito.mock(ServerPlayerFactory.class);
		entityFactory = Mockito.mock(EntityFactory.class);
		serverState = Mockito.mock(ServerState.class);
		teamsContainer = new TeamsContainer();
		enqueueHelper = Mockito.mock(EnqueueHelper.class);
		entityAttacher = Mockito.mock(EntityAttacher.class);
		factionConfigs = new HashMap<>(0);
	}

	protected HostedConnection getMockedHostedConnection() {
		return Mockito.mock(HostedConnection.class);
	}
}