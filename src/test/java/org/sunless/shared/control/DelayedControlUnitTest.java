package org.sunless.shared.control;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.sunless.shared.control.DelayedControl;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;

public class DelayedControlUnitTest {
	
	private DelayedControl control;
	
	@Before
	public void setup() {
		control = Mockito.spy(new DelayedControl(1000f) {
			@Override
			protected void controlRender(RenderManager arg0, ViewPort arg1) {
				
			}
			
			@Override
			protected void onUpdate(float elapsedTime) {
				
			}
		});
	}
	
	@Test
	public void shouldNotCallOnUpdate() {
		control.controlUpdate(1f);
		Mockito.verify(control, Mockito.never()).onUpdate(Mockito.anyFloat());
	}
	
	@Test
	public void shouldCallOnUpdate() {
		control.controlUpdate(2000f);
		Mockito.verify(control).onUpdate(2000f);
	}
}