package org.sunless.shared.gameplay.player;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.sunless.AbstractTest;
import org.sunless.shared.event.Event;
import org.sunless.shared.event.gameplay.PlayerPropertyChangeEvent;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.json.Faction;

import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public class PlayerUnitTest extends AbstractTest {

	private Player player;
	private Team team;
	private Team enemyTeam;
	private TeamsContainer teamsContainer;

	@Before
	public void setup() throws Exception {
		team = new Team((short)1, Faction.ALIENS, 16);
		enemyTeam = new Team((short)2, Faction.HUMANS, 16);
		teamsContainer = new TeamsContainer();
		teamsContainer.setTeam1(team);
		teamsContainer.setTeam2(enemyTeam);
		player = new Player("test", 1l, 1l, false, team.getTeamId(), teamsContainer);
	}

	@Test
	public void shouldCheckConstructorArguments() throws Exception {
		try {
			new Player(null, 0l, -1l, false, (short)0, teamsContainer);
			fail("IllegalArgumentException expected because name is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("name must not be blank!", e.getMessage());
		}

		try {
			new Player("", 0l, -1l, false, (short)0, teamsContainer);
			fail("IllegalArgumentException expected because name is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("name must not be blank!", e.getMessage());
		}

		try {
			new Player("test", -1l, -1l, false, (short)0, teamsContainer);
			fail("IllegalArgumentException expected because playerId is not higher than zero!");
		} catch (IllegalArgumentException e) {
			assertEquals("playerId must be higher than zero!", e.getMessage());
		}

		try {
			new Player("test", 0l, -1l, false, (short)0, teamsContainer);
			fail("IllegalArgumentException expected because playerId is not higher than zero!");
		} catch (IllegalArgumentException e) {
			assertEquals("playerId must be higher than zero!", e.getMessage());
		}

		try {
			new Player("test", 1l, -1l, false, (short)0, teamsContainer);
			fail("IllegalArgumentException expected because teamId is smaller than zero!");
		} catch (IllegalArgumentException e) {
			assertEquals("teamId must be higher than zero!", e.getMessage());
		}

		try {
			new Player("test", 1l, -1l, false, (short)1, teamsContainer);
			fail("IllegalArgumentException expected because init money is negative!");
		} catch (IllegalArgumentException e) {
			assertEquals("initMoney must not be negative!", e.getMessage());
		}

		Player player = new Player("test", 1l, 1l, false, (short)1, teamsContainer);
		assertEquals(1l, player.getMoney());
		assertEquals(0, player.getKills());
		assertEquals(0, player.getDeaths());
	}

	@Test
	public void shouldAddKill() {
		player.addKill();
		assertEquals(1, player.getKills());
	}

	@Test
	public void shouldAddDeath() {
		player.addDeath();
		assertEquals(1, player.getDeaths());
	}

	@Test
	public void shouldCheckMoneyArguments() {
		try {
			player.addMoney(-1l);
			fail("IllegalArgumentException expected because money to add is not higher than zero!");
		} catch (IllegalArgumentException e) {
			assertEquals("money to add must be higher than zero!", e.getMessage());
		}

		try {
			player.addMoney(0l);
			fail("IllegalArgumentException expected because money to add is not higher than zero!");
		} catch (IllegalArgumentException e) {
			assertEquals("money to add must be higher than zero!", e.getMessage());
		}
		try {
			player.removeMoney(-1l);
			fail("IllegalArgumentException expected because money to subtract is not higher than zero!");
		} catch (IllegalArgumentException e) {
			assertEquals("money to subtract must be higher than zero!", e.getMessage());
		}

		try {
			player.removeMoney(0l);
			fail("IllegalArgumentException expected because money to subtract is not higher than zero!");
		} catch (IllegalArgumentException e) {
			assertEquals("money to subtract must be higher than zero!", e.getMessage());
		}
	}

	@Test
	public void shouldAddMoney() {
		player.addMoney(5l);
		assertEquals(6l, player.getMoney());

		player.addMoney(1l);
		assertEquals(7l, player.getMoney());
	}

	@Test
	public void shouldRemoveMoney() {
		player.removeMoney(1l);
		assertEquals(0l, player.getMoney());

		player.removeMoney(1l);
		assertEquals(0l, player.getMoney());
	}

	@Test
	public void shouldCheckKeyArgument() {
		try {
			player.setProperty(null, null);
			fail("IllegalArgumentException expected because key is null!");
		} catch (IllegalArgumentException e) {
			assertEquals("the property name must not be blank!", e.getMessage());
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void shouldChangePropertyAndFireEvent() {
		player.setProperty("testkey", "testvalue");

		assertEquals("testvalue", player.properties.get("testkey"));

		ArgumentCaptor<Event> eventArg = ArgumentCaptor.forClass(Event.class);
		Mockito.verify(eventBus).fireEvent(eventArg.capture());

		PlayerPropertyChangeEvent event = (PlayerPropertyChangeEvent)eventArg.getValue();
		assertNotNull(event);
		assertNull(event.getOldPropertyValue());
		assertEquals("testkey", event.getPropertyName());
		assertEquals("testvalue", event.getPropertyValue());
		assertEquals(player, event.getPlayer());
	}

	@Test
	public void shouldDetachAllControls() {
		AbstractControl control1 = Mockito.mock(AbstractControl.class);
		player.addPlayerControl(control1);

		AbstractControl control2 = Mockito.mock(AbstractControl.class);
		player.addPlayerControl(control2);

		Spatial spatial = Mockito.mock(Spatial.class);
		Mockito.when(control2.getSpatial()).thenReturn(spatial);

		player.detachPlayerControls();

		Mockito.verify(control1).setEnabled(false);
		Mockito.verify(control2).setEnabled(false);

		Mockito.verify(spatial).removeControl(control2);

		assertEquals(0, player.playerControls.size());
	}

	@Test
	public void shouldCompareByKills() {
		Player player1 = new Player("player1", 1, 0, false, (short)1, teamsContainer);
		Player player2 = new Player("player2", 2, 0l, false, (short)1, teamsContainer);

		player1.addKill();

		int result = player1.compareTo(player2);
		assertEquals(-1, result);
	}

	@Test
	public void shouldCompareByDeaths() {
		Player player1 = new Player("player1", 1, 0, false, (short)1, teamsContainer);
		Player player2 = new Player("player2", 2, 0l, false, (short)1, teamsContainer);

		player1.addDeath();

		int result = player1.compareTo(player2);
		assertEquals(1, result);
	}

	@Test
	public void shouldBeEqual() {
		Player player1 = new Player("player1", 1, 0, false, (short)1, teamsContainer);
		Player player2 = new Player("player2", 2, 0l, false, (short)1, teamsContainer);

		int result = player1.compareTo(player2);
		assertEquals(0, result);
	}
}