package org.sunless.shared.util;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.sunless.shared.util.FormatHelper;

public class FormatHelperTest {
	
	@Test
	public void shouldReturnSecondsOnly() {
		String result = FormatHelper.formatTime(10f);
		assertEquals("00:10", result);
		
		result = FormatHelper.formatTime(59f);
		assertEquals("00:59", result);
	}
	
	@Test
	public void shouldReturnMinutesAndSeconds() {
		String result = FormatHelper.formatTime(60f);
		assertEquals("01:00", result);
		
		result = FormatHelper.formatTime(62f);
		assertEquals("01:02", result);
		
		result = FormatHelper.formatTime(119f);
		assertEquals("01:59", result);
		
		result = FormatHelper.formatTime(130f);
		assertEquals("02:10", result);
	}
	
	@Test
	public void shouldHandleMinutesOver10() {
		String result = FormatHelper.formatTime(610f);
		assertEquals("10:10", result);
	}
}