package org.sunless;

import org.junit.Before;
import org.mockito.Mockito;
import org.sunless.shared.event.EventBus;

public class AbstractTest {
	
	protected EventBus eventBus;
	
	@Before
	public void setupEventBus() {
		eventBus = Mockito.mock(EventBus.class);
		EventBus.init(eventBus);
	}
}