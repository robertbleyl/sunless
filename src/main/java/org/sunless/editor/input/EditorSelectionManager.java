package org.sunless.editor.input;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.sunless.editor.event.AddAsteroidFieldEvent;
import org.sunless.editor.event.AddAsteroidFieldEventHandler;
import org.sunless.editor.event.AddEditorObjectEvent;
import org.sunless.editor.event.AddEditorObjectEventHandler;
import org.sunless.editor.event.DeleteSelectionEvent;
import org.sunless.editor.event.DeleteSelectionEventHandler;
import org.sunless.editor.event.FinishAsteroidFieldEvent;
import org.sunless.editor.event.FinishAsteroidFieldEventHandler;
import org.sunless.editor.event.SelectionChangedEvent;
import org.sunless.editor.event.SelectionChangedEventHandler;
import org.sunless.editor.event.TransformSelectionEvent;
import org.sunless.editor.event.TransformSelectionEventHandler;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.EventHandlersContainer;
import org.sunless.shared.json.WorldObjectData;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.input.InputManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Line;

@Singleton
public class EditorSelectionManager {

	private static final float SELECTION_BOX_THRESHOLD = 0.5f;

	private final EventBus eventBus = EventBus.get();
	private final EventHandlersContainer eventHandlersContainer = new EventHandlersContainer();

	private final Map<Node, SelectionData> selectionQuadMap = new HashMap<>();

	private Node selectionBoxNode;
	private Line lineLeft;
	private Line lineTop;
	private Line lineRight;
	private Line lineBottom;
	private Material lineMat;

	private Material selectedObjectLineMat;
	private Material movementLineMaterialX;
	private Material movementLineMaterialY;
	private Material movementLineMaterialZ;

	private Vector3f currentTransformationAxis;
	private Node currentTransformationObject;

	private boolean select;
	private boolean keepSelection;
	private Vector2f selectionStartLocation;
	private boolean movementModeActive;
	private boolean rotationModeActive;
	private boolean scaleModeActive;

	private final InputManager inputManager;
	private final AssetManager assetManager;
	private final Camera camera;
	private final EditorObjectData objectData;

	private Node rootNode;
	private Node guiNode;

	@Inject
	public EditorSelectionManager(InputManager inputManager, AssetManager assetManager, Camera camera, EditorObjectData objectData) {
		this.inputManager = inputManager;
		this.assetManager = assetManager;
		this.camera = camera;
		this.objectData = objectData;
	}

	public void init(Node rootNode, Node guiNode) {
		this.rootNode = rootNode;
		this.guiNode = guiNode;
		final Vector3f lineStart = new Vector3f(1f, 1f, 1f);
		final Vector3f lineEnd = new Vector3f(2f, 1f, 1f);
		lineLeft = new Line(lineStart, lineEnd);
		lineTop = new Line(lineStart, lineEnd);
		lineRight = new Line(lineStart, lineEnd);
		lineBottom = new Line(lineStart, lineEnd);

		selectedObjectLineMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		selectedObjectLineMat.setColor("Color", ColorRGBA.Green);

		movementLineMaterialX = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		movementLineMaterialX.setColor("Color", ColorRGBA.Yellow);

		movementLineMaterialY = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		movementLineMaterialY.setColor("Color", ColorRGBA.Red);

		movementLineMaterialZ = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		movementLineMaterialZ.setColor("Color", ColorRGBA.Magenta);

		lineMat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		lineMat.setColor("Color", ColorRGBA.Blue);

		Geometry geomLineLeft = new Geometry("SelectionLineLeft", lineLeft);
		Geometry geomLineTop = new Geometry("SelectionLineLeft", lineTop);
		Geometry geomLineRight = new Geometry("SelectionLineLeft", lineRight);
		Geometry geomLineBottom = new Geometry("SelectionLineLeft", lineBottom);

		geomLineLeft.setMaterial(lineMat);
		geomLineTop.setMaterial(lineMat);
		geomLineRight.setMaterial(lineMat);
		geomLineBottom.setMaterial(lineMat);

		selectionBoxNode = new Node("SelectionBoxNode");
		selectionBoxNode.attachChild(geomLineLeft);
		selectionBoxNode.attachChild(geomLineTop);
		selectionBoxNode.attachChild(geomLineRight);
		selectionBoxNode.attachChild(geomLineBottom);

		eventHandlersContainer.addHandler(DeleteSelectionEvent.TYPE, new DeleteSelectionEventHandler() {
			@Override
			public void onDeleteSelection(DeleteSelectionEvent event) {
				deleteSelection();
			}
		});

		eventHandlersContainer.addHandler(TransformSelectionEvent.TYPE, new TransformSelectionEventHandler() {
			@Override
			public void onMoveSelection(TransformSelectionEvent event) {
				if (event.isMove()) {
					toggleMovementMode();
				}

				if (event.isRotate()) {
					toggleRotationMode();
				}

				if (event.isScale()) {
					toggleScaleMode();
				}
			}
		});

		eventHandlersContainer.addHandler(SelectionChangedEvent.TYPE, new SelectionChangedEventHandler() {
			@Override
			public void onSelectionChanged(SelectionChangedEvent event) {
				if (event.isFromHud()) {
					clearSelection();
					addSelection(event.getSelectedObjects().keySet().iterator().next());
				}
			}
		});

		eventHandlersContainer.addHandler(AddEditorObjectEvent.TYPE, new AddEditorObjectEventHandler() {
			@Override
			public void onAddEditorObject(AddEditorObjectEvent event) {
				addObject(event);
			}
		});

		eventHandlersContainer.addHandler(AddAsteroidFieldEvent.TYPE, new AddAsteroidFieldEventHandler() {
			@Override
			public void onAddAsteroidField(AddAsteroidFieldEvent event) {
				addAsteroidField();
			}
		});

		eventHandlersContainer.addHandler(FinishAsteroidFieldEvent.TYPE, new FinishAsteroidFieldEventHandler() {
			@Override
			public void onFinish(FinishAsteroidFieldEvent event) {
				finishAsteroidField();
			}
		});
	}

	private void addObject(AddEditorObjectEvent event) {
		try {
			WorldObjectData worldObject = event.getData();
			Node object = objectData.addObject(worldObject);

			clearSelection();
			addSelection(object);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void addAsteroidField() {
		Node asteroidFieldNode = objectData.addAsteroidField();

		clearSelection();
		addSelection(asteroidFieldNode);
	}

	private void finishAsteroidField() {
		clearSelection();
		objectData.finishAsteroidField();
	}

	public void clearSelection() {
		for (SelectionData data : selectionQuadMap.values()) {
			data.getSelectionBoxNode().removeFromParent();
			data.getTransformationLinesNode().removeFromParent();
		}

		selectionQuadMap.clear();

		eventBus.fireEvent(new SelectionChangedEvent(null, false));
	}

	public void drawSelectionBox() {
		if (select && selectionStartLocation != null) {
			Vector2f cursorPosition = inputManager.getCursorPosition().clone();

			if (cursorPosition.distance(selectionStartLocation) >= SELECTION_BOX_THRESHOLD) {
				guiNode.attachChild(selectionBoxNode);

				lineLeft.updatePoints(new Vector3f(selectionStartLocation.x, selectionStartLocation.y, 0f), new Vector3f(selectionStartLocation.x, cursorPosition.y, 0f));
				lineTop.updatePoints(new Vector3f(selectionStartLocation.x, selectionStartLocation.y, 0f), new Vector3f(cursorPosition.x, selectionStartLocation.y, 0f));
				lineRight.updatePoints(new Vector3f(cursorPosition.x, selectionStartLocation.y, 0f), new Vector3f(cursorPosition.x, cursorPosition.y, 0f));
				lineBottom.updatePoints(new Vector3f(selectionStartLocation.x, cursorPosition.y, 0f), new Vector3f(cursorPosition.x, cursorPosition.y, 0f));
			}
		}
	}

	public void selectFinished() {
		if (selectionStartLocation != null) {
			Vector2f cursorPosition = inputManager.getCursorPosition().clone();

			if (cursorPosition.distance(selectionStartLocation) < SELECTION_BOX_THRESHOLD) {
				Vector3f origin = camera.getLocation();
				Vector3f pos = camera.getWorldCoordinates(cursorPosition, 0f);
				Vector3f direction = pos.subtract(origin);
				Ray ray = new Ray(origin, direction);

				CollisionResults results = new CollisionResults();
				rootNode.collideWith(ray, results);

				for (CollisionResult collisionResult : results) {
					if (!collisionResult.getGeometry().getName().contains("Sky")) {
						addSelection(collisionResult.getGeometry().getParent().getParent().getParent());
						break;
					}
				}
			}
		}

		guiNode.detachChild(selectionBoxNode);
		selectionStartLocation = null;
	}

	public void deleteSelection() {
		for (Node spatial : selectionQuadMap.keySet()) {
			spatial.removeFromParent();

			objectData.deleteFromMap(spatial);
		}

		clearSelection();
	}

	public void checkTransformationLinePick() {
		Vector2f cursorPosition = inputManager.getCursorPosition().clone();
		Vector3f origin = camera.getLocation();
		Vector3f pos = camera.getWorldCoordinates(cursorPosition, 0f);
		Vector3f direction = pos.subtract(origin);
		Ray ray = new Ray(origin, direction);

		CollisionResults results = new CollisionResults();
		rootNode.collideWith(ray, results);

		for (CollisionResult collisionResult : results) {
			if (collisionResult.getGeometry().getName().contains("Transformation line")) {
				Geometry geom = collisionResult.getGeometry();
				currentTransformationAxis = geom.getUserData("DIRECTION");
				currentTransformationObject = getNewTransformationObject(geom);
				break;
			}
		}
	}

	public void transformObject(Vector2f mouseDir) {
		if (currentTransformationObject != null && currentTransformationAxis != null) {
			Vector3f objPos3d = camera.getScreenCoordinates(currentTransformationObject.getLocalTranslation());
			Vector2f objPos2d = new Vector2f(objPos3d.x, objPos3d.y);

			Vector3f worldPos = currentTransformationObject.getLocalTranslation().add(currentTransformationAxis);
			Vector3f screenCoordinates = camera.getScreenCoordinates(worldPos);
			Vector2f point = new Vector2f(screenCoordinates.x, screenCoordinates.y);

			Vector2f screenDir = point.subtract(objPos2d);
			float angle = FastMath.abs(screenDir.normalize().angleBetween(mouseDir));
			if (angle > FastMath.PI) {
				angle = FastMath.TWO_PI - angle;
			}

			float amount = FastMath.HALF_PI - angle;

			if (movementModeActive) {
				Vector3f newLocation = currentTransformationObject.getLocalTranslation().add(currentTransformationAxis.mult(amount));
				currentTransformationObject.setLocalTranslation(newLocation);

				SelectionData data = selectionQuadMap.get(currentTransformationObject);
				data.getTransformationLinesNode().setLocalTranslation(newLocation);
			} else if (rotationModeActive) {
				Quaternion newRotation = currentTransformationObject.getLocalRotation().mult(Quaternion.IDENTITY.fromAngleAxis(amount / 30f, currentTransformationAxis));
				currentTransformationObject.setLocalRotation(newRotation);
			} else if (scaleModeActive) {
				amount /= 20f;
				Vector3f newScale = currentTransformationObject.getLocalScale().add(currentTransformationAxis.mult(amount));
				currentTransformationObject.setLocalScale(newScale);

				SelectionData data = selectionQuadMap.get(currentTransformationObject);
				Vector3f newLinesScale = data.getTransformationLinesNode().getLocalScale().add(currentTransformationAxis.mult(amount));
				data.getTransformationLinesNode().setLocalScale(newLinesScale);
			}
		}
	}

	public void drawSelectedObjectsBoxes() {
		if (selectionStartLocation != null) {
			Vector2f selectionEndLocation = inputManager.getCursorPosition().clone();

			if (selectionEndLocation.distance(selectionStartLocation) > SELECTION_BOX_THRESHOLD) {
				final float xLeft = selectionStartLocation.x;
				final float yLeft = selectionStartLocation.y;
				final float xRight = selectionEndLocation.x;
				final float yRight = selectionEndLocation.y;

				for (Spatial c : rootNode.getChildren()) {
					if ((c.getName().equals(EditorObjectData.ASTEROID_FIELD_NODE) || c.getLastFrustumIntersection() == Camera.FrustumIntersect.Inside) && c instanceof Node && c.getWorldBound() != null && !objectData.isUnfinishedAsteroid(c) && !selectionQuadMap.containsKey(c)) {
						Vector3f p = camera.getScreenCoordinates(c.getLocalTranslation());

						if (p.x >= xLeft && p.y <= yLeft && p.x <= xRight && p.y >= yRight) {
							addSelection((Node)c);
						}
					}
				}
			}
		}

		for (SelectionData data : selectionQuadMap.values()) {
			data.update(camera);
		}
	}

	private void addSelection(Node c) {
		Node linesNode = new Node("Selected Object Lines Node for " + c.getName());
		guiNode.attachChild(linesNode);

		Node movementLineSnode = new Node("Movement lines for " + c.getName());

		SelectionData data = new SelectionData(c, linesNode, movementLineSnode, selectedObjectLineMat, movementLineMaterialX, movementLineMaterialY, movementLineMaterialZ);

		selectionQuadMap.put(c, data);

		Map<Node, WorldObjectData> selectionMap = new HashMap<>(selectionQuadMap.size());

		for (Node node : selectionQuadMap.keySet()) {
			selectionMap.put(node, objectData.getWorldObjectData(node));
		}

		eventBus.fireEvent(new SelectionChangedEvent(selectionMap, false));
	}

	public void selectionStarted() {
		selectionStartLocation = inputManager.getCursorPosition().clone();

		if (!keepSelection) {
			clearSelection();
		}
	}

	public void updateTransformationLines(boolean show) {
		for (Entry<Node, SelectionData> entry : selectionQuadMap.entrySet()) {
			Node node = entry.getKey();
			SelectionData data = entry.getValue();
			Node movementLinesNode = data.getTransformationLinesNode();

			if (show) {
				movementLinesNode.setLocalTranslation(node.getLocalTranslation());
				rootNode.attachChild(movementLinesNode);
			} else {
				rootNode.detachChild(movementLinesNode);
			}
		}
	}

	private Node getNewTransformationObject(Geometry geom) {
		for (Entry<Node, SelectionData> entry : selectionQuadMap.entrySet()) {
			Node node = entry.getKey();
			SelectionData data = entry.getValue();
			Node transformationLinesNode = data.getTransformationLinesNode();

			if (transformationLinesNode == geom.getParent()) {
				return node;
			}
		}

		return null;
	}

	public void handleLeftClick(boolean isPressed) {
		if (movementModeActive || rotationModeActive || scaleModeActive) {
			if (isPressed) {
				if (currentTransformationAxis == null) {
					checkTransformationLinePick();
				}
			} else {
				currentTransformationAxis = null;
				currentTransformationObject = null;
			}
		} else {
			if (!select && isPressed) {
				selectionStarted();
			}

			if (select && !isPressed) {
				selectFinished();
			}

			select = isPressed;
		}
	}

	public void toggleMovementMode() {
		movementModeActive = !movementModeActive;
		updateTransformationLines(movementModeActive);
	}

	public void toggleRotationMode() {
		rotationModeActive = !rotationModeActive;
		updateTransformationLines(rotationModeActive);
	}

	public void toggleScaleMode() {
		scaleModeActive = !scaleModeActive;
		updateTransformationLines(scaleModeActive);
	}

	public Vector3f getNewCameraCenter() {
		if (selectionQuadMap.isEmpty()) {
			return new Vector3f();
		} else if (selectionQuadMap.size() == 1) {
			return selectionQuadMap.keySet().iterator().next().getLocalTranslation().clone();
		}

		return null;
	}

	public void saveMap() {
		objectData.saveMap();
	}

	public void setKeepSelecting(boolean keepSelection) {
		this.keepSelection = keepSelection;
	}

	public void cleanup() {
		eventHandlersContainer.cleanup();
		objectData.cleanup();
	}
}