package org.sunless.editor.event;

import org.sunless.shared.event.EventHandler;

public interface AddEditorObjectEventHandler extends EventHandler {

	void onAddEditorObject(AddEditorObjectEvent event);
}