package org.sunless.editor.event;

import org.sunless.shared.event.EventHandler;

public interface TransformSelectionEventHandler extends EventHandler {

	void onMoveSelection(TransformSelectionEvent event);
}