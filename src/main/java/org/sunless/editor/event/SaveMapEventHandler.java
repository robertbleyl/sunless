package org.sunless.editor.event;

import org.sunless.shared.event.EventHandler;

public interface SaveMapEventHandler extends EventHandler {

	void onSaveMap(SaveMapEvent event);
}