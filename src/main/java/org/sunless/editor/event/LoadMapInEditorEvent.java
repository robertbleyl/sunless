package org.sunless.editor.event;

import java.io.File;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class LoadMapInEditorEvent extends Event<LoadMapInEditorEventHandler> {

	public static final EventType<LoadMapInEditorEventHandler> TYPE = new EventType<>();

	private final File mapFolder;

	public LoadMapInEditorEvent(File mapFolder) {
		this.mapFolder = mapFolder;
	}

	@Override
	public EventType<LoadMapInEditorEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(LoadMapInEditorEventHandler handler) {
		handler.onLoad(this);
	}

	public File getMapFolder() {
		return mapFolder;
	}
}