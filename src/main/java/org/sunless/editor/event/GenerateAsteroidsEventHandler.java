package org.sunless.editor.event;

import org.sunless.shared.event.EventHandler;

public interface GenerateAsteroidsEventHandler extends EventHandler {

	void onGenerate(GenerateAsteroidsEvent event);
}