package org.sunless.editor.event;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class DeleteSelectionEvent extends Event<DeleteSelectionEventHandler> {

	public static final EventType<DeleteSelectionEventHandler> TYPE = new EventType<>();

	@Override
	public EventType<DeleteSelectionEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(DeleteSelectionEventHandler handler) {
		handler.onDeleteSelection(this);
	}
}