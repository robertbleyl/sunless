package org.sunless.editor.event;

import org.sunless.shared.event.EventHandler;

public interface LoadMapInEditorEventHandler extends EventHandler {

	void onLoad(LoadMapInEditorEvent event);
}