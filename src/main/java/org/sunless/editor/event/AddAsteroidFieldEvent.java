package org.sunless.editor.event;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class AddAsteroidFieldEvent extends Event<AddAsteroidFieldEventHandler> {

	public static final EventType<AddAsteroidFieldEventHandler> TYPE = new EventType<>();

	@Override
	public EventType<AddAsteroidFieldEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(AddAsteroidFieldEventHandler handler) {
		handler.onAddAsteroidField(this);
	}
}