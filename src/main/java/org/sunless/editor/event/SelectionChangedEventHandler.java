package org.sunless.editor.event;

import org.sunless.shared.event.EventHandler;

public interface SelectionChangedEventHandler extends EventHandler {

	void onSelectionChanged(SelectionChangedEvent event);
}