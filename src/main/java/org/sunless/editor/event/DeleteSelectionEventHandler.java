package org.sunless.editor.event;

import org.sunless.shared.event.EventHandler;

public interface DeleteSelectionEventHandler extends EventHandler {

	void onDeleteSelection(DeleteSelectionEvent event);
}