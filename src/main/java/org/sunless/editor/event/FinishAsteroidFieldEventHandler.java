package org.sunless.editor.event;

import org.sunless.shared.event.EventHandler;

public interface FinishAsteroidFieldEventHandler extends EventHandler {

	void onFinish(FinishAsteroidFieldEvent event);
}