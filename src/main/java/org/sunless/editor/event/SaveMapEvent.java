package org.sunless.editor.event;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class SaveMapEvent extends Event<SaveMapEventHandler> {

	public static final EventType<SaveMapEventHandler> TYPE = new EventType<>();

	@Override
	public EventType<SaveMapEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(SaveMapEventHandler handler) {
		handler.onSaveMap(this);
	}
}