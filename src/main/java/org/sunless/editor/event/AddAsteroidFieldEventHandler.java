package org.sunless.editor.event;

import org.sunless.shared.event.EventHandler;

public interface AddAsteroidFieldEventHandler extends EventHandler {

	void onAddAsteroidField(AddAsteroidFieldEvent event);
}