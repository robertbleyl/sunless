package org.sunless.editor.event;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class FinishAsteroidFieldEvent extends Event<FinishAsteroidFieldEventHandler> {

	public static final EventType<FinishAsteroidFieldEventHandler> TYPE = new EventType<>();

	@Override
	public EventType<FinishAsteroidFieldEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(FinishAsteroidFieldEventHandler handler) {
		handler.onFinish(this);
	}
}