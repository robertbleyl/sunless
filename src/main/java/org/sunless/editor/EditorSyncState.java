package org.sunless.editor;

import org.sunless.shared.loading.EnqueueHelper;
import org.sunless.shared.network.sync.AbstractSyncState;

import com.google.inject.Inject;

public class EditorSyncState extends AbstractSyncState {

	@Inject
	public EditorSyncState(EnqueueHelper enqueueHelper) {
		super(enqueueHelper, 0);
	}

	@Override
	protected void onUpdate(float tpf) {

	}
}