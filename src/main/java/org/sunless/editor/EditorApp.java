package org.sunless.editor;

import org.sunless.shared.AbstractGameApplication;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;
import com.jme3.app.FlyCamAppState;
import com.jme3.system.AppSettings;
import com.jme3.system.JmeContext.Type;

public class EditorApp extends AbstractGameApplication {

	// private static final Logger log = LoggerFactory.getLogger(ClientApp.class);

	private Editor editor;

	public static void main(String[] args) {
		try {
			EditorApp app = new EditorApp();
			app.start(Type.Display);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public EditorApp() {
		super();

		setShowSettings(false);

		AppSettings settings = new AppSettings(true);

		settings.setWidth(1024);
		settings.setHeight(768);

		settings.setFullscreen(false);
		settings.setBitsPerPixel(16);
		settings.setFrequency(30);
		setSettings(settings);

		setDisplayStatView(true);
	}

	@Override
	public void simpleInitApp() {
		try {
			Injector injector = Guice.createInjector(Stage.PRODUCTION, new EditorModule(this));

			editor = injector.getInstance(Editor.class);
			stateManager.detach(stateManager.getState(FlyCamAppState.class));

			editor.init();
		} catch (Exception e) {
			e.printStackTrace();
			stop();
		}
	}
}