package org.sunless.editor.entity;

import org.sunless.shared.entity.EntityAttacher;
import org.sunless.shared.gameplay.IngameState;
import org.sunless.shared.gameplay.PhysicsWrapper;
import org.sunless.shared.network.sync.AbstractSyncState;

import com.google.inject.Inject;
import com.jme3.scene.Spatial;

public class EditorEntityAttacher extends EntityAttacher {

	@Inject
	public EditorEntityAttacher(IngameState ingameState, AbstractSyncState syncState, PhysicsWrapper physicsWrapper) {
		super(ingameState, syncState, physicsWrapper);
	}

	@Override
	public void attachEntity(Spatial entity) {
		entity.removeFromParent();
		super.attachEntity(entity);
	}
}