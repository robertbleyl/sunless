package org.sunless.editor.entity;

import org.sunless.shared.entity.EntityCreationResult;
import org.sunless.shared.entity.EntityFactoryImpl;

import com.google.inject.Singleton;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

@Singleton
public class EditorEntityFactory extends EntityFactoryImpl {

	@Override
	protected void initPhysicsForEntity(String fileName, EntityCreationResult result, Node entity) {

	}

	@Override
	protected void updatePhysicsForEntity(Vector3f location, Quaternion rotation, Vector3f scale, Node entity) {

	}
}