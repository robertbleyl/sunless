package org.sunless.editor.entity;

import org.sunless.shared.entity.EntityIdProvider;

public class EditorEntityIdProvider implements EntityIdProvider {

	@Override
	public long getNextId() {
		return -1L;
	}
}