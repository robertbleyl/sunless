package org.sunless.editor.ui.hud;

import org.sunless.shared.json.MapData;
import org.sunless.shared.ui.GUIState;

public interface EditorHudState extends GUIState {

	void setMapData(MapData mapData);
}