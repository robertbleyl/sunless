package org.sunless.editor.ui.hud;

import org.sunless.shared.ui.EmptyContainer;
import org.sunless.shared.ui.SimpleTextField;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.text.Label;
import tonegod.gui.core.ElementManager;

public abstract class EditorVectorPanel extends EmptyContainer {

	private final Vector3f vector;
	private final String labelPrefix;

	private SimpleTextField txtX;
	private SimpleTextField txtY;
	private SimpleTextField txtZ;

	public EditorVectorPanel(ElementManager screen, Vector2f dimensions, Vector3f vector, String labelPrefix) {
		super(screen);
		this.vector = vector;
		this.labelPrefix = labelPrefix;

		setDimensions(dimensions);

		init();
	}

	protected abstract void onApply(Vector3f newVector);

	private void init() {
		final Vector2f dimensions = new Vector2f(getWidth() / 2f, getHeight() / 4f);

		Vector2f pos = new Vector2f(0f, 0f);

		ButtonAdapter btnApply = new ButtonAdapter(screen, pos, dimensions) {
			@Override
			public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
				apply();
			}
		};
		btnApply.setText("Apply");
		addChild(btnApply);

		pos.y += dimensions.y;

		Label lblLeft = new Label(screen, pos, dimensions);
		lblLeft.setText(labelPrefix + " X:");
		addChild(lblLeft);

		pos.x += dimensions.x;

		txtX = new SimpleTextField(screen, pos, dimensions);
		txtX.setText(vector.x + "");
		addChild(txtX);

		pos.x = 0f;
		pos.y += dimensions.y;

		lblLeft = new Label(screen, pos, dimensions);
		lblLeft.setText(labelPrefix + " Y:");
		addChild(lblLeft);

		pos.x += dimensions.x;

		txtY = new SimpleTextField(screen, pos, dimensions);
		txtY.setText(vector.y + "");
		addChild(txtY);

		pos.x = 0f;
		pos.y += dimensions.y;

		lblLeft = new Label(screen, pos, dimensions);
		lblLeft.setText(labelPrefix + " Z:");
		addChild(lblLeft);

		pos.x += dimensions.x;

		txtZ = new SimpleTextField(screen, pos, dimensions);
		txtZ.setText(vector.z + "");
		addChild(txtZ);
	}

	private void apply() {
		try {
			float x = Float.valueOf(txtX.getText());
			float y = Float.valueOf(txtY.getText());
			float z = Float.valueOf(txtZ.getText());

			Vector3f newVector = new Vector3f(x, y, z);
			onApply(newVector);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}