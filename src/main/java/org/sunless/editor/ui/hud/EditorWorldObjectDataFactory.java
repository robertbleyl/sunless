package org.sunless.editor.ui.hud;

import org.sunless.shared.json.WorldObjectData;

public interface EditorWorldObjectDataFactory {

	WorldObjectData create();
}