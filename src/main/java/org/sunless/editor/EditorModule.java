package org.sunless.editor;

import org.sunless.editor.entity.EditorEntityAttacher;
import org.sunless.editor.entity.EditorEntityFactory;
import org.sunless.editor.entity.EditorEntityIdProvider;
import org.sunless.editor.loading.EditorLoadingStepBuilder;
import org.sunless.editor.ui.hud.EditorHudState;
import org.sunless.editor.ui.hud.EditorHudStateImpl;
import org.sunless.editor.ui.mapselection.EditorMapSelectionState;
import org.sunless.editor.ui.mapselection.EditorMapSelectionStateImpl;
import org.sunless.shared.AbstractGameApplication;
import org.sunless.shared.SharedModule;
import org.sunless.shared.entity.EntityAttacher;
import org.sunless.shared.entity.EntityFactory;
import org.sunless.shared.entity.EntityIdProvider;
import org.sunless.shared.gameplay.IngameState;
import org.sunless.shared.gameplay.IngameStateImpl;
import org.sunless.shared.gameplay.PhysicsWrapper;
import org.sunless.shared.gameplay.PhysicsWrapperImpl;
import org.sunless.shared.loading.LoadingStepsBuilder;
import org.sunless.shared.network.sync.AbstractSyncState;

public class EditorModule extends SharedModule {

	public EditorModule(AbstractGameApplication app) throws Exception {
		super(app);
	}

	@Override
	protected void configure() {
		super.configure();
		bind(EntityFactory.class).to(EditorEntityFactory.class);
		bind(EntityIdProvider.class).to(EditorEntityIdProvider.class);
		bind(IngameState.class).to(IngameStateImpl.class);
		bind(PhysicsWrapper.class).to(PhysicsWrapperImpl.class);
		bind(AbstractSyncState.class).to(EditorSyncState.class);
		bind(EntityAttacher.class).to(EditorEntityAttacher.class);
		bind(LoadingStepsBuilder.class).to(EditorLoadingStepBuilder.class);

		bind(EditorHudState.class).to(EditorHudStateImpl.class);
		bind(EditorMapSelectionState.class).to(EditorMapSelectionStateImpl.class);
	}
}