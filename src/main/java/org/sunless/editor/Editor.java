package org.sunless.editor;

import java.io.File;
import java.util.List;

import org.sunless.editor.event.LoadMapInEditorEvent;
import org.sunless.editor.event.LoadMapInEditorEventHandler;
import org.sunless.editor.input.EditorInputHandler;
import org.sunless.editor.input.EditorObjectData;
import org.sunless.editor.input.EditorSelectionManager;
import org.sunless.editor.loading.EditorLoadingStepBuilder;
import org.sunless.editor.ui.hud.EditorHudState;
import org.sunless.editor.ui.mapselection.EditorMapSelectionState;
import org.sunless.shared.NodeConstants;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.gameplay.IngameState;
import org.sunless.shared.gameplay.PhysicsWrapper;
import org.sunless.shared.gameplay.PhysicsWrapperImpl;
import org.sunless.shared.json.JsonHelper;
import org.sunless.shared.json.MapData;
import org.sunless.shared.loading.EnqueueHelper;
import org.sunless.shared.loading.LoadingState;
import org.sunless.shared.loading.LoadingStep;
import org.sunless.shared.loading.LoadingStepBuildRequest;
import org.sunless.shared.loading.LoadingThread;
import org.sunless.shared.state.fps.FPSState;
import org.sunless.shared.util.FileConstants;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.jme3.app.state.AppStateManager;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.BulletAppState.ThreadingType;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;

import tonegod.gui.core.Screen;

public class Editor {

	// private final Logger log = LoggerFactory.getLogger(getClass());

	private final EventBus eventBus = EventBus.get();

	private EnqueueHelper enqueueHelper;
	private Screen screen;
	private EditorLoadingStepBuilder loadingStepBuilder;
	private AppStateManager stateManager;
	private EditorObjectData editorObjectData;
	private EditorSelectionManager editorSelectionManager;
	private EditorInputHandler editorInputHandler;
	private Node appGuiNode;

	private EditorMapSelectionState editorMapSelectionState;
	private FPSState fpsState;
	private IngameState ingameState;
	private EditorHudState editorHudState;

	private LoadingState loadingState;
	private LoadingThread loadingThread;

	private BulletAppState bulletAppState;

	public void init() throws Exception {
		stateManager.attach(editorMapSelectionState);
		editorMapSelectionState.setEnabled(true);

		stateManager.attach(fpsState);
		fpsState.setEnabled(true);

		stateManager.attach(ingameState);
		ingameState.setEnabled(false);

		appGuiNode.addControl(screen);

		bulletAppState = new BulletAppState();
		bulletAppState.setThreadingType(ThreadingType.PARALLEL);
		stateManager.attach(bulletAppState);
		bulletAppState.setEnabled(false);
		bulletAppState.setDebugEnabled(true);

		PhysicsSpace physicsSpace = bulletAppState.getPhysicsSpace();
		physicsSpace.setGravity(new Vector3f());

		PhysicsWrapper physicsWrapper = new PhysicsWrapperImpl();
		physicsWrapper.setPhysicsSpace(physicsSpace);

		initEventHandlers();
	}

	protected void loadMap(File mapFolder) throws Exception {
		stateManager.attach(loadingState);

		final String mapName = mapFolder.getName();
		String completeMapPathName = FileConstants.MAPS_PATH_COMPLETE + mapName + FileConstants.MAP_DATA_FILE;
		MapData mapData = JsonHelper.get().toPOJO(completeMapPathName, MapData.class);

		LoadingStepBuildRequest request = new LoadingStepBuildRequest();
		request.setMapData(mapData);

		List<LoadingStep> steps = loadingStepBuilder.createLoadingSteps(request);

		steps.add(new LoadingStep(1f, true) {
			@Override
			public void load() throws Exception {
				ingameState.setEnabled(true);
				loadingState.setEnabled(false);
				bulletAppState.setEnabled(true);

				stateManager.attach(editorHudState);
				editorHudState.setEnabled(true);
				editorHudState.setMapData(mapData);

				Node ingameRootNode = ingameState.getRootNode();
				editorObjectData.init(mapData, ingameRootNode, mapName);
				editorSelectionManager.init(ingameRootNode, appGuiNode);

				ingameRootNode.addControl(editorInputHandler);
			}
		});

		loadingThread = new LoadingThread(steps, enqueueHelper, loadingState);

		editorMapSelectionState.setEnabled(false);
		loadingState.setEnabled(true);
		loadingThread.start();
	}

	protected void initEventHandlers() {
		eventBus.addHandler(LoadMapInEditorEvent.TYPE, new LoadMapInEditorEventHandler() {
			@Override
			public void onLoad(LoadMapInEditorEvent event) {
				try {
					loadMap(event.getMapFolder());
				} catch (Exception e) {
					// TODO
					e.printStackTrace();
				}
			}
		});
	}

	@Inject
	public void setEnqueueHelper(EnqueueHelper enqueueHelper) {
		this.enqueueHelper = enqueueHelper;
	}

	@Inject
	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	@Inject
	public void setLoadingStepBuilder(EditorLoadingStepBuilder loadingStepBuilder) {
		this.loadingStepBuilder = loadingStepBuilder;
	}

	@Inject
	public void setStateManager(AppStateManager stateManager) {
		this.stateManager = stateManager;
	}

	@Inject
	public void setAppGuiNode(@Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		this.appGuiNode = appGuiNode;
	}

	@Inject
	public void setEditorMapSelectionState(EditorMapSelectionState editorMapSelectionState) {
		this.editorMapSelectionState = editorMapSelectionState;
	}

	@Inject
	public void setFpsState(FPSState fpsState) {
		this.fpsState = fpsState;
	}

	@Inject
	public void setIngameState(IngameState ingameState) {
		this.ingameState = ingameState;
	}

	@Inject
	public void setEditorHudState(EditorHudState editorHudState) {
		this.editorHudState = editorHudState;
	}

	@Inject
	public void setLoadingState(LoadingState loadingState) {
		this.loadingState = loadingState;
	}

	@Inject
	public void setEditorObjectData(EditorObjectData editorObjectData) {
		this.editorObjectData = editorObjectData;
	}

	@Inject
	public void setEditorSelectionManager(EditorSelectionManager editorSelectionManager) {
		this.editorSelectionManager = editorSelectionManager;
	}

	@Inject
	public void setEditorInputHandler(EditorInputHandler editorInputHandler) {
		this.editorInputHandler = editorInputHandler;
	}
}