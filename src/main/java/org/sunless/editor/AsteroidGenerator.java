package org.sunless.editor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.sunless.editor.event.GenerateAsteroidsEvent;
import org.sunless.shared.json.Position;
import org.sunless.shared.json.Rotation;
import org.sunless.shared.json.WorldObjectData;

import com.jme3.bounding.BoundingBox;
import com.jme3.bounding.BoundingVolume;
import com.jme3.math.Vector3f;

public class AsteroidGenerator {

	private final Random r = new Random();

	private final List<BoundingVolume> existing = new ArrayList<>();

	private final int asteroidModelCount;
	private final Vector3f center;
	private final Vector3f size;
	private final int count;
	private final int minSize;
	private final int diffSize;

	public AsteroidGenerator(int asteroidModelCount, Vector3f center, Vector3f size, GenerateAsteroidsEvent event) {
		this.asteroidModelCount = asteroidModelCount;
		this.center = center;
		this.size = size;
		this.count = event.getCount();
		this.minSize = event.getMinSize();
		this.diffSize = event.getMaxSize() - minSize;
	}

	public List<WorldObjectData> generate() {
		List<WorldObjectData> objects = new ArrayList<>(count);

		for (int i = 0; i < count; i++) {
			WorldObjectData object = generateAsteroid();

			if (object != null) {
				objects.add(object);
			}
		}

		return objects;
	}

	private WorldObjectData generateAsteroid() {
		int iterations = 0;

		while (true) {
			if (iterations > 200) {
				break;
			}

			iterations++;
			float x = center.x + next((int)size.x);
			float y = center.y + next((int)size.y);
			float z = center.z + next((int)size.z);

			Vector3f location = new Vector3f(x, y, z);
			Vector3f scale = createScale();

			BoundingVolume vol = new BoundingBox(location, scale.x * 2f, scale.y * 2f, scale.z * 2f);

			boolean exists = false;

			for (BoundingVolume v : existing) {
				if (v.intersects(vol)) {
					exists = true;
					break;
				}
			}

			if (!exists) {
				existing.add(vol);

				Rotation rotation = createRotation();

				WorldObjectData object = new WorldObjectData();
				object.setName("asteroid" + (r.nextInt(asteroidModelCount) + 1));
				object.setLocation(new Position(x, y, z));
				object.setScale(new Position(scale.x, scale.y, scale.z));
				object.setRotation(rotation);

				return object;
			}
		}

		return null;
	}

	private float next(float bound) {
		if (bound < 0) {
			bound *= -1;
		}

		int next = r.nextInt((int)bound);

		if (r.nextBoolean()) {
			next = -next;
		}

		return next;
	}

	private Rotation createRotation() {
		float x = r.nextFloat();
		float y = r.nextFloat();
		float z = r.nextFloat();
		float w = r.nextFloat();

		Rotation rotation = new Rotation(x, y, z, w);
		return rotation;
	}

	private Vector3f createScale() {
		float s = minSize + r.nextInt(diffSize);
		float deviation = 0.1f;

		float x = s + ((r.nextBoolean() ? -deviation : deviation) * s);
		float y = s + ((r.nextBoolean() ? -deviation : deviation) * s);
		float z = s + ((r.nextBoolean() ? -deviation : deviation) * s);

		Vector3f scale = new Vector3f(x, y, z);
		return scale;
	}
}