package org.sunless.shared.state.fps;

import org.sunless.shared.NodeConstants;
import org.sunless.shared.ui.GUIStateImpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;

import tonegod.gui.controls.text.Label;
import tonegod.gui.core.Screen;

@Singleton
public class FPSStateImpl extends GUIStateImpl implements FPSState {

	private Label label;

	private float timer;

	@Inject
	public FPSStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(screen, appRootNode, appGuiNode);

		init();
	}

	@Override
	public void init() {
		Vector2f dimensions = new Vector2f(screen.getWidth() / 15, screen.getHeight() / 40);

		float x = screen.getWidth() - dimensions.getX();
		float y = dimensions.getY();

		Vector2f position = new Vector2f(x, y);
		label = new Label(screen, position, dimensions);
		label.setFontColor(ColorRGBA.White);
		mainContent.addChild(label);
	}

	@Override
	public void update(float tpf) {
		super.update(tpf);

		timer += tpf;

		if (timer >= 0.5f) {
			timer -= 0.5f;

			int fps = (int)(1f / tpf);

			label.setText(fps + "");
		}
	}
}