package org.sunless.shared.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jme3.math.Vector3f;

/**
 * Inspired by Ascaria's post (https://hub.jmonkeyengine.org/t/quaternions-torque-and-direction/25158/9)
 */
public class PIDController {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final Vector3f lastError = new Vector3f();
	private final Vector3f derivative = new Vector3f();
	private final Vector3f integral = new Vector3f();

	private final float errorWeight;
	private final float integralWeight;
	private final float derivativeWeight;

	public PIDController(float errorWeight, float integralWeight, float derivativeWeight) {
		this.errorWeight = errorWeight;
		this.integralWeight = integralWeight;
		this.derivativeWeight = derivativeWeight;
	}

	public Vector3f getCorrection(float tpf, Vector3f error, boolean logData) {
		derivative.set(error.subtract(lastError).divideLocal(tpf));
		integral.addLocal(error.mult(tpf));
		lastError.set(error);

		Vector3f correction = error.mult(errorWeight).add(integral.mult(integralWeight)).add(derivative.mult(derivativeWeight));

		if (logData) {
			log.info("derivative: " + derivative + " integral: " + integral + " lastError: " + lastError + " correction: " + correction);
		}

		return correction;
	}
}