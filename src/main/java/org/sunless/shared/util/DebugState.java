package org.sunless.shared.util;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.config.DebugConfigChangedEvent;
import org.sunless.shared.event.config.DebugConfigChangedHandler;

public class DebugState {

	private final EventBus eventBus = EventBus.get();

	private static final DebugState instance = new DebugState();

	private final Map<String, DebugStateEntry> entries = new HashMap<>();

	public static DebugState get() {
		return instance;
	}

	private DebugState() {
		eventBus.addHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				DebugStateEntry entry = entries.get(event.getDebugCommandKey());

				if (entry != null) {
					entry = new DebugStateEntry(!entry.isToggled(), event.getArguments());
				} else {
					entry = new DebugStateEntry(true, event.getArguments());
				}

				entries.put(event.getDebugCommandKey(), entry);
			}
		});
	}

	public void clear() {
		entries.clear();
	}

	public boolean isToggled(String command) {
		DebugStateEntry entry = entries.get(command);
		return entry != null && entry.isToggled();
	}

	public Map<String, String[]> getCommandsArgumentsMap() {
		Map<String, String[]> map = new HashMap<>(entries.size());

		for (Entry<String, DebugStateEntry> entry : entries.entrySet()) {
			String[] arguments = entry.getValue().getArguments();

			if (arguments != null) {
				map.put(entry.getKey(), arguments);
			}
		}

		return map;
	}

	public Set<String> getToggledCommands() {
		return entries.keySet().stream()
				.filter(key -> {
					DebugStateEntry val = entries.get(key);
					return val.isToggled() && val.getArguments() == null;
				})
				.collect(Collectors.toSet());
	}
}