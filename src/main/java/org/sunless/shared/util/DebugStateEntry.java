package org.sunless.shared.util;

public class DebugStateEntry {

	private final boolean toggled;
	private final String[] arguments;

	public DebugStateEntry(boolean toggled, String[] arguments) {
		this.toggled = toggled;
		this.arguments = arguments;
	}

	public boolean isToggled() {
		return toggled;
	}

	public String[] getArguments() {
		return arguments;
	}
}