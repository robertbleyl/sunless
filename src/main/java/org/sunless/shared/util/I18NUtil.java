package org.sunless.shared.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class I18NUtil {

	private static final Logger log = LoggerFactory.getLogger(I18NUtil.class);

	private static final ResourceBundle bundle = ResourceBundle.getBundle(FileConstants.UI_PATH + ".i18n.i18n");

	public static String get(String key) {
		try {
			String value = bundle.getString(key);
			return value;
		} catch (MissingResourceException e) {
			log.warn("The i18n key '" + key + "' was not found!");
		}

		return key;
	}
}