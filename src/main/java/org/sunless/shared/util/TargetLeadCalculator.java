package org.sunless.shared.util;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

public class TargetLeadCalculator {

	public static Vector3f getTargetLeadPosition(Vector3f startLocation, Spatial target, float weaponFireSpeed) {
		RigidBodyControl bodyControl = target.getControl(RigidBodyControl.class);
		Vector3f targetVelocity = bodyControl.getLinearVelocity();
		Vector3f targetLocation = target.getLocalTranslation();

		float speedFactor = targetVelocity.lengthSquared() - (weaponFireSpeed * weaponFireSpeed);
		Vector3f diffPos = targetLocation.subtract(startLocation);
		float diffFactor = 2f * diffPos.dot(targetVelocity);

		if (speedFactor < 0) {
			float diffFactorSqr = diffFactor * diffFactor;
			float diffPosSqr = diffPos.lengthSquared();
			float rootFactor = FastMath.sqrt(diffFactorSqr - speedFactor * 4f * diffPosSqr);
			float doubleSpeedFactor = 2f * speedFactor;

			float derivativeOne = (rootFactor - diffFactor) / doubleSpeedFactor;
			float derivateTwo = -(rootFactor + diffFactor) / doubleSpeedFactor;
			float derivative = derivativeOne >= 0 ? derivativeOne : derivateTwo;

			return targetLocation.add(targetVelocity.mult(derivative));
		}

		return targetLocation;

		// BoundingSphere sphere = (BoundingSphere)target.getWorldBound();
		// Vector3f targetLocation = target.getLocalTranslation().add(linearVelocity.normalize().mult(sphere.getRadius()));
		//
		// float diffPos = targetLocation.subtract(startLocation).length();
		// float diffSpeed = weaponFireSpeed - targetSpeed;
		// if (diffSpeed == 0f) {
		// diffSpeed = 0.0001f;
		// }
		//
		// Vector3f linVelNorm = linearVelocity.normalize();
		// Vector3f mult = linVelNorm.mult(diffPos / diffSpeed);
		// Vector3f targetLead = mult.mult(targetSpeed);
		// Vector3f impactLocation = targetLocation.add(targetLead);

		// System.out.println("targetSpeed: " + targetSpeed);
		// System.out.println("diffPos: " + diffPos);
		// System.out.println("diffSpeed: " + diffSpeed);
		// System.out.println("impactLocation: " + impactLocation);
		// System.out.println("screenCoordinates: " + screenCoordinates);
		// System.out.println("========================================");

		// return impactLocation;
	}
}