package org.sunless.shared.util;

import org.apache.commons.math3.util.FastMath;

import com.jme3.math.Vector3f;

public class CheckUtils {

	public static void checkVectorForZeroValues(Vector3f v) {
		if (FastMath.abs(v.x) < 0.01f) {
			v.x = 0f;
		}

		if (FastMath.abs(v.y) < 0.01f) {
			v.y = 0f;
		}

		if (FastMath.abs(v.z) < 0.01f) {
			v.z = 0f;
		}
	}
}