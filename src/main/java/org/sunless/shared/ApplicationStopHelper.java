package org.sunless.shared;

import com.jme3.app.SimpleApplication;

public class ApplicationStopHelper {

	private final SimpleApplication app;

	public ApplicationStopHelper(SimpleApplication app) {
		this.app = app;
	}

	public void stop() {
		app.stop();
	}
}