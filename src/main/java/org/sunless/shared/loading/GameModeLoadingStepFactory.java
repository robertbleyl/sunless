package org.sunless.shared.loading;

import java.util.List;

public interface GameModeLoadingStepFactory {
	
	void addLoadingSteps(List<LoadingStep> steps);
}