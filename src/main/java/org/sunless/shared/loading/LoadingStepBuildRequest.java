package org.sunless.shared.loading;

import org.sunless.shared.json.MapData;

public class LoadingStepBuildRequest {

	private MapData mapData;

	public MapData getMapData() {
		return mapData;
	}

	public void setMapData(MapData mapData) {
		this.mapData = mapData;
	}
}