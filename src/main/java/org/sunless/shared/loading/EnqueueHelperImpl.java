package org.sunless.shared.loading;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import com.jme3.app.Application;

public class EnqueueHelperImpl implements EnqueueHelper {
	
	protected final Application app;
	
	public EnqueueHelperImpl(Application app) {
		this.app = app;
	}
	
	@Override
	public Future<Void> enqueue(Callable<Void> callable) {
		return app.enqueue(callable);
	}
}