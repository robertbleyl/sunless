package org.sunless.shared.loading;

import org.sunless.shared.ui.GUIState;

public interface LoadingState extends GUIState {
	
	void updatePercentage(float percentage);
}