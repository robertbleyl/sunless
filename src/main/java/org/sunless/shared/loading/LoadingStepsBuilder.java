package org.sunless.shared.loading;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.sunless.shared.entity.EntityAttacher;
import org.sunless.shared.entity.EntityFactory;
import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.GameInfoProvider;
import org.sunless.shared.gameplay.IngameState;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.gameplay.mode.ConquestLoadingStepFactory;
import org.sunless.shared.gameplay.mode.MotherShipLoadingStepFactory;
import org.sunless.shared.json.Faction;
import org.sunless.shared.json.FactionConfig;
import org.sunless.shared.json.GameMode;
import org.sunless.shared.json.JsonHelper;
import org.sunless.shared.json.MapData;
import org.sunless.shared.json.WorldObjectData;
import org.sunless.shared.util.FileConstants;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.jme3.asset.AssetManager;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.texture.Texture;
import com.jme3.util.SkyFactory;

public abstract class LoadingStepsBuilder {

	protected Provider<AssetManager> assetManagerProvider;
	protected EntityFactory entityFactory;
	protected GameInfoProvider gameInfoProvider;
	protected EntityAttacher entityAttacher;
	protected TeamsContainer teamsContainer;
	protected Provider<Camera> cameraProvider;
	protected IngameState ingameState;

	protected final Map<Faction, FactionConfig> factionConfigs = new HashMap<>(2);

	protected abstract void addSteps(List<LoadingStep> steps, LoadingStepBuildRequest request);

	public List<LoadingStep> createLoadingSteps(LoadingStepBuildRequest request) {
		List<LoadingStep> steps = new ArrayList<>();

		steps.add(new LoadingStep(10f, false) {
			@Override
			public void load() throws Exception {
				loadFactionConfigs(request);
			}
		});

		addMapObjectsSteps(steps, request);

		addGameModeLoadingSteps(steps, request);

		addSteps(steps, request);

		return steps;
	}

	protected void addMapObjectsSteps(List<LoadingStep> steps, LoadingStepBuildRequest request) {
		final List<WorldObjectData> objects = request.getMapData().getObjects();

		if (objects != null) {
			for (final WorldObjectData object : objects) {
				steps.add(new LoadingStep(5f, true) {
					@Override
					public void load() throws Exception {
						List<GameMode> gameModes = object.getGameModes();

						GameInfo gameInfo = gameInfoProvider.getGameInfo();

						if (CollectionUtils.isEmpty(gameModes) || gameInfo == null || gameModes.contains(gameInfo.getGameMode())) {
							Node entity = entityFactory.createObject(object);
							entityAttacher.attachEntity(entity);
						}
					}
				});
			}

			entityFactory.finishBatchNodes();
		}
	}

	protected void addGameModeLoadingSteps(List<LoadingStep> steps, LoadingStepBuildRequest request) {
		GameModeLoadingStepFactory factory = null;

		switch (gameInfoProvider.getGameInfo().getGameMode()) {
			case CONQUEST:
				factory = new ConquestLoadingStepFactory();
				break;
			case MOTHER_SHIP:
				factory = createMotherShipLoadingStepFactory(request);
				break;
		}

		if (factory != null) {
			factory.addLoadingSteps(steps);
		}
	}

	protected GameModeLoadingStepFactory createMotherShipLoadingStepFactory(LoadingStepBuildRequest request) {
		GameModeLoadingStepFactory factory = new MotherShipLoadingStepFactory(request.getMapData(), entityFactory, teamsContainer, factionConfigs, entityAttacher, gameInfoProvider.getGameInfo());
		return factory;
	}

	protected void loadFactionConfigs(LoadingStepBuildRequest request) throws Exception {
		MapData mapData = request.getMapData();
		Faction faction1 = mapData.getFaction1();
		Faction faction2 = mapData.getFaction2();

		loadFactionConfig(faction1);
		loadFactionConfig(faction2);
	}

	protected void loadFactionConfig(Faction faction) throws Exception {
		FactionConfig config = JsonHelper.get().toPOJO(FileConstants.FACTIONS_PATH_COMPLETE + faction.name().toLowerCase() + FileConstants.FACTION_CONFIG_FILE, FactionConfig.class);
		factionConfigs.put(faction, config);
	}

	protected void initSkyBox(LoadingStepBuildRequest request) {
		String skyBoxName = request.getMapData().getSkyBox();

		String path = FileConstants.SKYBOXES_PATH + skyBoxName + "/";

		AssetManager assetManager = assetManagerProvider.get();
		Texture west = assetManager.loadTexture(path + "west.jpg");
		Texture east = assetManager.loadTexture(path + "east.jpg");
		Texture north = assetManager.loadTexture(path + "north.jpg");
		Texture south = assetManager.loadTexture(path + "south.jpg");
		Texture up = assetManager.loadTexture(path + "up.jpg");
		Texture down = assetManager.loadTexture(path + "down.jpg");

		Spatial skybox = SkyFactory.createSky(assetManager, west, east, north, south, up, down, Vector3f.UNIT_XYZ, 500);
		ingameState.getRootNode().attachChild(skybox);
	}

	protected void initCamera(LoadingStepBuildRequest request) {
		Vector3f pos = request.getMapData().getInitCameraPosition().toVector();
		// Vector3f dir = mapData.getInitCameraDirection().toVector().normalize();

		Camera camera = cameraProvider.get();
		camera.setFrustumFar(999999f);
		camera.getLocation().set(pos);
		camera.lookAt(Vector3f.ZERO, Vector3f.UNIT_Y);
		camera.update();
	}

	protected void initLights(LoadingStepBuildRequest request) {
		Vector3f dir = request.getMapData().getLightDirection().toVector();

		DirectionalLight sun = new DirectionalLight();
		sun.setDirection(dir.normalize());
		sun.setColor(ColorRGBA.White);
		sun.setName("sun");

		ingameState.getRootNode().addLight(sun);

		AmbientLight al = new AmbientLight();
		al.setColor(ColorRGBA.White.mult(0.3f));
		al.setName("AL");
		ingameState.getRootNode().addLight(al);
	}

	@Inject
	public void setAssetManagerProvider(Provider<AssetManager> assetManagerProvider) {
		this.assetManagerProvider = assetManagerProvider;
	}

	@Inject
	public void setEntityFactory(EntityFactory entityFactory) {
		this.entityFactory = entityFactory;
	}

	@Inject
	public void setGameInfoProvider(GameInfoProvider gameInfoProvider) {
		this.gameInfoProvider = gameInfoProvider;
	}

	@Inject
	public void setEntityAttacher(EntityAttacher entityAttacher) {
		this.entityAttacher = entityAttacher;
	}

	@Inject
	public void setTeamsContainer(TeamsContainer teamsContainer) {
		this.teamsContainer = teamsContainer;
	}

	@Inject
	public void setCameraProvider(Provider<Camera> cameraProvider) {
		this.cameraProvider = cameraProvider;
	}

	@Inject
	public void setIngameState(IngameState ingameState) {
		this.ingameState = ingameState;
	}
}