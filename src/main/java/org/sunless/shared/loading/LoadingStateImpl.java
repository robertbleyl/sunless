package org.sunless.shared.loading;

import org.sunless.shared.NodeConstants;
import org.sunless.shared.ui.GUIStateImpl;
import org.sunless.shared.ui.ScreenUtils;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.scene.Node;

import tonegod.gui.core.Screen;

@Singleton
public class LoadingStateImpl extends GUIStateImpl implements LoadingState {

	private LoadingBar loadingBar;

	@Inject
	public LoadingStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(screen, appRootNode, appGuiNode);

		init();
	}

	@Override
	public void init() {
		float screenWidth = screen.getWidth();
		float screenHeight = screen.getHeight();

		float padding = ScreenUtils.getPadding(screen);

		float width = screenWidth - (padding * 2);
		float height = screenHeight / 15;

		float x = padding;
		float y = screenHeight - padding - height;

		loadingBar = new LoadingBar(screen, width, height, "blueBar.png");
		loadingBar.setPosition(x, y);
		mainContent.addChild(loadingBar);
	}

	@Override
	public void updatePercentage(float percentage) {
		loadingBar.updateProgress(percentage);
	}
}