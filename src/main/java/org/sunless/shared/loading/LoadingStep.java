package org.sunless.shared.loading;

public abstract class LoadingStep {
	
	protected final float time;
	protected final boolean enqueue;
	
	public LoadingStep(float time, boolean enqueue) {
		this.time = time;
		this.enqueue = enqueue;
	}
	
	public abstract void load() throws Exception;
	
	public float getTime() {
		return time;
	}
	
	public boolean isEnqueue() {
		return enqueue;
	}
}