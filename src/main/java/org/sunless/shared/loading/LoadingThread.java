package org.sunless.shared.loading;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoadingThread extends Thread {
	
	private static final Logger log = LoggerFactory.getLogger(LoadingThread.class);
	
	protected final List<LoadingStep> steps;
	protected final float totalTime;
	protected final EnqueueHelper enqueueHelper;
	protected final LoadingState loadingState;
	
	public LoadingThread(List<LoadingStep> steps, EnqueueHelper enqueueHelper, LoadingState loadingState) {
		super();
		this.steps = steps;
		this.enqueueHelper = enqueueHelper;
		this.loadingState = loadingState;
		
		if (steps == null) {
			throw new IllegalArgumentException("steps must not be null!");
		}
		
		if (steps.isEmpty()) {
			throw new IllegalArgumentException("steps must not be empty!");
		}
		
		if (enqueueHelper == null) {
			throw new IllegalArgumentException("enqueueHelper must not be null!");
		}
		
		if (loadingState == null) {
			throw new IllegalArgumentException("loadingState must not be null!");
		}
		
		float time = 0f;
		
		for (LoadingStep step : steps) {
			time += step.getTime();
		}
		
		totalTime = time;
	}
	
	@Override
	public void run() {
		try {
			float elapsedTime = 0f;
			
			for (LoadingStep step : steps) {
				processStep(step);
				
				elapsedTime += step.getTime();
				
				final float percentage = elapsedTime / totalTime;
				
				updateLoadingState(percentage);
			}
		} catch (Exception e) {
			log.error("Error while running LoadingThread:", e);
		}
	}
	
	protected void processStep(LoadingStep step) throws Exception {
		if (step.isEnqueue()) {
			enqueueStep(step);
		} else {
			step.load();
		}
	}
	
	protected void enqueueStep(LoadingStep step) throws InterruptedException, ExecutionException {
		Callable<Void> callable = createStepCallable(step);
		Future<Void> future = enqueueHelper.enqueue(callable);
		future.get();
	}
	
	protected Callable<Void> createStepCallable(final LoadingStep step) {
		return new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				step.load();
				return null;
			}
		};
	}
	
	protected void updateLoadingState(final float percentage) throws InterruptedException, ExecutionException {
		Callable<Void> callable = createLoadingStateCallable(percentage);
		Future<Void> future = enqueueHelper.enqueue(callable);
		future.get();
	}
	
	protected Callable<Void> createLoadingStateCallable(final float percentage) {
		return new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				loadingState.updatePercentage(percentage);
				return null;
			}
		};
	}
}