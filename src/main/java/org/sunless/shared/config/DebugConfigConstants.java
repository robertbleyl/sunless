package org.sunless.shared.config;

public interface DebugConfigConstants {

	String dbg_toggle_damage_collision = "dbg_toggle_damage_collision";
	String dbg_toggle_damage_weapons = "dbg_toggle_damage_weapons";
	String dbg_toggle_show_bounding_volumes = "dbg_toggle_show_bounding_volumes";
	String dbg_toggle_show_weaponfire_bounding_volumes = "dbg_toggle_show_weaponfire_bounding_volumes";
	String dbg_toggle_show_weapon_paths = "dbg_toggle_show_weapon_paths";
	String dbg_toggle_bot_targeting = "dbg_toggle_bot_targeting";
	String dbg_toggle_bot_movement = "dbg_toggle_bot_movement";
	String dbg_toggle_weapon_fire_movement = "dbg_toggle_weapon_fire_movement";
	String dbg_toggle_draw_collision_bounds = "dbg_toggle_draw_collision_bounds";

	String dbg_toggle_log_player_infos = "dbg_toggle_log_player_infos";
	String dbg_toggle_log_player_info_type_speed = "speed";
	String dbg_toggle_log_player_info_type_rotation = "rotation";

	String dbg_toggle_bot_spectating_data = "dbg_toggle_bot_spectating_data";

	String dbg_switch_team = "dbg_switch_team";
	String dbg_change_entity_property = "dbg_change_entity_property";
}