package org.sunless.shared.entity;

import com.jme3.scene.Mesh;
import com.jme3.scene.Node;

public class EntityCreationResult {
	
	private final Node entity;
	private final Mesh mesh;
	private final EntityProcessResult result;
	
	public EntityCreationResult(Node entity, Mesh mesh, EntityProcessResult result) {
		this.entity = entity;
		this.mesh = mesh;
		this.result = result;
	}
	
	public Node getEntity() {
		return entity;
	}
	
	public Mesh getMesh() {
		return mesh;
	}
	
	public EntityProcessResult getResult() {
		return result;
	}
}