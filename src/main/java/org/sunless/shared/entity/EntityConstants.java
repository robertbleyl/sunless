package org.sunless.shared.entity;

public interface EntityConstants {

	String id = "id";
	String player = "player";
	String costs = "costs";
	String fileName = "fileName";

	String isMotherShip = "isMotherShip";
	String faction = "faction";
	String team = "team";
	String isSpawnPoint = "isSpawnPoint";

	String sync = "sync";

	String mass = "mass";
	String hitPoints = "hitPoints";
	String shields = "shields";
	String maxHitPoints = "maxHitPoints";
	String maxShields = "maxShields";
	String shieldRegeneration = "shieldRegeneration";
	String specialWeaponEnergy = "specialWeaponEnergy";
	String specialWeaponEnergyPerWeaponFire = "specialWeaponEnergyPerWeaponFire";
	String maxSpecialWeaponEnergy = "maxSpecialWeaponEnergy";
	String specialWeaponEnergyRegeneration = "specialWeaponEnergyRegeneration";

	String acceleration = "acceleration";
	String maxForwardSpeed = "maxForwardSpeed";
	String maxBoostSpeed = "maxBoostSpeed";
	String maxBoostEnergy = "maxBoostEnergy";
	String boostEnergy = "boostEnergy";
	String maxStrafeSpeed = "maxStrafeSpeed";
	String maxRollSpeed = "maxRollSpeed";
	String maxYawSpeed = "maxYawSpeed";
	String maxPitchSpeed = "maxPitchSpeed";

	String weaponFireType = "weaponFireType";
	String standardWeaponSlots = "standardWeaponSlots";
	String specialWeaponSlots = "specialWeaponSlots";

	String weaponFireDamage = "weaponFireDamage";
	String weaponFireCoolDown = "weaponFireCoolDown";
	String weaponFireSpeed = "weaponFireSpeed";
	String weaponFireLifeTime = "weaponFireLifeTime";
	String weaponFireSize = "weaponFireSize";

	String standard_weaponFirePrefix = "standard_";
	String special_weaponFirePrefix = "special_";

	String standard_weaponFireType = "standard_weaponFireType";
	String standard_weaponFireDamage = "standard_weaponFireDamage";
	String standard_weaponFireCoolDown = "standard_weaponFireCoolDown";
	String standard_weaponFireSpeed = "standard_weaponFireSpeed";
	String standard_weaponFireLifeTime = "standard_weaponFireLifeTime";
	String standard_weaponFireSize = "standard_weaponFireSize";

	String special_weaponFireDamage = "special_weaponFireDamage";
	String special_weaponFireCoolDown = "special_weaponFireCoolDown";
	String special_weaponFireSpeed = "special_weaponFireSpeed";
	String special_weaponFireLifeTime = "special_weaponFireLifeTime";
	String special_weaponFireSize = "special_weaponFireSize";

	String lightningSlots = "lightningSlots";

	String special_weaponFireType = "special_weaponFireType";
	String special_weaponFireType_rocketLauncher = "special_weaponFireType_rocketLauncher";
	String special_weaponFireType_railGun = "special_weaponFireType_railGun";
	String special_weaponFireType_lightning = "special_weaponFireType_lightning";
	String special_weaponFireType_gatling = "special_weaponFireType_gatling";

	String weaponFireFiringShip = "weaponFireFiringShip";

	String syncMessageFactory = "syncMessageFactory";
	String damageHistory = "damageHistory";

	String controlPointType = "controlPointType";
	String controlPointCapturePoints = "controlPointCapturePoints";
	String controlPointCapturePointsTeam1 = "controlPointCapturePointsTeam1";
	String controlPointCapturePointsTeam2 = "controlPointCapturePointsTeam2";
	String controlPointCaptureRadius = "controlPointCaptureRadius";
	String controlPointSpawnDirection = "controlPointSpawnDirection";

	String main_geom = "main_geom";
	String slot = "slot";
	String standard_fireslot = "standard_fireslot";
	String special_fireslot = "special_fireslot";
	String lightning_slot = "lightning_slot";
	String nozzle_slot = "nozzle_slot";

	String nozzleSlots = "nozzleSlots";
	String batchId = "batchId";

	String shipEngineAudioNode = "shipEngineAudioNode";
	String weaponFireAudioNode = "weaponFireAudioNode";
}