package org.sunless.shared.entity;

import org.sunless.shared.gameplay.weapon.WeaponFireCreationData;
import org.sunless.shared.json.WorldObjectData;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public interface EntityFactory {

	Node createObject(WorldObjectData object) throws Exception;

	Node createEntity(String fileName) throws Exception;

	Node createShip(String fileName) throws Exception;

	Spatial createWeaponFire(WeaponFireCreationData data) throws Exception;

	Node createWorldObject(String fileName, boolean loadProperties) throws Exception;

	Spatial createExplosion();

	void finishBatchNodes();
}