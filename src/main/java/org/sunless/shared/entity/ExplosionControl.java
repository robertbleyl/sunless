package org.sunless.shared.entity;

import com.jme3.effect.ParticleEmitter;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class ExplosionControl extends AbstractControl {
	
	private final float lifeTime;
	
	private float time;
	
	public ExplosionControl(float lifeTime) {
		this.lifeTime = lifeTime;
	}
	
	@Override
	protected void controlUpdate(float tpf) {
		time += tpf;
		
		ParticleEmitter e = (ParticleEmitter)spatial;
		e.getStartColor().a -= tpf;
		e.getEndColor().a -= tpf;
		
		if (time > lifeTime) {
			spatial.removeFromParent();
			spatial.removeControl(this);
		}
	}
	
	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {
		
	}
}