package org.sunless.shared.entity;

public interface EntityIdProvider {
	
	long getNextId();
}