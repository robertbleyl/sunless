package org.sunless.shared.entity;

import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.gameplay.EntityPropertyChangeEvent;
import org.sunless.shared.gameplay.EntityPropertyChangeCause;

import com.jme3.scene.Spatial;

public class EntityHelper {
	
	public static long getEntityId(Spatial entity) {
		Long id = entity.getUserData(EntityConstants.id);
		return id.longValue();
	}
	
	public static void changeProperty(Spatial entity, String key, Object value, EntityPropertyChangeCause cause) {
		Object oldValue = entity.getUserData(key);
		entity.setUserData(key, value);
		
		long entityId = getEntityId(entity);
		EventBus.get().fireEvent(new EntityPropertyChangeEvent(entity, entityId, key, value, oldValue, cause));
	}
}