package org.sunless.shared.event;

public abstract class Event<H extends EventHandler> {
	
	public abstract EventType<H> getType();
	
	public abstract void callHandler(H handler);
}