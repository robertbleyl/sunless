package org.sunless.shared.event;

public interface EventBusAction {

	void perform();
}