package org.sunless.shared.event.main;

import org.sunless.shared.event.EventHandler;

public interface EndGameTimeUpdateHandler extends EventHandler {
	
	void onUpdateGameTime(EndGameTimeUpdateEvent event);
}