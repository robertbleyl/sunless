package org.sunless.shared.event.main;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;
import org.sunless.shared.gameplay.player.Player;

public class RemovePlayerEvent extends Event<RemovePlayerHandler> {
	
	public static final EventType<RemovePlayerHandler> TYPE = new EventType<>();
	
	private final Player player;
	
	public RemovePlayerEvent(Player player) {
		this.player = player;
	}
	
	@Override
	public EventType<RemovePlayerHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(RemovePlayerHandler handler) {
		handler.onRemovePlayer(this);
	}
	
	public Player getPlayer() {
		return player;
	}
}