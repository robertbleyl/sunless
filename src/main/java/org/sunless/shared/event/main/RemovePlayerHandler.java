package org.sunless.shared.event.main;

import org.sunless.shared.event.EventHandler;

public interface RemovePlayerHandler extends EventHandler {
	
	void onRemovePlayer(RemovePlayerEvent event);
}