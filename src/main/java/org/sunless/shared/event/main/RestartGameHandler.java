package org.sunless.shared.event.main;

import org.sunless.shared.event.EventHandler;

public interface RestartGameHandler extends EventHandler {
	
	void onRestartGame(RestartGameEvent event);
}