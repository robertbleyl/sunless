package org.sunless.shared.event.main;

import org.sunless.shared.event.EventHandler;

public interface GameTimeUpdateHandler extends EventHandler {
	
	void onGameTimeUpdate(GameTimeUpdateEvent event);
}