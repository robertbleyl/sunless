package org.sunless.shared.event.config;

import org.sunless.shared.event.EventHandler;

public interface UpdateGraphicsConfigHandler extends EventHandler {
	
	void onUpdate(UpdateGraphicsConfigEvent event);
}