package org.sunless.shared.event.config;

import org.sunless.shared.event.EventHandler;

public interface DebugConfigChangedHandler extends EventHandler {
	
	void onChanged(DebugConfigChangedEvent event);
}