package org.sunless.shared.event.config;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;
import org.sunless.shared.gameplay.player.Player;

public class DebugConfigChangedEvent extends Event<DebugConfigChangedHandler> {

	public static final EventType<DebugConfigChangedHandler> TYPE = new EventType<>();

	private Player player;
	private final String debugCommandKey;
	private final String[] arguments;

	public DebugConfigChangedEvent(String debugCommandKey, String[] arguments) {
		this.debugCommandKey = debugCommandKey;
		this.arguments = arguments;
	}

	public DebugConfigChangedEvent(Player player, String debugCommandKey, String[] arguments) {
		this.player = player;
		this.debugCommandKey = debugCommandKey;
		this.arguments = arguments;
	}

	@Override
	public EventType<DebugConfigChangedHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(DebugConfigChangedHandler handler) {
		handler.onChanged(this);
	}

	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}

	public String getDebugCommandKey() {
		return debugCommandKey;
	}

	public String[] getArguments() {
		return arguments;
	}
}