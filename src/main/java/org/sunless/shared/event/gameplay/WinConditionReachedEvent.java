package org.sunless.shared.event.gameplay;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;
import org.sunless.shared.gameplay.player.Team;

public class WinConditionReachedEvent extends Event<WinConditionReachedHandler> {
	
	public static final EventType<WinConditionReachedHandler> TYPE = new EventType<>();
	
	private final Team winnerTeam;
	
	public WinConditionReachedEvent(Team winnerTeam) {
		this.winnerTeam = winnerTeam;
	}
	
	@Override
	public EventType<WinConditionReachedHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(WinConditionReachedHandler handler) {
		handler.onWinConditionReached(this);
	}
	
	public Team getWinnerTeam() {
		return winnerTeam;
	}
}