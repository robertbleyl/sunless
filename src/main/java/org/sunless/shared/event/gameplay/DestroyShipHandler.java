package org.sunless.shared.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface DestroyShipHandler extends EventHandler {
	
	void onDestroyShip(DestroyShipEvent event);
}