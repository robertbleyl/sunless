package org.sunless.shared.event.gameplay;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;
import org.sunless.shared.gameplay.player.Player;

public class RespawnPlayerEvent extends Event<RespawnPlayerHandler> {

	public static final EventType<RespawnPlayerHandler> TYPE = new EventType<>();

	private final Player player;

	public RespawnPlayerEvent(Player player) {
		this.player = player;
	}

	@Override
	public EventType<RespawnPlayerHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(RespawnPlayerHandler handler) {
		handler.onRespawnPlayer(this);
	}

	public Player getPlayer() {
		return player;
	}
}