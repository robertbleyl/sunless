package org.sunless.shared.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface WinConditionReachedHandler extends EventHandler {
	
	void onWinConditionReached(WinConditionReachedEvent event);
}