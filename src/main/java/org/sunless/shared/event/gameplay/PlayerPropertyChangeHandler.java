package org.sunless.shared.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface PlayerPropertyChangeHandler extends EventHandler {
	
	void onChange(PlayerPropertyChangeEvent event);
}