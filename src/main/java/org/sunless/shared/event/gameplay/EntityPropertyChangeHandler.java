package org.sunless.shared.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface EntityPropertyChangeHandler extends EventHandler {
	
	void onChange(EntityPropertyChangeEvent event);
}