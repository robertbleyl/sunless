package org.sunless.shared.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface RespawnPlayerHandler extends EventHandler {
	
	void onRespawnPlayer(RespawnPlayerEvent event);
}