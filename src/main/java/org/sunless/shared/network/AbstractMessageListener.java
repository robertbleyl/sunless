package org.sunless.shared.network;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.inject.Inject;
import com.jme3.network.Message;
import com.jme3.network.MessageListener;

@SuppressWarnings({ "rawtypes", "unchecked" })
public class AbstractMessageListener<S, D extends AbstractMessageHandler> implements MessageListener<S> {

	protected Map<Class<? extends Message>, AbstractMessageHandler> messageHandlers;

	@Override
	public void messageReceived(S source, Message m) {
		AbstractMessageHandler handler = messageHandlers.get(m.getClass());

		if (handler != null && handler.isEnabled()) {
			handler.handleMessage(source, m);
		}
	}

	@Inject
	public void setMessageHandlers(Set<D> handlers) {
		messageHandlers = new HashMap<>(handlers.size());

		for (D handler : handlers) {
			messageHandlers.put(handler.getMessageClass(), handler);
		}
	}
}