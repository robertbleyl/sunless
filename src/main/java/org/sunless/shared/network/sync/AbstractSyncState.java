package org.sunless.shared.network.sync;

import java.util.HashMap;

import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.loading.EnqueueHelper;

import com.jme3.app.state.AbstractAppState;
import com.jme3.scene.Spatial;

public abstract class AbstractSyncState extends AbstractAppState {

	// private static final Logger log = LoggerFactory.getLogger(AbstractSyncState.class);

	protected final EventBus eventBus = EventBus.get();

	protected float syncFrequency;

	protected HashMap<Long, Spatial> syncEntities = new HashMap<>();
	protected float syncTimer;

	protected final EnqueueHelper enqueueHelper;

	public AbstractSyncState(EnqueueHelper enqueueHelper, int tickRate) {
		super();
		this.enqueueHelper = enqueueHelper;

		setTickRate(tickRate);

		resetValues();
	}

	private void resetValues() {
		syncTimer = 0;
	}

	protected abstract void onUpdate(float tpf);

	@Override
	public void update(float tpf) {
		onUpdate(tpf);
	}

	public void addEntity(Spatial entity) {
		long id = EntityHelper.getEntityId(entity);
		syncEntities.put(id, entity);
	}

	public void removeEntity(Spatial entity) {
		long id = EntityHelper.getEntityId(entity);

		removeEntity(id);
	}

	public void removeEntity(long entityId) {
		syncEntities.remove(entityId);
	}

	public void clearObjects() {
		syncEntities.clear();
	}

	public Spatial getEntity(long entityId) {
		return syncEntities.get(entityId);
	}

	@Override
	public void cleanup() {
		super.cleanup();

		clearObjects();

		resetValues();
	}

	public void setTickRate(int tickRate) {
		syncFrequency = 1f / tickRate;
	}
}