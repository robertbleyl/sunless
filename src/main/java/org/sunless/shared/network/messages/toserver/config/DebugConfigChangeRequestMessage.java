package org.sunless.shared.network.messages.toserver.config;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class DebugConfigChangeRequestMessage extends AbstractMessage {

	private String debugCommandKey;
	private String[] arguments;

	public DebugConfigChangeRequestMessage() {
		super(true);
	}

	public DebugConfigChangeRequestMessage(String debugCommandKey, String[] arguments) {
		this();
		this.debugCommandKey = debugCommandKey;
		this.arguments = arguments;
	}

	public String getDebugCommandKey() {
		return debugCommandKey;
	}

	public void setDebugCommandKey(String debugCommandKey) {
		this.debugCommandKey = debugCommandKey;
	}

	public String[] getArguments() {
		return arguments;
	}

	public void setArguments(String[] arguments) {
		this.arguments = arguments;
	}
}