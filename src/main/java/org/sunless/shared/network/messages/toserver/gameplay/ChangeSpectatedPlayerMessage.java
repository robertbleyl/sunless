package org.sunless.shared.network.messages.toserver.gameplay;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class ChangeSpectatedPlayerMessage extends AbstractMessage {

	private long playerId;

	public ChangeSpectatedPlayerMessage() {
		super(true);
	}

	public ChangeSpectatedPlayerMessage(long playerId) {
		this();
		this.playerId = playerId;
	}

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}
}