package org.sunless.shared.network.messages.toclient;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class RestartGameMessage extends AbstractMessage {
	
	public RestartGameMessage() {
		super(true);
	}
}