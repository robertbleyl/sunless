package org.sunless.shared.network.messages.toclient.sync;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class AbstractSyncMessage extends AbstractMessage {
	
	protected long entityId;
	protected float time;
	
	public AbstractSyncMessage() {
		super(false);
	}
	
	public float getTime() {
		return time;
	}
	
	public void setTime(float time) {
		this.time = time;
	}
	
	public long getEntityId() {
		return entityId;
	}
	
	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}
}