package org.sunless.shared.network.messages.toclient.main;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class GameTimeUpdateMessage extends AbstractMessage {
	
	private float currentGameTime;
	
	public GameTimeUpdateMessage() {
		super(false);
	}
	
	public GameTimeUpdateMessage(float currentGameTime) {
		this();
		this.currentGameTime = currentGameTime;
	}
	
	public float getCurrentGameTime() {
		return currentGameTime;
	}
	
	public void setCurrentGameTime(float currentGameTime) {
		this.currentGameTime = currentGameTime;
	}
}