package org.sunless.shared.network.messages.toclient.gamestate;

import java.util.Map;
import java.util.Set;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class GameStateResponseMessage extends AbstractMessage {

	private Long[] alivePlayers;
	private PropertyInitEntry[] entityProperties;
	private PropertyInitEntry[] playerProperties;
	private Map<String, String[]> debugCommands;
	private Set<String> toggledCommands;

	public GameStateResponseMessage() {
		super(true);
	}

	public GameStateResponseMessage(Long[] alivePlayers, PropertyInitEntry[] playerProperties, PropertyInitEntry[] entityProperties, Map<String, String[]> debugCommands, Set<String> toggledCommands) {
		this();
		this.alivePlayers = alivePlayers;
		this.playerProperties = playerProperties;
		this.entityProperties = entityProperties;
		this.debugCommands = debugCommands;
		this.toggledCommands = toggledCommands;
	}

	public Long[] getAlivePlayers() {
		return alivePlayers;
	}

	public void setAlivePlayers(Long[] alivePlayers) {
		this.alivePlayers = alivePlayers;
	}

	public PropertyInitEntry[] getPlayerProperties() {
		return playerProperties;
	}

	public void setPlayerProperties(PropertyInitEntry[] playerProperties) {
		this.playerProperties = playerProperties;
	}

	public PropertyInitEntry[] getEntityProperties() {
		return entityProperties;
	}

	public void setEntityProperties(PropertyInitEntry[] entityProperties) {
		this.entityProperties = entityProperties;
	}

	public Map<String, String[]> getDebugCommands() {
		return debugCommands;
	}

	public void setDebugCommands(Map<String, String[]> debugCommands) {
		this.debugCommands = debugCommands;
	}

	public Set<String> getToggledCommands() {
		return toggledCommands;
	}

	public void setToggledCommands(Set<String> toggledCommands) {
		this.toggledCommands = toggledCommands;
	}
}