package org.sunless.shared.network.messages.toclient.gameplay;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class PlayerShipChangeMessage extends AbstractMessage {
	
	private long playerId;
	private String shipName;
	private long entityId;
	
	public PlayerShipChangeMessage() {
		super(true);
	}
	
	public PlayerShipChangeMessage(long playerId, String shipName, long entityId) {
		this();
		this.playerId = playerId;
		this.shipName = shipName;
		this.entityId = entityId;
	}
	
	public long getPlayerId() {
		return playerId;
	}
	
	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}
	
	public String getShipName() {
		return shipName;
	}
	
	public void setShipName(String shipName) {
		this.shipName = shipName;
	}
	
	public long getEntityId() {
		return entityId;
	}
	
	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}
}