package org.sunless.shared.network.messages.toclient.main;

import org.sunless.shared.gameplay.GameInfoInitData;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class GameInfoInitMessage extends AbstractMessage {
	
	private GameInfoInitData data;
	
	public GameInfoInitMessage() {
		super(true);
	}
	
	public GameInfoInitMessage(GameInfoInitData data) {
		this();
		this.data = data;
	}
	
	public GameInfoInitData getData() {
		return data;
	}
	
	public void setData(GameInfoInitData data) {
		this.data = data;
	}
}