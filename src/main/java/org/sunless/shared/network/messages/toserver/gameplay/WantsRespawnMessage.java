package org.sunless.shared.network.messages.toserver.gameplay;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class WantsRespawnMessage extends AbstractMessage {
	
	public WantsRespawnMessage() {
		super(true);
	}
}