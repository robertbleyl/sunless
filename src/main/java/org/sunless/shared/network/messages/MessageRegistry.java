package org.sunless.shared.network.messages;

import org.sunless.shared.control.MovementInfo;
import org.sunless.shared.gameplay.CollisionEntityPropertyCauseSync;
import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.GameInfoInitData;
import org.sunless.shared.gameplay.WeaponFireEntityPropertyCauseSync;
import org.sunless.shared.network.messages.toclient.RestartGameMessage;
import org.sunless.shared.network.messages.toclient.config.DebugConfigChangedMessage;
import org.sunless.shared.network.messages.toclient.gameplay.ChangePlayerSpawnLocationRequestMessage;
import org.sunless.shared.network.messages.toclient.gameplay.EntityPropertyChangeMessage;
import org.sunless.shared.network.messages.toclient.gameplay.PlayerPropertyChangeMessage;
import org.sunless.shared.network.messages.toclient.gameplay.PlayerShipChangeMessage;
import org.sunless.shared.network.messages.toclient.gameplay.RemoveWeaponFireMessage;
import org.sunless.shared.network.messages.toclient.gameplay.RespawnPlayerMessage;
import org.sunless.shared.network.messages.toclient.gameplay.SpectatedPlayerUpdateMessage;
import org.sunless.shared.network.messages.toclient.gameplay.WeaponFireCreationMessage;
import org.sunless.shared.network.messages.toclient.gameplay.WinConditionReachedMessage;
import org.sunless.shared.network.messages.toclient.gamestate.GameStateResponseMessage;
import org.sunless.shared.network.messages.toclient.gamestate.PropertyInitEntry;
import org.sunless.shared.network.messages.toclient.main.EndTimerUpdateMessage;
import org.sunless.shared.network.messages.toclient.main.GameInfoInitMessage;
import org.sunless.shared.network.messages.toclient.main.GameTimeUpdateMessage;
import org.sunless.shared.network.messages.toclient.main.PlayerNameRequestMessage;
import org.sunless.shared.network.messages.toclient.main.ServerRestartedMessage;
import org.sunless.shared.network.messages.toclient.sync.AbstractSyncMessage;
import org.sunless.shared.network.messages.toclient.sync.ShipSyncMessage;
import org.sunless.shared.network.messages.toclient.sync.WeaponFireSyncMessage;
import org.sunless.shared.network.messages.toserver.EntityCreationRequestMessage;
import org.sunless.shared.network.messages.toserver.GameStateRequestMessage;
import org.sunless.shared.network.messages.toserver.config.DebugConfigChangeRequestMessage;
import org.sunless.shared.network.messages.toserver.gameplay.BuyShipRequestMessage;
import org.sunless.shared.network.messages.toserver.gameplay.ChangeSpectatedPlayerMessage;
import org.sunless.shared.network.messages.toserver.gameplay.WantsRespawnMessage;
import org.sunless.shared.network.messages.toserver.input.FireWeaponsMessage;
import org.sunless.shared.network.messages.toserver.input.MoveShipMessage;
import org.sunless.shared.network.messages.toserver.main.PlayerNameResponseMessage;

import com.jme3.network.serializing.Serializer;

public class MessageRegistry {

	public static void registerMessages() {
		Serializer.registerClass(MovementInfo.class);
		Serializer.registerClass(PropertyInitEntry.class);
		Serializer.registerClass(GameInfo.class);
		Serializer.registerClass(GameInfoInitMessage.class);
		Serializer.registerClass(EntityCreationRequestMessage.class);
		Serializer.registerClass(PlayerNameRequestMessage.class);
		Serializer.registerClass(PlayerNameResponseMessage.class);
		Serializer.registerClass(BuyShipRequestMessage.class);
		Serializer.registerClass(AbstractSyncMessage.class);
		Serializer.registerClass(ShipSyncMessage.class);
		Serializer.registerClass(WeaponFireSyncMessage.class);
		Serializer.registerClass(GameTimeUpdateMessage.class);
		Serializer.registerClass(WantsRespawnMessage.class);
		Serializer.registerClass(RespawnPlayerMessage.class);
		Serializer.registerClass(PlayerShipChangeMessage.class);
		Serializer.registerClass(MoveShipMessage.class);
		Serializer.registerClass(GameStateResponseMessage.class);
		Serializer.registerClass(GameStateRequestMessage.class);
		Serializer.registerClass(FireWeaponsMessage.class);
		Serializer.registerClass(WeaponFireCreationMessage.class);
		Serializer.registerClass(RemoveWeaponFireMessage.class);
		Serializer.registerClass(EntityPropertyChangeMessage.class);
		Serializer.registerClass(PlayerPropertyChangeMessage.class);
		Serializer.registerClass(WinConditionReachedMessage.class);
		Serializer.registerClass(EndTimerUpdateMessage.class);
		Serializer.registerClass(RestartGameMessage.class);
		Serializer.registerClass(ServerRestartedMessage.class);
		Serializer.registerClass(GameInfoInitData.class);
		Serializer.registerClass(DebugConfigChangeRequestMessage.class);
		Serializer.registerClass(DebugConfigChangedMessage.class);
		Serializer.registerClass(ChangePlayerSpawnLocationRequestMessage.class);
		Serializer.registerClass(CollisionEntityPropertyCauseSync.class);
		Serializer.registerClass(WeaponFireEntityPropertyCauseSync.class);
		Serializer.registerClass(ChangeSpectatedPlayerMessage.class);
		Serializer.registerClass(SpectatedPlayerUpdateMessage.class);
	}
}