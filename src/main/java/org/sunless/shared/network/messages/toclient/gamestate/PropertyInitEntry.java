package org.sunless.shared.network.messages.toclient.gamestate;

import com.jme3.network.serializing.Serializable;

@Serializable
public class PropertyInitEntry {

	private long entityId;
	private String key;
	private Object value;

	public PropertyInitEntry() {

	}

	public PropertyInitEntry(long entityId, String key, Object value) {
		this.entityId = entityId;
		this.key = key;
		this.value = value;
	}

	public long getEntityId() {
		return entityId;
	}

	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
}