package org.sunless.shared.network.messages.toclient.main;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class EndTimerUpdateMessage extends AbstractMessage {
	
	private int secondsLeft;
	
	public EndTimerUpdateMessage() {
		super(true);
	}
	
	public EndTimerUpdateMessage(int secondsLeft) {
		this();
		this.secondsLeft = secondsLeft;
	}
	
	public int getSecondsLeft() {
		return secondsLeft;
	}
	
	public void setSecondsLeft(int secondsLeft) {
		this.secondsLeft = secondsLeft;
	}
}