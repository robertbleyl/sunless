package org.sunless.shared.network.messages.toclient.gameplay;

import com.jme3.math.Vector3f;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class ChangePlayerSpawnLocationRequestMessage extends AbstractMessage {

	private Vector3f requestedSpawnLocation;

	public ChangePlayerSpawnLocationRequestMessage() {
		super(true);
	}

	public ChangePlayerSpawnLocationRequestMessage(Vector3f requestedSpawnLocation) {
		this();
		this.requestedSpawnLocation = requestedSpawnLocation;
	}

	public Vector3f getRequestedSpawnLocation() {
		return requestedSpawnLocation;
	}

	public void setRequestedSpawnLocation(Vector3f requestedSpawnLocation) {
		this.requestedSpawnLocation = requestedSpawnLocation;
	}
}
