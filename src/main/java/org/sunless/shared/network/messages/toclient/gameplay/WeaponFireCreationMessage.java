package org.sunless.shared.network.messages.toclient.gameplay;

import com.jme3.math.Vector3f;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class WeaponFireCreationMessage extends AbstractMessage {

	private Vector3f startLocation;
	private long weaponFireId;
	private Vector3f direction;
	private long shipId;
	private String weaponSlotsContainerName;

	public WeaponFireCreationMessage() {
		super(true);
	}

	public WeaponFireCreationMessage(Vector3f startLocation, long weaponFireId, Vector3f direction, long shipId, String weaponSlotsContainerName) {
		this();
		this.startLocation = startLocation;
		this.weaponFireId = weaponFireId;
		this.direction = direction;
		this.shipId = shipId;
		this.weaponSlotsContainerName = weaponSlotsContainerName;
	}

	public Vector3f getStartLocation() {
		return startLocation;
	}

	public void setStartLocation(Vector3f startLocation) {
		this.startLocation = startLocation;
	}

	public long getWeaponFireId() {
		return weaponFireId;
	}

	public void setWeaponFireId(long weaponFireId) {
		this.weaponFireId = weaponFireId;
	}

	public Vector3f getDirection() {
		return direction;
	}

	public void setDirection(Vector3f direction) {
		this.direction = direction;
	}

	public long getShipId() {
		return shipId;
	}

	public void setShipId(long shipId) {
		this.shipId = shipId;
	}

	public String getWeaponSlotsContainerName() {
		return weaponSlotsContainerName;
	}

	public void setWeaponSlotsContainerName(String weaponSlotsContainerName) {
		this.weaponSlotsContainerName = weaponSlotsContainerName;
	}
}