package org.sunless.shared.network.messages.toserver.input;

import com.jme3.math.Vector3f;
import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class FireWeaponsMessage extends AbstractMessage {

	private Vector3f direction;
	private String weaponSlotsContainerName;

	public FireWeaponsMessage() {
		super(true);
	}

	public FireWeaponsMessage(Vector3f direction, String weaponSlotsContainerName) {
		this();
		this.direction = direction;
		this.weaponSlotsContainerName = weaponSlotsContainerName;
	}

	public Vector3f getDirection() {
		return direction;
	}

	public void setDirection(Vector3f direction) {
		this.direction = direction;
	}

	public String getWeaponSlotsContainerName() {
		return weaponSlotsContainerName;
	}

	public void setWeaponSlotsContainerName(String weaponSlotsContainerName) {
		this.weaponSlotsContainerName = weaponSlotsContainerName;
	}
}