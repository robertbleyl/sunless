package org.sunless.shared.network.messages.toclient.gameplay;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class PlayerPropertyChangeMessage extends AbstractMessage {
	
	private long playerId;
	private String propertyName;
	private Object value;
	
	public PlayerPropertyChangeMessage() {
		super(true);
	}
	
	public PlayerPropertyChangeMessage(long playerId, String propertyName, Object value) {
		this();
		this.playerId = playerId;
		this.propertyName = propertyName;
		this.value = value;
	}
	
	public long getPlayerId() {
		return playerId;
	}
	
	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}
	
	public String getPropertyName() {
		return propertyName;
	}
	
	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}
	
	public Object getValue() {
		return value;
	}
	
	public void setValue(Object value) {
		this.value = value;
	}
}