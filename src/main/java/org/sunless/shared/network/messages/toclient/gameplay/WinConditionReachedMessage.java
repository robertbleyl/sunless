package org.sunless.shared.network.messages.toclient.gameplay;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class WinConditionReachedMessage extends AbstractMessage {
	
	private short teamId;
	
	public WinConditionReachedMessage() {
		super(true);
	}
	
	public WinConditionReachedMessage(short teamId) {
		this();
		this.teamId = teamId;
	}
	
	public short getTeamId() {
		return teamId;
	}
	
	public void setTeamId(short teamId) {
		this.teamId = teamId;
	}
}