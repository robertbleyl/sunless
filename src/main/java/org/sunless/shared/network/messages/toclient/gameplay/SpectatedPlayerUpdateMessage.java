package org.sunless.shared.network.messages.toclient.gameplay;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class SpectatedPlayerUpdateMessage extends AbstractMessage {

	private long spectatedPlayerId;
	private long targetPlayerId;

	public SpectatedPlayerUpdateMessage() {
		super(false);
	}

	public SpectatedPlayerUpdateMessage(long spectatedPlayerId, long targetPlayerId) {
		this();
		this.spectatedPlayerId = spectatedPlayerId;
		this.targetPlayerId = targetPlayerId;
	}

	public long getSpectatedPlayerId() {
		return spectatedPlayerId;
	}

	public void setSpectatedPlayerId(long spectatedPlayerId) {
		this.spectatedPlayerId = spectatedPlayerId;
	}

	public long getTargetPlayerId() {
		return targetPlayerId;
	}

	public void setTargetPlayerId(long targetPlayerId) {
		this.targetPlayerId = targetPlayerId;
	}
}