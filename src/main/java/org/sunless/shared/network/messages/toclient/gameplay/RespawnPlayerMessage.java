package org.sunless.shared.network.messages.toclient.gameplay;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;

@Serializable
public class RespawnPlayerMessage extends AbstractMessage {

	private long playerId;

	public RespawnPlayerMessage() {
		super(true);
	}

	public RespawnPlayerMessage(long playerId) {
		this();
		this.playerId = playerId;
	}

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}
}