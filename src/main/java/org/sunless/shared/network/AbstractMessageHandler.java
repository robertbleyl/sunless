package org.sunless.shared.network;

import org.sunless.shared.entity.EntityAttacher;
import org.sunless.shared.entity.EntityFactory;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.loading.EnqueueHelper;

import com.google.inject.Inject;
import com.jme3.network.Message;

public abstract class AbstractMessageHandler<M extends Message, S> {

	protected final EventBus eventBus = EventBus.get();

	protected EntityFactory entityFactory;
	protected TeamsContainer teamsContainer;
	protected EnqueueHelper enqueueHelper;
	protected EntityAttacher entityAttacher;

	protected boolean enabled = true;

	public abstract Class<M> getMessageClass();

	public abstract void handleMessage(S source, M m);

	public void noPlayerForConnection(int connectionId) {
		throw new RuntimeException("The player from connection id " + connectionId + " was not found!");
	}

	public boolean isEnabled() {
		return enabled;
	}

	@Inject
	public void setEntityFactory(EntityFactory entityFactory) {
		this.entityFactory = entityFactory;
	}

	@Inject
	public void setTeamsContainer(TeamsContainer teamsContainer) {
		this.teamsContainer = teamsContainer;
	}

	@Inject
	public void setEnqueueHelper(EnqueueHelper enqueueHelper) {
		this.enqueueHelper = enqueueHelper;
	}

	@Inject
	public void setEntityAttacher(EntityAttacher entityAttacher) {
		this.entityAttacher = entityAttacher;
	}
}