package org.sunless.shared.json;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import org.sunless.shared.entity.EntityConstants;

import com.jme3.math.Vector3f;

public class MapData implements Serializable {

	private static final long serialVersionUID = -4682840672649374484L;

	private Faction faction1;
	private Faction faction2;
	private String skyBox;
	private Position lightDirection;
	private Position initCameraPosition;
	private Position initCameraDirection;
	private List<WorldObjectData> objects;

	private transient List<Vector3f> objectives;

	public Faction getFaction1() {
		return faction1;
	}

	public void setFaction1(Faction faction1) {
		this.faction1 = faction1;
	}

	public Faction getFaction2() {
		return faction2;
	}

	public void setFaction2(Faction faction2) {
		this.faction2 = faction2;
	}

	public String getSkyBox() {
		return skyBox;
	}

	public void setSkyBox(String skyBox) {
		this.skyBox = skyBox;
	}

	public Position getLightDirection() {
		return lightDirection;
	}

	public void setLightDirection(Position lightDirection) {
		this.lightDirection = lightDirection;
	}

	public Position getInitCameraPosition() {
		return initCameraPosition;
	}

	public void setInitCameraPosition(Position initCameraPosition) {
		this.initCameraPosition = initCameraPosition;
	}

	public Position getInitCameraDirection() {
		return initCameraDirection;
	}

	public void setInitCameraDirection(Position initCameraDirection) {
		this.initCameraDirection = initCameraDirection;
	}

	public List<WorldObjectData> getObjects() {
		return objects;
	}

	public void setObjects(List<WorldObjectData> objects) {
		this.objects = objects;
	}

	public List<Vector3f> retrieveObjectives(GameMode gameMode) {
		if (objectives == null) {
			objectives = objects.parallelStream()
					.filter(o -> o.getData() != null && !o.getData().isEmpty() && !o.getData().containsKey(EntityConstants.batchId) && (o.getGameModes() == null || o.getGameModes().contains(gameMode)))
					.map(o -> o.getLocation().toVector())
					.collect(Collectors.toList());
		}

		return objectives;
	}
}