package org.sunless.shared.json;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.sunless.shared.util.CacheHelper;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.util.DefaultPrettyPrinter;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import net.sf.ehcache.Element;

public class JsonHelper {

	private static JsonHelper instance;

	protected final CacheHelper cacheHelper;
	protected final ObjectMapper mapper;

	protected final ObjectWriter writer;

	public JsonHelper(CacheHelper cacheHelper, ObjectMapper mapper) {
		this.cacheHelper = cacheHelper;
		this.mapper = mapper;

		writer = mapper.writer(new DefaultPrettyPrinter());
	}

	public static JsonHelper get() throws IOException {
		if (instance == null) {
			instance = new JsonHelper(CacheHelper.get(), new ObjectMapper());
		}

		return instance;
	}

	public static void setInstance(JsonHelper xmlHelper) {
		instance = xmlHelper;
	}

	public void writeJsonFile(Object o, String fileName) throws Exception {
		File file = new File(fileName);
		file.createNewFile();
		writer.writeValue(file, o);
	}

	public <T> T toPOJO(String fileName, Class<T> clazz) throws JsonParseException, JsonMappingException, IOException {
		return toPOJO(fileName, clazz, false);
	}

	@SuppressWarnings("unchecked")
	public <T> T toPOJO(String fileName, Class<T> clazz, boolean absolutePath) throws JsonParseException, JsonMappingException, IOException {
		Element element = cacheHelper.getJsonCache().get(fileName);

		if (element == null) {
			T pojo = createNewPojo(fileName, clazz, absolutePath);
			return pojo;
		}

		T pojo = (T)element.getObjectValue();
		return pojo;
	}

	protected <T> T createNewPojo(String fileName, Class<T> clazz, boolean absolutePath) throws JsonParseException, JsonMappingException, IOException {
		if (absolutePath) {
			File file = new File(fileName);

			if (file.exists()) {
				T pojo = mapper.readValue(file, clazz);
				return pojo;
			}

			return null;
		}

		try (InputStream inputStream = getClass().getResourceAsStream(fileName);) {
			if (inputStream != null) {
				T pojo = mapper.readValue(inputStream, clazz);

				cacheHelper.getJsonCache().putIfAbsent(new Element(fileName, pojo));
				return pojo;
			}
		}

		return null;
	}
}