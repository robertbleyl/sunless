package org.sunless.shared.json;

public enum EntityPropertyType {

	STRING, INTEGER, FLOAT, BOOLEAN
}