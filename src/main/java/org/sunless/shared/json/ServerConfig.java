package org.sunless.shared.json;

import java.io.Serializable;
import java.util.List;

public class ServerConfig implements Serializable {

	private static final long serialVersionUID = -565123008019702782L;

	private int mapRotationIndex;
	private List<MapRotationEntry> mapRotationEntries;
	private int maxPlayers;
	private int networkTickRate;
	private int physicsTickRate;
	private boolean enableDebug;
	private boolean enableBots;
	private String aiLevel;
	private List<ServerDebugSetting> debugSettings;

	public int getMapRotationIndex() {
		return mapRotationIndex;
	}

	public void setMapRotationIndex(int mapRotationIndex) {
		this.mapRotationIndex = mapRotationIndex;
	}

	public List<MapRotationEntry> getMapRotationEntries() {
		return mapRotationEntries;
	}

	public void setMapRotationEntries(List<MapRotationEntry> mapRotationEntries) {
		this.mapRotationEntries = mapRotationEntries;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public int getNetworkTickRate() {
		return networkTickRate;
	}

	public void setNetworkTickRate(int networkTickRate) {
		this.networkTickRate = networkTickRate;
	}

	public int getPhysicsTickRate() {
		return physicsTickRate;
	}

	public void setPhysicsTickRate(int physicsTickRate) {
		this.physicsTickRate = physicsTickRate;
	}

	public boolean isEnableDebug() {
		return enableDebug;
	}

	public void setEnableDebug(boolean enableDebug) {
		this.enableDebug = enableDebug;
	}

	public boolean isEnableBots() {
		return enableBots;
	}

	public void setEnableBots(boolean enableBots) {
		this.enableBots = enableBots;
	}

	public String getAiLevel() {
		return aiLevel;
	}

	public void setAiLevel(String aiLevel) {
		this.aiLevel = aiLevel;
	}

	public List<ServerDebugSetting> getDebugSettings() {
		return debugSettings;
	}

	public void setDebugSettings(List<ServerDebugSetting> debugSettings) {
		this.debugSettings = debugSettings;
	}
}