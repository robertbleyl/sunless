package org.sunless.shared.json;

import java.io.Serializable;

public class ShipConfig implements Serializable {
	
	private static final long serialVersionUID = 7576037713942942356L;
	
	private String shipName;
	
	public String getShipName() {
		return shipName;
	}
	
	public void setShipName(String shipName) {
		this.shipName = shipName;
	}
}