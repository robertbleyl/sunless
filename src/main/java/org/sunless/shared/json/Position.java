package org.sunless.shared.json;

import java.io.Serializable;

import com.jme3.math.Vector3f;

public class Position implements Serializable {

	private static final long serialVersionUID = -7326885934711694209L;

	private float x;
	private float y;
	private float z;

	public Position() {

	}

	public Position(Vector3f vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}

	public Position(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public Vector3f toVector() {
		return new Vector3f(x, y, z);
	}
}