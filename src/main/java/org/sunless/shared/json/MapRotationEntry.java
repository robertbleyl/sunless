package org.sunless.shared.json;

import java.io.Serializable;

public class MapRotationEntry implements Serializable {

	private static final long serialVersionUID = -3588310125448321448L;

	private int endTimer;
	private String mapName;
	private int gameTime;
	private float spawnDelay;
	private String gameMode;
	private long initMoney;

	public int getEndTimer() {
		return endTimer;
	}

	public void setEndTimer(int endTimer) {
		this.endTimer = endTimer;
	}

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public int getGameTime() {
		return gameTime;
	}

	public void setGameTime(int gameTime) {
		this.gameTime = gameTime;
	}

	public float getSpawnDelay() {
		return spawnDelay;
	}

	public void setSpawnDelay(float spawnDelay) {
		this.spawnDelay = spawnDelay;
	}

	public String getGameMode() {
		return gameMode;
	}

	public void setGameMode(String gameMode) {
		this.gameMode = gameMode;
	}

	public long getInitMoney() {
		return initMoney;
	}

	public void setInitMoney(long initMoney) {
		this.initMoney = initMoney;
	}

}