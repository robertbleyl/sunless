package org.sunless.shared.json;

import java.io.Serializable;
import java.util.List;

public class EntityProperties implements Serializable {
	
	private static final long serialVersionUID = 6074810480160281573L;
	
	private List<EntityProperty> properties;
	
	public List<EntityProperty> getProperties() {
		return properties;
	}
	
	public void setProperties(List<EntityProperty> properties) {
		this.properties = properties;
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getValue(String key) {
		for (EntityProperty prop : properties) {
			if (prop.getKey().equals(key)) {
				return (T)prop.getTypedValue();
			}
		}
		
		return null;
	}
}