package org.sunless.shared.json;

public enum GameMode {

	TEAM_DEATH_MATCH, CONQUEST, MOTHER_SHIP
}