package org.sunless.shared.json;

public enum Faction {
	
	HUMANS, ALIENS
}