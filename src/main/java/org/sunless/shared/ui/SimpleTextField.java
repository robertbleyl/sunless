package org.sunless.shared.ui;

import com.jme3.input.KeyInput;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;

import tonegod.gui.controls.text.TextField;
import tonegod.gui.core.ElementManager;

public class SimpleTextField extends TextField {

	public SimpleTextField(ElementManager screen) {
		super(screen);
		// TODO Auto-generated constructor stub
	}

	public SimpleTextField(ElementManager screen, Vector2f position) {
		super(screen, position);
		// TODO Auto-generated constructor stub
	}

	public SimpleTextField(ElementManager screen, Vector2f position, Vector2f dimensions) {
		super(screen, position, dimensions);
		// TODO Auto-generated constructor stub
	}

	public SimpleTextField(ElementManager screen, Vector2f position, Vector2f dimensions, Vector4f resizeBorders, String defaultImg) {
		super(screen, position, dimensions, resizeBorders, defaultImg);
		// TODO Auto-generated constructor stub
	}

	public SimpleTextField(ElementManager screen, String UID, Vector2f position) {
		super(screen, UID, position);
		// TODO Auto-generated constructor stub
	}

	public SimpleTextField(ElementManager screen, String UID, Vector2f position, Vector2f dimensions) {
		super(screen, UID, position, dimensions);
		// TODO Auto-generated constructor stub
	}

	public SimpleTextField(ElementManager screen, String UID, Vector2f position, Vector2f dimensions, Vector4f resizeBorders, String defaultImg) {
		super(screen, UID, position, dimensions, resizeBorders, defaultImg);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onKeyPress(KeyInputEvent evt) {
		switch (evt.getKeyCode()) {
			case 16:
				evt.setConsumed();
				return; // 16 means "not set" here
			case KeyInput.KEY_BACK:
				super.onKeyPress(evt);
				return;
			default:
				if (evt.getKeyChar() != 0) {
					super.onKeyPress(evt);
					break;
				}
		}
	}

	@Override
	public void onKeyRelease(KeyInputEvent evt) {
		switch (evt.getKeyCode()) {
			case 16:
				evt.setConsumed();
				return; // 16 means "not set" here
			case KeyInput.KEY_BACK:
				super.onKeyRelease(evt);
				return;
		}

		if (evt.getKeyChar() != 0) {
			super.onKeyRelease(evt);
		}
	}
}
