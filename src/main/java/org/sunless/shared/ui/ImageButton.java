package org.sunless.shared.ui;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.core.ElementManager;

import com.jme3.math.Vector2f;
import com.jme3.math.Vector4f;

public class ImageButton extends ButtonAdapter {
	
	public ImageButton(ElementManager screen, String image) {
		super(screen, new Vector2f(), new Vector2f(), new Vector4f(), image);
		
		setButtonHoverInfo(null, null);
		setButtonPressedInfo(null, null);
	}
}