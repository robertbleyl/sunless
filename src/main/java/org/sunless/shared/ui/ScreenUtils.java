package org.sunless.shared.ui;

import com.jme3.math.Vector2f;

import tonegod.gui.core.ElementManager;

public class ScreenUtils {

	private static float widgetWidth = -1;
	private static float widgetHeight = -1;
	private static float padding = -1;

	public static float getWidgetHeight(ElementManager screen) {
		if (widgetHeight == -1) {
			widgetHeight = screen.getHeight() / 36;
		}

		return widgetHeight;
	}

	public static float getWidgetWidth(ElementManager screen) {
		if (widgetWidth == -1) {
			widgetWidth = screen.getWidth() / 7;
		}

		return widgetWidth;
	}

	public static float getPadding(ElementManager screen) {
		if (padding == -1) {
			padding = screen.getHeight() / 48;
		}

		return padding;
	}

	public static Vector2f createWidgetSize(ElementManager screen) {
		float width = getWidgetWidth(screen);
		float height = getWidgetHeight(screen);

		return new Vector2f(width, height);
	}

	public static void reset() {
		widgetWidth = -1;
		widgetHeight = -1;
		padding = -1;
	}
}