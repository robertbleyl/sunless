package org.sunless.shared.gameplay;

import org.sunless.shared.NodeConstants;
import org.sunless.shared.state.AppStateImpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.scene.Node;

@Singleton
public class IngameStateImpl extends AppStateImpl implements IngameState {

	@Inject
	public IngameStateImpl(@Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(appRootNode, appGuiNode);
	}
}
