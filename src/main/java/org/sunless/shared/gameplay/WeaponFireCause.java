package org.sunless.shared.gameplay;

import org.sunless.shared.entity.EntityHelper;

import com.jme3.scene.Spatial;

public class WeaponFireCause implements EntityPropertyChangeCause {

	private final Spatial weaponFire;

	public WeaponFireCause(Spatial weaponFire) {
		this.weaponFire = weaponFire;
	}

	public Spatial getWeaponFire() {
		return weaponFire;
	}

	@Override
	public WeaponFireEntityPropertyCauseSync createCauseForMessage() {
		return new WeaponFireEntityPropertyCauseSync(EntityHelper.getEntityId(weaponFire));
	}
}