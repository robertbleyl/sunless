package org.sunless.shared.gameplay;

import java.util.HashMap;
import java.util.Map;

import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.json.Faction;

import com.google.inject.Singleton;

@Singleton
public class TeamsContainer {

	private Team team1;
	private Team team2;

	private final Map<Faction, Team> teamsMap = new HashMap<>(2);
	private final Map<Short, Team> teamIdMap = new HashMap<>(2);

	public Team getTeam1() {
		return team1;
	}

	public void setTeam1(Team team1) {
		this.team1 = team1;
		teamsMap.put(team1.getFaction(), team1);
		teamIdMap.put(team1.getTeamId(), team1);
	}

	public Team getTeam2() {
		return team2;
	}

	public void setTeam2(Team team2) {
		this.team2 = team2;
		teamsMap.put(team2.getFaction(), team2);
		teamIdMap.put(team2.getTeamId(), team2);
	}

	public Team getTeam(Faction faction) {
		return teamsMap.get(faction);
	}

	public Team getEnemyTeam(Team team) {
		if (team == team1) {
			return team2;
		}

		return team1;
	}

	public Team getTeam(short teamId) {
		return teamIdMap.get(teamId);
	}

	public Team getEnemyTeam(short teamId) {
		Team team = teamIdMap.get(teamId);
		return getEnemyTeam(team);
	}

	public void clear() {
		team1 = null;
		team2 = null;
		teamsMap.clear();
		teamIdMap.clear();
	}

	public Team getTeamForNextPlayer() {
		int playerCount1 = team1.getPlayerCount();
		int playerCount2 = team2.getPlayerCount();

		return playerCount1 < playerCount2 ? team1 : team2;
	}
}