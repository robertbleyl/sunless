package org.sunless.shared.gameplay;

import com.jme3.scene.Spatial;

public class CollisionCause implements EntityPropertyChangeCause {

	private final Spatial collidingObject;

	public CollisionCause(Spatial collidingObject) {
		this.collidingObject = collidingObject;
	}

	public Spatial getCollidingObject() {
		return collidingObject;
	}

	@Override
	public CollisionEntityPropertyCauseSync createCauseForMessage() {
		return null;
	}
}