package org.sunless.shared.gameplay;

import org.sunless.shared.network.sync.AbstractSyncState;

public interface EntityPropertyCauseSync {

	EntityPropertyChangeCause createCollisionCause(AbstractSyncState syncState);
}