package org.sunless.shared.gameplay;

import com.google.inject.Singleton;

@Singleton
public class GameInfoProvider {

	private GameInfo gameInfo;

	public GameInfo getGameInfo() {
		return gameInfo;
	}

	public void setGameInfo(GameInfo gameInfo) {
		this.gameInfo = gameInfo;
	}
}