package org.sunless.shared.gameplay;

import org.sunless.shared.network.sync.AbstractSyncState;

import com.jme3.network.serializing.Serializable;

@Serializable
public class WeaponFireEntityPropertyCauseSync implements EntityPropertyCauseSync {

	private long weaponFireId;

	public WeaponFireEntityPropertyCauseSync() {

	}

	public WeaponFireEntityPropertyCauseSync(long weaponFireId) {
		this.weaponFireId = weaponFireId;
	}

	public long getWeaponFireId() {
		return weaponFireId;
	}

	public void setWeaponFireId(long weaponFireId) {
		this.weaponFireId = weaponFireId;
	}

	@Override
	public WeaponFireCause createCollisionCause(AbstractSyncState syncState) {
		return new WeaponFireCause(syncState.getEntity(weaponFireId));
	}
}