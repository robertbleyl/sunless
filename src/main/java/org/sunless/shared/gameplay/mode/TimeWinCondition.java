package org.sunless.shared.gameplay.mode;

import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.gameplay.WinConditionReachedEvent;
import org.sunless.shared.event.main.GameTimeUpdateEvent;
import org.sunless.shared.event.main.GameTimeUpdateHandler;
import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.TeamsContainer;

public class TimeWinCondition implements WinCondition {

	private final EventBus eventBus = EventBus.get();

	private final TeamsContainer teamsContainer;
	private final GameInfo gameInfo;
	private final GameTimeUpdateHandler handler;

	private boolean winConditionReached;

	public TimeWinCondition(TeamsContainer teamsContainer, GameInfo gameInfo) {
		this.teamsContainer = teamsContainer;
		this.gameInfo = gameInfo;

		handler = new GameTimeUpdateHandler() {
			@Override
			public void onGameTimeUpdate(GameTimeUpdateEvent event) {
				check(event);
			}
		};
		eventBus.addHandler(GameTimeUpdateEvent.TYPE, handler);
	}

	protected void check(GameTimeUpdateEvent event) {
		if (!winConditionReached && event.getCurrentTime() >= gameInfo.getGameTime()) {
			winConditionReached = true;
			eventBus.fireEvent(new WinConditionReachedEvent(teamsContainer.getTeam1()));
			eventBus.removeHandler(handler, GameTimeUpdateEvent.TYPE);
		}
	}
}