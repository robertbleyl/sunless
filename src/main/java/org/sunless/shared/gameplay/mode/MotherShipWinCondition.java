package org.sunless.shared.gameplay.mode;

import org.apache.commons.lang3.BooleanUtils;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.gameplay.EntityPropertyChangeEvent;
import org.sunless.shared.event.gameplay.EntityPropertyChangeHandler;
import org.sunless.shared.event.gameplay.WinConditionReachedEvent;
import org.sunless.shared.gameplay.player.Team;

import com.jme3.scene.Spatial;

public class MotherShipWinCondition implements WinCondition {

	private final EventBus eventBus = EventBus.get();

	private final EntityPropertyChangeHandler handler;

	public MotherShipWinCondition() {
		handler = new EntityPropertyChangeHandler() {
			@Override
			public void onChange(EntityPropertyChangeEvent event) {
				check(event);
			}
		};
		eventBus.addHandler(EntityPropertyChangeEvent.TYPE, handler);
	}

	private void check(EntityPropertyChangeEvent event) {
		Spatial entity = event.getEntity();
		Boolean isMotherShip = entity.getUserData(EntityConstants.isMotherShip);

		if (BooleanUtils.isTrue(isMotherShip)) {
			String propertyName = event.getPropertyName();

			if (propertyName.equals(EntityConstants.hitPoints)) {
				int hitPoints = (int)event.getPropertyValue();

				if (hitPoints <= 0) {
					Team team = entity.getUserData(EntityConstants.team);

					eventBus.fireEvent(new WinConditionReachedEvent(team));
					eventBus.removeHandler(handler, EntityPropertyChangeEvent.TYPE);
				}
			}
		}
	}
}