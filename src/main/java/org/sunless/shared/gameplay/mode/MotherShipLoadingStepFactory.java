package org.sunless.shared.gameplay.mode;

import java.util.List;
import java.util.Map;

import org.sunless.shared.entity.EntityAttacher;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityFactory;
import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.json.Faction;
import org.sunless.shared.json.FactionConfig;
import org.sunless.shared.json.MapData;
import org.sunless.shared.loading.GameModeLoadingStepFactory;
import org.sunless.shared.loading.LoadingStep;

import com.jme3.scene.Spatial;

public class MotherShipLoadingStepFactory implements GameModeLoadingStepFactory {

	protected final MapData mapData;
	protected final EntityFactory entityFactory;
	protected final TeamsContainer gameDataContainer;
	protected final Map<Faction, FactionConfig> factionConfigs;
	protected final EntityAttacher entityAttacher;
	protected final GameInfo gameInfo;

	public MotherShipLoadingStepFactory(MapData mapData, EntityFactory entityFactory, TeamsContainer gameDataContainer, Map<Faction, FactionConfig> factionConfigs, EntityAttacher entityAttacher,
			GameInfo gameInfo) {
		this.mapData = mapData;
		this.entityFactory = entityFactory;
		this.gameDataContainer = gameDataContainer;
		this.factionConfigs = factionConfigs;
		this.entityAttacher = entityAttacher;
		this.gameInfo = gameInfo;
	}

	@Override
	public void addLoadingSteps(List<LoadingStep> steps) {
		steps.add(new LoadingStep(15f, true) {
			@Override
			public void load() throws Exception {
				initMotherShip(gameDataContainer.getTeam1());
				initMotherShip(gameDataContainer.getTeam2());
			}
		});
	}

	protected void initMotherShip(Team team) throws Exception {
		Spatial motherShip = team.getMotherShip();
		long id = gameInfo.getObjectIds().get(motherShip.getLocalTranslation());
		motherShip.setUserData(EntityConstants.id, id);
	}
}