package org.sunless.shared.gameplay;

import org.sunless.shared.json.Faction;

import com.jme3.network.serializing.Serializable;

@Serializable
public class GameInfoInitData {

	private long playerId;
	private GameInfo gameInfo;
	private Faction faction;

	private String[] playerNames;
	private String[] playerShipNames;
	private Faction[] playerFactions;
	private long[] playerShipIds;
	private long[] playerIds;

	private long[] playerSpawnLocationEntityIds;

	public GameInfoInitData() {

	}

	public GameInfoInitData(GameInfo gameInfo, Faction faction, String[] playerNames, String[] playerShipNames, Faction[] playerFactions, long[] playerShipIds, long[] playerIds, long[] playerSpawnLocationEntityIds) {
		this.gameInfo = gameInfo;
		this.faction = faction;
		this.playerNames = playerNames;
		this.playerShipNames = playerShipNames;
		this.playerFactions = playerFactions;
		this.playerShipIds = playerShipIds;
		this.playerIds = playerIds;
		this.playerSpawnLocationEntityIds = playerSpawnLocationEntityIds;
	}

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public GameInfo getGameInfo() {
		return gameInfo;
	}

	public void setGameInfo(GameInfo gameInfo) {
		this.gameInfo = gameInfo;
	}

	public Faction getFaction() {
		return faction;
	}

	public void setFaction(Faction faction) {
		this.faction = faction;
	}

	public String[] getPlayerNames() {
		return playerNames;
	}

	public void setPlayerNames(String[] playerNames) {
		this.playerNames = playerNames;
	}

	public String[] getPlayerShipNames() {
		return playerShipNames;
	}

	public void setPlayerShipNames(String[] playerShipNames) {
		this.playerShipNames = playerShipNames;
	}

	public Faction[] getPlayerFactions() {
		return playerFactions;
	}

	public void setPlayerFactions(Faction[] playerFactions) {
		this.playerFactions = playerFactions;
	}

	public long[] getPlayerShipIds() {
		return playerShipIds;
	}

	public void setPlayerShipIds(long[] playerShipIds) {
		this.playerShipIds = playerShipIds;
	}

	public long[] getPlayerIds() {
		return playerIds;
	}

	public void setPlayerIds(long[] playerIds) {
		this.playerIds = playerIds;
	}

	public long[] getPlayerSpawnLocationEntityIds() {
		return playerSpawnLocationEntityIds;
	}

	public void setPlayerSpawnLocationEntityIds(long[] playerSpawnLocationEntityIds) {
		this.playerSpawnLocationEntityIds = playerSpawnLocationEntityIds;
	}
}