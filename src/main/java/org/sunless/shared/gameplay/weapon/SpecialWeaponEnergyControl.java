package org.sunless.shared.gameplay.weapon;

import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityHelper;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class SpecialWeaponEnergyControl extends AbstractControl {

	private float currentRegenStep;

	@Override
	protected void controlUpdate(float tpf) {
		float currentEnergy = spatial.getUserData(EntityConstants.specialWeaponEnergy);
		float maxEnergy = spatial.getUserData(EntityConstants.maxSpecialWeaponEnergy);

		if (currentEnergy < maxEnergy) {
			float regen = spatial.getUserData(EntityConstants.specialWeaponEnergyRegeneration);

			currentRegenStep += regen * tpf;

			if (currentRegenStep >= 1f) {
				currentRegenStep -= 1f;

				float newEnergy = currentEnergy + 1f;

				EntityHelper.changeProperty(spatial, EntityConstants.specialWeaponEnergy, newEnergy, null);
			}
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}