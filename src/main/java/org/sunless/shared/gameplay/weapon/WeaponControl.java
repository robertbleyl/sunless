package org.sunless.shared.gameplay.weapon;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class WeaponControl extends AbstractControl {

	private final String coolDownPropertyKey;

	private float timer;
	private boolean canFire;

	public WeaponControl(String coolDownPropertyKey) {
		this.coolDownPropertyKey = coolDownPropertyKey;
	}

	@Override
	protected void controlUpdate(float tpf) {
		if (!canFire) {
			timer += tpf;

			float coolDown = spatial.getParent().getUserData(coolDownPropertyKey);
			canFire = timer >= coolDown;
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}

	public void weaponFired() {
		timer = 0f;
		canFire = false;
	}

	public boolean canFire() {
		return canFire;
	}
}