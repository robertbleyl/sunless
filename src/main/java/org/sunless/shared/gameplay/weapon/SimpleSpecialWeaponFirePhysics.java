package org.sunless.shared.gameplay.weapon;

import org.sunless.shared.entity.EntityConstants;

import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class SimpleSpecialWeaponFirePhysics extends WeaponFirePhysics {

	public SimpleSpecialWeaponFirePhysics(String prefix) {
		super(prefix);
	}

	@Override
	public CollisionShape createCollisionShape(Node weaponFire, Spatial firingShip) {
		float size = firingShip.getUserData(prefix + EntityConstants.weaponFireSize);
		CollisionShape shape = new SphereCollisionShape(size);
		return shape;
	}
}