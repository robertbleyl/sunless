package org.sunless.shared.gameplay.weapon;

import java.util.List;

import org.sunless.shared.entity.EntityConstants;

import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class LightningWeaponFirePhysics extends WeaponFirePhysics {

	public LightningWeaponFirePhysics() {
		super("");
	}

	@Override
	public CollisionShape createCollisionShape(Node weaponFire, Spatial firingShip) {
		float size = firingShip.getUserData(EntityConstants.special_weaponFireSize);
		List<Vector3f> lightningSlots = firingShip.getUserData(EntityConstants.lightningSlots);

		Vector3f a = lightningSlots.get(0);
		Vector3f b = lightningSlots.get(1);

		float x = firingShip.localToWorld(b, new Vector3f()).subtract(firingShip.localToWorld(a, new Vector3f())).length() / 2f;
		float y = size / 2f;
		float z = y;

		CollisionShape shape = new BoxCollisionShape(new Vector3f(x, y, z));

		return shape;
	}
}