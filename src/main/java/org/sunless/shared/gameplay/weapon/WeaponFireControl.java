package org.sunless.shared.gameplay.weapon;

import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class WeaponFireControl extends AbstractControl {

	protected final float speed;
	protected final Vector3f direction;

	public WeaponFireControl(float speed, Vector3f direction) {
		this.speed = speed;
		this.direction = direction;
	}

	@Override
	protected void controlUpdate(float tpf) {

	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}

	public Vector3f getDirection() {
		return direction;
	}
}