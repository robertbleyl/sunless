package org.sunless.shared.gameplay.weapon;

import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityHelper;

public class SpecialWeaponControl extends WeaponControl {

	public SpecialWeaponControl(String coolDownPropertyKey) {
		super(coolDownPropertyKey);
	}

	@Override
	public void weaponFired() {
		super.weaponFired();

		float energy = spatial.getParent().getUserData(EntityConstants.specialWeaponEnergy);
		float drop = spatial.getParent().getUserData(EntityConstants.specialWeaponEnergyPerWeaponFire);

		energy -= drop;

		if (energy < 0f) {
			energy = 0f;
		}

		EntityHelper.changeProperty(spatial.getParent(), EntityConstants.specialWeaponEnergy, energy, null);
	}

	@Override
	public boolean canFire() {
		float energy = spatial.getParent().getUserData(EntityConstants.specialWeaponEnergy);
		float drop = spatial.getParent().getUserData(EntityConstants.specialWeaponEnergyPerWeaponFire);
		return super.canFire() && energy >= drop;
	}
}