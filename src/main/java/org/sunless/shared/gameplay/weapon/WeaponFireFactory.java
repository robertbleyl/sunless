package org.sunless.shared.gameplay.weapon;

import org.sunless.client.BoundingVolumeDisplayControl;
import org.sunless.shared.config.DebugConfigConstants;
import org.sunless.shared.entity.CollisionGroups;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.util.DebugState;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public abstract class WeaponFireFactory {

	protected final String prefix;
	protected final AssetManager assetManager;
	protected final WeaponFirePhysics physics;
	protected final String weaponFireType;

	public WeaponFireFactory(String prefix, AssetManager assetManager, WeaponFirePhysics physics, String weaponFireType) {
		this.prefix = prefix;
		this.assetManager = assetManager;
		this.physics = physics;
		this.weaponFireType = weaponFireType;
	}

	public String getWeaponFireFactoryId() {
		return weaponFireType;
	}

	protected boolean showPaths() {
		return DebugState.get().isToggled(DebugConfigConstants.dbg_toggle_show_weapon_paths);
	}

	protected float getSize(Spatial firingShip) {
		return firingShip.getUserData(prefix + EntityConstants.weaponFireSize);
	}

	public Node createWeaponFire(WeaponFireCreationData data) throws Exception {
		Spatial firingShip = data.getFiringShip();

		Node weaponFire = new Node();
		weaponFire.setLocalTranslation(data.getStartPoint());
		weaponFire.setUserData(EntityConstants.weaponFireType, weaponFireType);

		weaponFire.setUserData(EntityConstants.weaponFireFiringShip, firingShip);
		weaponFire.setUserData(EntityConstants.sync, true);

		CollisionShape shape = physics.createCollisionShape(weaponFire, firingShip);

		RigidBodyControl control = new RigidBodyControl(shape, 0.0001f);
		control.setCollisionGroup(CollisionGroups.WEAPON_FIRE);
		control.removeCollideWithGroup(CollisionGroups.LARGE_OBJECTS);
		control.removeCollideWithGroup(CollisionGroups.SMALL_SHIPS);
		control.removeCollideWithGroup(CollisionGroups.WEAPON_FIRE);
		control.setFriction(50f);
		control.setAngularDamping(70f);
		control.setRestitution(0f);
		weaponFire.addControl(control);

		float weaponFireSpeed = firingShip.getUserData(prefix + EntityConstants.weaponFireSpeed);
		control.setLinearVelocity(data.getDirection().normalize().mult(weaponFireSpeed));

		if (DebugState.get().isToggled(DebugConfigConstants.dbg_toggle_show_weaponfire_bounding_volumes)) {
			weaponFire.addControl(new BoundingVolumeDisplayControl(assetManager, DebugConfigConstants.dbg_toggle_show_weaponfire_bounding_volumes));
		}

		return weaponFire;
	}
}