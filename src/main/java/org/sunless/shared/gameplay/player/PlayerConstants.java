package org.sunless.shared.gameplay.player;

public interface PlayerConstants {

	String MONEY = "MONEY";
	String KILLS = "KILLS";
	String DEATHS = "DEATHS";
	String SPAWN_POINT = "SPAWN_POINT";
	String TEAM = "TEAM";
}