package org.sunless.shared.gameplay.player;

public interface PlayerIdProvider {
	
	int generatePlayerId();
}