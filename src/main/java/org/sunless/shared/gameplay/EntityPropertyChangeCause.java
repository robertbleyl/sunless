package org.sunless.shared.gameplay;

public interface EntityPropertyChangeCause {

	EntityPropertyCauseSync createCauseForMessage();
}