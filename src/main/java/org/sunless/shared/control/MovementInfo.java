package org.sunless.shared.control;

import com.jme3.network.serializing.Serializable;

@Serializable
public class MovementInfo {

	private float forwardSpeedPercentage;
	private boolean boosting;
	private boolean strafeLeft;
	private boolean strafeRight;
	private boolean rollLeft;
	private boolean rollRight;
	private float rotationPercentageX;
	private float rotationPercentageY;

	public MovementInfo() {

	}

	public void apply(MovementInfo o) {
		forwardSpeedPercentage = o.forwardSpeedPercentage;
		boosting = o.boosting;
		strafeLeft = o.strafeLeft;
		strafeRight = o.strafeRight;
		rollLeft = o.rollLeft;
		rollRight = o.rollRight;
		rotationPercentageX = o.rotationPercentageX;
		rotationPercentageY = o.rotationPercentageY;
	}

	public float getForwardSpeedPercentage() {
		return forwardSpeedPercentage;
	}

	public void setForwardSpeedPercentage(float forwardSpeedPercentage) {
		this.forwardSpeedPercentage = forwardSpeedPercentage;
	}

	public boolean isBoosting() {
		return boosting;
	}

	public void setBoosting(boolean boosting) {
		this.boosting = boosting;
	}

	public boolean isStrafeLeft() {
		return strafeLeft;
	}

	public void setStrafeLeft(boolean strafeLeft) {
		this.strafeLeft = strafeLeft;
	}

	public boolean isStrafeRight() {
		return strafeRight;
	}

	public void setStrafeRight(boolean strafeRight) {
		this.strafeRight = strafeRight;
	}

	public boolean isRollLeft() {
		return rollLeft;
	}

	public void setRollLeft(boolean rollLeft) {
		this.rollLeft = rollLeft;
	}

	public boolean isRollRight() {
		return rollRight;
	}

	public void setRollRight(boolean rollRight) {
		this.rollRight = rollRight;
	}

	public float getRotationPercentageX() {
		return rotationPercentageX;
	}

	public void setRotationPercentageX(float rotationPercentageX) {
		this.rotationPercentageX = rotationPercentageX;
	}

	public float getRotationPercentageY() {
		return rotationPercentageY;
	}

	public void setRotationPercentageY(float rotationPercentageY) {
		this.rotationPercentageY = rotationPercentageY;
	}
}