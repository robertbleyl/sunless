package org.sunless.client.ui.hud;

import org.sunless.shared.loading.LoadingBar;
import org.sunless.shared.ui.EmptyContainer;

import tonegod.gui.core.ElementManager;

public class ShipStatusPanel extends EmptyContainer {

	private LoadingBar barHitPoints;
	private LoadingBar barShields;
	private LoadingBar barSpeed;
	private LoadingBar barSpecialWeaponEnergy;
	private LoadingBar barBoostEnergy;

	public ShipStatusPanel(ElementManager screen, float width, float height) {
		super(screen);

		setDimensions(width, height);

		init();
	}

	private void init() {
		float widgetHeight = getHeight() / 3f;
		float x = 0f;
		float barY = getHeight() - widgetHeight;

		barShields = new LoadingBar(screen, getWidth(), widgetHeight, "blueBar.png");
		barShields.setPosition(x, barY);
		barShields.updateProgress(1f);
		addChild(barShields);

		barY -= widgetHeight;

		barHitPoints = new LoadingBar(screen, getWidth(), widgetHeight, "redBar.png");
		barHitPoints.setPosition(x, barY);
		barHitPoints.updateProgress(1f);
		addChild(barHitPoints);

		barY -= widgetHeight;

		barSpeed = new LoadingBar(screen, getWidth(), widgetHeight, "greyBar.png");
		barSpeed.setPosition(x, barY);
		barSpeed.updateProgress(1f);
		addChild(barSpeed);

		barY -= widgetHeight;

		barSpecialWeaponEnergy = new LoadingBar(screen, getWidth(), widgetHeight, "greenBar.png");
		barSpecialWeaponEnergy.setPosition(x, barY);
		barSpecialWeaponEnergy.updateProgress(1f);
		addChild(barSpecialWeaponEnergy);

		barY -= widgetHeight;

		barBoostEnergy = new LoadingBar(screen, getWidth(), widgetHeight, "greyBar.png");
		barBoostEnergy.setPosition(x, barY);
		barBoostEnergy.updateProgress(1f);
		addChild(barBoostEnergy);
	}

	public void setShieldsPercentage(float percentage) {
		barShields.updateProgress(percentage);
	}

	public void setHitPointsPercentage(float percentage) {
		barHitPoints.updateProgress(percentage);
	}

	public void setSpeedPercentage(float percentage) {
		barSpeed.updateProgress(percentage);
	}

	public void setSpecialWeaponEnergyPercentage(float percentage) {
		barSpecialWeaponEnergy.updateProgress(percentage);
	}

	public void setBoostEnergyPercentage(float percentage) {
		barBoostEnergy.updateProgress(percentage);
	}
}