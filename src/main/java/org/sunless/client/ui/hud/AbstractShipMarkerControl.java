package org.sunless.client.ui.hud;

import org.sunless.client.event.gameplay.UpdateNearestTargetEvent;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.Team;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public abstract class AbstractShipMarkerControl<T extends Spatial> extends AbstractControl {

	protected final T pictureSpatial;
	protected final Node guiNode;
	protected final Camera camera;
	protected final AssetManager assetManager;
	protected final Player player;

	protected Player spectatedPlayer;

	protected boolean isSameTeam;

	protected final Vector3f screenPos = new Vector3f();

	public AbstractShipMarkerControl(T pictureSpatial, Node guiNode, Camera camera, Player player, Player localPlayer, AssetManager assetManager) {
		this.pictureSpatial = pictureSpatial;
		this.guiNode = guiNode;
		this.camera = camera;
		this.player = player;
		this.spectatedPlayer = localPlayer;
		this.assetManager = assetManager;
	}

	protected abstract void update(Vector3f spatialPos);

	protected abstract boolean show();

	public abstract void onUpdateNearestTarget(UpdateNearestTargetEvent event);

	@Override
	protected void controlUpdate(float tpf) {
		Team localTeam = spectatedPlayer.getTeam();
		Team team = player.getTeam();

		isSameTeam = team == localTeam;

		if (pictureSpatial.getParent() != null && spatial.getParent() == null) {
			pictureSpatial.removeFromParent();
		} else {
			Vector3f position = spatial.getLocalTranslation();
			camera.getScreenCoordinates(position, screenPos);

			if (show()) {
				if (pictureSpatial.getParent() == null) {
					guiNode.attachChild(pictureSpatial);
				}

				update(position);
			} else {
				pictureSpatial.removeFromParent();
			}
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);

		if (!enabled) {
			pictureSpatial.removeFromParent();
		}
	}

	public void setSpectatedPlayer(Player spectatedPlayer) {
		this.spectatedPlayer = spectatedPlayer;
	}
}