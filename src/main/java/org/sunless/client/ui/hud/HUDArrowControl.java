package org.sunless.client.ui.hud;

import org.sunless.client.event.gameplay.UpdateNearestTargetEvent;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.util.FileConstants;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

public class HUDArrowControl extends AbstractShipMarkerControl<Picture> {

	private final Vector3f screenCenter = new Vector3f();

	private final float DEFAULT_SIZE = 20f;
	private final float TARGET_SIZE = 50f;

	private final Vector3f dir = new Vector3f();
	private final Vector3f pos = new Vector3f();

	private float currentSize = DEFAULT_SIZE;

	public HUDArrowControl(Picture picture, Node guiNode, Camera camera, Player player, Player localPlayer, AssetManager assetManager) {
		super(picture, guiNode, camera, player, localPlayer, assetManager);

		picture.setWidth(currentSize);
		picture.setHeight(currentSize);
	}

	@Override
	public void onUpdateNearestTarget(UpdateNearestTargetEvent event) {
		Player target = event.getTarget();
		boolean isTarget = target != null && target.getShip() == spatial;
		currentSize = isTarget ? TARGET_SIZE : DEFAULT_SIZE;
		String arrow = isTarget ? "targetArrow.png" : "enemyArrow.png";

		pictureSpatial.setImage(assetManager, FileConstants.HUD_PATH + arrow, true);

		pictureSpatial.setWidth(currentSize);
		pictureSpatial.setHeight(currentSize);
	}

	@Override
	protected void update(Vector3f spatialPos) {
		int camWidth = camera.getWidth();
		int camHeight = camera.getHeight();

		float halfCameraWidth = camWidth / 2f;

		screenCenter.set(halfCameraWidth, camHeight / 2f, 0f);

		screenPos.subtract(screenCenter, dir);

		if (screenPos.getZ() > 1f) {
			dir.negateLocal();
		}

		pictureSpatial.rotateUpTo(dir.normalizeLocal());
		dir.multLocal(halfCameraWidth - currentSize);

		screenCenter.add(dir, pos);

		if (pos.x < currentSize) {
			pos.x = currentSize;
		}
		if (pos.x > camWidth - currentSize) {
			pos.x = camWidth - currentSize;
		}

		if (pos.y < currentSize) {
			pos.y = currentSize;
		}
		if (pos.y > camHeight - currentSize) {
			pos.y = camHeight - currentSize;
		}

		pictureSpatial.setPosition(pos.x, pos.y);
	}

	@Override
	protected boolean show() {
		return !isSameTeam && !(screenPos.x > 0f && screenPos.x < camera.getWidth() && screenPos.y > 0f && screenPos.y < camera.getHeight() && screenPos.z < 1f);
	}
}