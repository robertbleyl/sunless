package org.sunless.client.ui.hud;

import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.ui.GUIState;

public interface HUDState extends GUIState {

	void init(Player localPlayer);
}