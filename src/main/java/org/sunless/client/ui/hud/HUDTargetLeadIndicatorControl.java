package org.sunless.client.ui.hud;

import org.sunless.client.event.gameplay.UpdateNearestTargetEvent;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.WeaponSlotContainer;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.util.FileConstants;
import org.sunless.shared.util.TargetLeadCalculator;

import com.jme3.asset.AssetManager;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.scene.Node;
import com.jme3.ui.Picture;

public class HUDTargetLeadIndicatorControl extends AbstractShipMarkerControl<Node> {

	private boolean isTarget;

	private final Picture pictureStandard;
	private final Picture pictureSpecial;

	private final float standardSize;
	private final float hitSize;

	public HUDTargetLeadIndicatorControl(Node pictureSpatial, Node guiNode, Camera camera, Player player, Player localPlayer, AssetManager assetManager) {
		super(pictureSpatial, guiNode, camera, player, localPlayer, assetManager);

		standardSize = camera.getWidth() / 70f;
		hitSize = camera.getWidth() / 50f;

		pictureStandard = new Picture("Target Lead Indicator Standard");
		pictureStandard.setImage(assetManager, FileConstants.HUD_PATH + "targetLeadIndicatorStandard.png", true);
		pictureStandard.setWidth(standardSize);
		pictureStandard.setHeight(standardSize);
		pictureSpatial.attachChild(pictureStandard);

		pictureSpecial = new Picture("Target Lead Indicator Standard");
		pictureSpecial.setImage(assetManager, FileConstants.HUD_PATH + "targetLeadIndicatorSpecial.png", true);
		pictureSpecial.setWidth(standardSize);
		pictureSpecial.setHeight(standardSize);
		pictureSpatial.attachChild(pictureSpecial);
	}

	@Override
	protected void update(Vector3f spatialPos) {
		Node playerShip = spectatedPlayer.getShip();

		if (playerShip != null) {
			updatePicture(EntityConstants.standardWeaponSlots, pictureStandard, EntityConstants.standard_weaponFireSpeed);
			updatePicture(EntityConstants.specialWeaponSlots, pictureSpecial, EntityConstants.special_weaponFireSpeed);
		}
	}

	private void updatePicture(String containerKey, Picture picture, String weaponFireSpeedKey) {
		Node playerShip = spectatedPlayer.getShip();
		WeaponSlotContainer weapons = playerShip.getUserData(containerKey);

		if (weapons != null) {
			Vector3f startLocation = weapons.size() == 2 ? weapons.calculateInBetweenPoint() : weapons.get(0).getWorldTranslation();
			float wfSpeed = playerShip.getUserData(weaponFireSpeedKey);
			Vector3f impactLocation = TargetLeadCalculator.getTargetLeadPosition(startLocation, spatial, wfSpeed);
			Vector3f screenCoordinates = camera.getScreenCoordinates(impactLocation);
			picture.setPosition(screenCoordinates.x, screenCoordinates.y);
		}
	}

	@Override
	protected boolean show() {
		return isTarget && screenPos.getZ() < 1;
	}

	@Override
	public void onUpdateNearestTarget(UpdateNearestTargetEvent event) {
		Player target = event.getTarget();
		isTarget = target != null && target.getShip() == spatial;
	}

	public void registerHit() {
		pictureStandard.setWidth(hitSize);
		pictureStandard.setHeight(hitSize);
		pictureSpecial.setWidth(hitSize);
		pictureSpecial.setHeight(hitSize);

		pictureStandard.addControl(new HUDTargetLeadIndicatorHitControl(1 / 10f, standardSize));
		pictureSpecial.addControl(new HUDTargetLeadIndicatorHitControl(1 / 10f, standardSize));
	}
}