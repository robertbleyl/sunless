package org.sunless.client.ui.hud;

import org.sunless.shared.loading.LoadingBar;
import org.sunless.shared.ui.EmptyContainer;
import org.sunless.shared.ui.ScreenUtils;

import com.jme3.math.Vector2f;

import tonegod.gui.core.ElementManager;

public class CapturePointPanel extends EmptyContainer {

	private LoadingBar barTeam1;
	private LoadingBar barTeam2;

	private long currentCapturePointId;

	public CapturePointPanel(ElementManager screen) {
		super(screen);

		Vector2f widgetSize = ScreenUtils.createWidgetSize(screen);
		setDimensions(widgetSize.y * 2.5f, widgetSize.x * 1.5f);

		init();
	}

	private void init() {
		float widgetWidth = getWidth() / 2f;
		float x = 0f;
		float y = 0f;

		barTeam1 = new LoadingBar(screen, widgetWidth, getHeight(), "blueBar.png", true);
		barTeam1.setPosition(x, y);
		barTeam1.updateProgress(0f);
		addChild(barTeam1);

		x += widgetWidth;

		barTeam2 = new LoadingBar(screen, widgetWidth, getHeight(), "redBar.png", true);
		barTeam2.setPosition(x, y);
		barTeam2.updateProgress(0f);
		addChild(barTeam2);
	}

	public void updatePercentage1(float percentage) {
		barTeam1.updateProgress(percentage);
	}

	public void updatePercentage2(float percentage) {
		barTeam2.updateProgress(percentage);
	}

	public long getCurrentCapturePointId() {
		return currentCapturePointId;
	}

	public void setCurrentCapturePointId(long currentCapturePointId) {
		this.currentCapturePointId = currentCapturePointId;

		setIsVisible(currentCapturePointId > 0L);

		if (!getIsVisible()) {
			barTeam1.updateProgress(0f);
			barTeam2.updateProgress(0f);
		}
	}
}