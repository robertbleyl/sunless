package org.sunless.client.ui.console;

import org.sunless.shared.NodeConstants;
import org.sunless.shared.ui.GUIStateImpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.scene.Node;

import tonegod.gui.core.Screen;

@Singleton
public class ConsoleStateImpl extends GUIStateImpl implements ConsoleState {

	private Console console;

	@Inject
	public ConsoleStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(screen, appRootNode, appGuiNode);

		init();
	}

	@Override
	protected void reinit() {
		super.reinit();

		if (isEnabled()) {
			console.focusTextField(true);
		}
	}

	@Override
	public void init() {
		console = new Console(screen) {
			@Override
			protected void hideConsole() {
				setEnabled(false);
			}
		};
		mainContent.addChild(console);
	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);

		if (console != null) {
			console.focusTextField(enabled);
		}
	}
}