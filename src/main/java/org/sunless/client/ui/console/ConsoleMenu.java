package org.sunless.client.ui.console;

import java.util.List;

import org.sunless.client.config.GameConfig;
import org.sunless.shared.ui.ScreenUtils;

import com.jme3.input.event.KeyInputEvent;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.input.event.MouseMotionEvent;
import com.jme3.math.Vector2f;

import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.windows.Panel;
import tonegod.gui.core.ElementManager;
import tonegod.gui.listeners.KeyboardListener;
import tonegod.gui.listeners.MouseButtonListener;
import tonegod.gui.listeners.MouseFocusListener;
import tonegod.gui.listeners.TabFocusListener;

public class ConsoleMenu extends Panel implements KeyboardListener, TabFocusListener, MouseFocusListener, MouseButtonListener {
	
	public ConsoleMenu(ElementManager screen, List<String> commands) {
		super(screen);
		
		Vector2f labelSize = ScreenUtils.createWidgetSize(screen);
		Vector2f labelPos = new Vector2f();
		
		for (String command : commands) {
			Label label = new Label(screen, labelPos, labelSize);
			label.setText(command + ": " + GameConfig.getString(command));
			addChild(label);
			
			labelPos.y += labelSize.y;
		}
	}
	
	@Override
	public void setTabFocus() {
	}
	
	@Override
	public void resetTabFocus() {
		
	}
	
	@Override
	public void onMouseLeftPressed(MouseButtonEvent evt) {
		
	}
	
	@Override
	public void onMouseLeftReleased(MouseButtonEvent evt) {
		
	}
	
	@Override
	public void onMouseRightPressed(MouseButtonEvent evt) {
		
	}
	
	@Override
	public void onMouseRightReleased(MouseButtonEvent evt) {
		
	}
	
	@Override
	public void onGetFocus(MouseMotionEvent evt) {
		
	}
	
	@Override
	public void onLoseFocus(MouseMotionEvent evt) {
		
	}
	
	@Override
	public void onKeyPress(KeyInputEvent evt) {
		
	}
	
	@Override
	public void onKeyRelease(KeyInputEvent evt) {
		
	}
}