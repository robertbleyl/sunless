package org.sunless.client.ui.console;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.sunless.client.config.GameConfig;
import org.sunless.shared.ui.EmptyContainer;
import org.sunless.shared.ui.ScreenUtils;

import com.jme3.input.KeyInput;
import com.jme3.input.event.KeyInputEvent;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;

import tonegod.gui.controls.scrolling.ScrollPanel;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.text.TextField;
import tonegod.gui.core.ElementManager;

public abstract class Console extends EmptyContainer {

	private static final List<String> commandHistory = new ArrayList<>();

	private final Collection<ConsoleCommand> availableCommands;

	private final ScrollPanel panScroll;
	private final TextField txtInput;

	private final Vector2f contentItemSize;

	private final Vector2f contentItemPos;

	private int commandHistoryIndex;

	public Console(ElementManager screen) {
		super(screen);

		availableCommands = GameConfig.getAvailableCommands();

		float screenWidth = screen.getWidth();
		float screenHeight = screen.getHeight();

		setDimensions(screenWidth, screenHeight / 2f);
		setPosition(0f, 0f);

		float textHeight = ScreenUtils.getWidgetHeight(screen);

		contentItemSize = new Vector2f(screenWidth, textHeight / 5f * 3f);
		contentItemPos = new Vector2f();

		Vector2f scrollSize = new Vector2f(screenWidth - 10f, (screenHeight / 2f) - 10f);
		panScroll = new ScrollPanel(screen, new Vector2f(0f, 10f), scrollSize);
		panScroll.setUseVerticalWrap(true);
		addChild(panScroll);

		for (String itemString : commandHistory) {
			addToConsolePanel(itemString);
		}

		Vector2f textPos = new Vector2f(0f, screenHeight / 2f);
		Vector2f textSize = new Vector2f(screenWidth, textHeight);
		txtInput = new TextField(screen, textPos, textSize) {
			@Override
			public void onKeyPress(KeyInputEvent evt) {
				switch (evt.getKeyCode()) {
					case 16:
						evt.setConsumed();
						return; // 16 means "not set" here
					case KeyInput.KEY_DOWN:
						replaceWithCommandHistory(false);
						evt.setConsumed();
						break;
					case KeyInput.KEY_UP:
						replaceWithCommandHistory(true);
						evt.setConsumed();
						break;
					case KeyInput.KEY_RETURN:
						executeCommand();
						evt.setConsumed();
						break;
					case KeyInput.KEY_F12:
						hideConsole();
						evt.setConsumed();
						break;
					case KeyInput.KEY_BACK:
						super.onKeyPress(evt);
						return;
					default:
						if (evt.getKeyChar() != 0) {
							super.onKeyPress(evt);
							break;
						}
				}
			}

			@Override
			public void onKeyRelease(KeyInputEvent evt) {
				switch (evt.getKeyCode()) {
					case 16:
						evt.setConsumed();
						return; // 16 means "not set" here
					case KeyInput.KEY_TAB:
						lookupCommands();
						evt.setConsumed();
						return;
					case KeyInput.KEY_BACK:
						super.onKeyRelease(evt);
						return;
				}

				if (evt.getKeyChar() != 0) {
					super.onKeyRelease(evt);
				}
			}
		};
		txtInput.setIsEnabled(true);
		addChild(txtInput);
	}

	protected abstract void hideConsole();

	private void replaceWithCommandHistory(boolean up) {
		if (!commandHistory.isEmpty()) {
			commandHistoryIndex += up ? -1 : +1;

			if (commandHistoryIndex < 0) {
				commandHistoryIndex = 0;
			}

			if (commandHistoryIndex >= 0 && commandHistoryIndex < commandHistory.size()) {
				txtInput.setText(commandHistory.get(commandHistoryIndex));
				txtInput.setCaretPositionToEnd();
			} else {
				if (commandHistoryIndex > commandHistory.size()) {
					commandHistoryIndex = commandHistory.size();
				}

				txtInput.setText("");
			}
		}
	}

	private void lookupCommands() {
		final String text = txtInput.getText().replace(" ", "").toLowerCase();

		if (StringUtils.isNotBlank(text)) {
			List<ConsoleCommand> foundMatches = new ArrayList<>();

			for (ConsoleCommand command : availableCommands) {
				String commandKey = command.getKey();

				if (commandKey.toLowerCase().startsWith(text)) {
					foundMatches.add(command);
				}
			}

			if (!foundMatches.isEmpty()) {
				if (foundMatches.size() == 1) {
					txtInput.setText(foundMatches.get(0).getKey());
					txtInput.setCaretPositionToEnd();
				} else {
					char[] leastCommonStart = null;

					for (ConsoleCommand match : foundMatches) {
						String key = match.getKey();
						char[] start = key.substring(text.length(), key.length()).toCharArray();

						if (leastCommonStart == null) {
							leastCommonStart = start;
						} else if (leastCommonStart.length > 0) {
							String newStart = "";

							for (int i = 0; i < start.length && i < leastCommonStart.length; i++) {
								if (start[i] == leastCommonStart[i]) {
									newStart += start[i];
								} else {
									break;
								}
							}

							leastCommonStart = newStart.toCharArray();
						}

						String matchString = key;

						if (match.getArguments() == 1) {
							matchString += ": " + GameConfig.getString(key);
						}

						addToConsolePanel(matchString);
					}

					if (leastCommonStart != null && leastCommonStart.length > 0) {
						txtInput.setText(text + new String(leastCommonStart));
					}
				}
			}
		}
	}

	private void addToConsolePanel(String text) {
		contentItemPos.y += contentItemSize.y;
		Label label = new Label(screen, contentItemPos, contentItemSize);
		label.setFontColor(ColorRGBA.Blue);
		label.setText(text);
		panScroll.addScrollableContent(label);
		panScroll.scrollToBottom();
	}

	private void executeCommand() {
		String command = txtInput.getText();

		if (StringUtils.isNotBlank(command)) {
			String[] parts = command.split(" ");
			String key = parts[0];

			for (ConsoleCommand cmd : availableCommands) {
				String commandKey = cmd.getKey();

				if (commandKey.toLowerCase().equals(key.toLowerCase())) {
					int argumentsCount = cmd.getArguments();

					if (argumentsCount == parts.length - 1) {
						try {
							String[] args = new String[argumentsCount];

							for (int i = 0; i < argumentsCount; i++) {
								args[i] = parts[i + 1];
							}

							cmd.executeCommand(args);

							txtInput.setText("");

							commandHistory.add(command);
							commandHistoryIndex = commandHistory.size();
							addToConsolePanel(command);
						} catch (Exception e) {
							// TODO
							e.printStackTrace();
						}

						break;
					}
				}
			}
		}
	}

	public void focusTextField(boolean focus) {
		if (focus) {
			txtInput.setTabFocus();
		} else {
			txtInput.resetTabFocus();
		}
	}
}