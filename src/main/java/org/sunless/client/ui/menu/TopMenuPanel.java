package org.sunless.client.ui.menu;

import org.sunless.shared.ui.EmptyContainer;
import org.sunless.shared.ui.ScreenUtils;
import org.sunless.shared.util.I18N;
import org.sunless.shared.util.I18NUtil;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.core.ElementManager;

public abstract class TopMenuPanel extends EmptyContainer {

	private ButtonAdapter btnNewGame;
	private ButtonAdapter btnConnectToServer;
	private ButtonAdapter btnQuit;
	private ButtonAdapter btnCloseGame;

	public TopMenuPanel(ElementManager screen) {
		super(screen);

		init();
	}

	private void init() {
		float padding = ScreenUtils.getPadding(screen);

		float x = padding;
		float widgetWidth = ScreenUtils.getWidgetWidth(screen);
		float widgetHeight = ScreenUtils.getWidgetHeight(screen);
		float y = padding + (widgetHeight * 2) + padding;
		Vector2f widgetSize = ScreenUtils.createWidgetSize(screen);

		btnNewGame = new ButtonAdapter(screen, new Vector2f(x, y), widgetSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				newGame();
			}
		};
		btnNewGame.setText(I18NUtil.get(I18N.NEW_GAME));
		addChild(btnNewGame);

		x += padding + widgetWidth;

		btnConnectToServer = new ButtonAdapter(screen, new Vector2f(x, y), widgetSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				connectToServer();
			}
		};
		btnConnectToServer.setText(I18NUtil.get(I18N.CONNECT_TO_SERVER));
		addChild(btnConnectToServer);

		x += padding + widgetWidth;

		btnQuit = new ButtonAdapter(screen, new Vector2f(x, y), widgetSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				quit();
			}
		};
		btnQuit.setText(I18NUtil.get(I18N.QUIT));
		addChild(btnQuit);

		x += padding + widgetWidth;

		btnCloseGame = new ButtonAdapter(screen, new Vector2f(x, y), widgetSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				closeGame();
			}
		};
		btnCloseGame.setText(I18NUtil.get(I18N.CLOSE_GAME));
	}

	public void showCloseGameButton(boolean show) {
		if (show && btnCloseGame.getParent() == null) {
			addChild(btnCloseGame);
		} else if (!show && btnCloseGame.getParent() != null) {
			removeChild(btnCloseGame);
		}
	}

	protected abstract void continueGame();

	protected abstract void closeGame();

	protected abstract void newGame();

	protected abstract void connectToServer();

	protected abstract void showOptions();

	protected abstract void quit();
}