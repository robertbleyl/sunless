package org.sunless.client.ui.end;

import org.sunless.shared.NodeConstants;
import org.sunless.shared.event.main.EndGameTimeUpdateEvent;
import org.sunless.shared.event.main.EndGameTimeUpdateHandler;
import org.sunless.shared.ui.GUIStateImpl;
import org.sunless.shared.ui.ScreenUtils;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;

import tonegod.gui.controls.text.Label;
import tonegod.gui.core.Screen;

@Singleton
public class EndStateImpl extends GUIStateImpl implements EndState {

	private Label lblEndTimer;

	@Inject
	public EndStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(screen, appRootNode, appGuiNode);

		init();
	}

	@Override
	public void init() {
		addEventHandler(EndGameTimeUpdateEvent.TYPE, new EndGameTimeUpdateHandler() {
			@Override
			public void onUpdateGameTime(EndGameTimeUpdateEvent event) {
				lblEndTimer.setText(event.getSecondsLeft() + "");
			}
		});

		Vector2f position = new Vector2f();
		Vector2f dimensions = ScreenUtils.createWidgetSize(screen);
		lblEndTimer = new Label(screen, position, dimensions);
		mainContent.addChild(lblEndTimer);
	}
}