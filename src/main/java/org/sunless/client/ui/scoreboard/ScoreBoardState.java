package org.sunless.client.ui.scoreboard;

import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.ui.GUIState;

public interface ScoreBoardState extends GUIState {

	void init(Team team1, Team team2);
}