package org.sunless.client.ui.scoreboard;

import java.util.Collections;
import java.util.List;

import org.sunless.shared.event.gameplay.PlayerPropertyChangeEvent;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.PlayerConstants;
import org.sunless.shared.gameplay.player.Team;

import com.jme3.math.Vector2f;

import tonegod.gui.controls.lists.Table;
import tonegod.gui.core.ElementManager;

public class ScoreTable extends Table {
	
	private final Team team;
	
	public ScoreTable(ElementManager screen, Vector2f position, Vector2f dimensions, Team team) {
		super(screen, position, dimensions);
		this.team = team;
		
		init();
	}
	
	private void init() {
		setColumnResizeMode(Table.ColumnResizeMode.AUTO_FIRST);
		
		addColumn("Name");
		addColumn("Kills");
		addColumn("Deaths");
		
		redraw();
	}
	
	public void onPlayerPropertyChange(PlayerPropertyChangeEvent event) {
		String propertyName = event.getPropertyName();
		Player player = event.getPlayer();
		
		if ((propertyName.equals(PlayerConstants.KILLS) || propertyName.equals(PlayerConstants.DEATHS)) && team == player.getTeam()) {
			redraw();
		}
	}
	
	private void redraw() {
		removeAllRows();
		
		List<Player> players = team.getPlayers();
		
		if (!players.isEmpty()) {
			Collections.sort(players);
			
			for (Player player : players) {
				TableRow row = new TableRow(screen, this);
				row.addCell(player.getName(), player.getName());
				row.addCell(player.getKills() + "", player.getKills());
				row.addCell(player.getDeaths() + "", player.getDeaths());
				addRow(row, false);
			}
			
			pack();
		}
	}
	
	@Override
	public void onChange() {
		
	}
}