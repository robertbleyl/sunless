package org.sunless.client.ui.spawnmenu;

import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.json.MapData;
import org.sunless.shared.ui.GUIState;

public interface SpawnMenuState extends GUIState {

	void init(MapData mapData, Player localPlayer);
}