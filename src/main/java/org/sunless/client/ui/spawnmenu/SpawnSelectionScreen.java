package org.sunless.client.ui.spawnmenu;

import java.io.IOException;
import java.util.List;

import org.sunless.shared.json.EntityProperties;
import org.sunless.shared.json.FactionConfig;
import org.sunless.shared.json.JsonHelper;
import org.sunless.shared.json.ShipConfig;
import org.sunless.shared.ui.EmptyContainer;
import org.sunless.shared.ui.ImageButton;
import org.sunless.shared.ui.ScreenUtils;
import org.sunless.shared.util.FileConstants;

import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;

import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.text.Label;
import tonegod.gui.core.ElementManager;

public abstract class SpawnSelectionScreen extends EmptyContainer {

	public SpawnSelectionScreen(ElementManager screen, Vector2f dimensions) {
		super(screen);

		setDimensions(dimensions);
	}

	protected abstract void selectShip(String name);

	protected abstract void respawn();

	protected abstract void spectate();

	public void initShips(FactionConfig factionConfig) {
		removeAllChildren();

		Vector2f widgetSize = ScreenUtils.createWidgetSize(screen);
		float padding = ScreenUtils.getPadding(screen);

		float x = padding;

		Vector2f buttonsPos = new Vector2f(x, padding);

		ButtonAdapter btnWantsRespawn = new ButtonAdapter(screen, buttonsPos, widgetSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				respawn();
			}
		};
		btnWantsRespawn.setText("Respawn");
		addChild(btnWantsRespawn);

		buttonsPos = new Vector2f((padding * 2) + widgetSize.x, padding);

		ButtonAdapter btnSpectate = new ButtonAdapter(screen, buttonsPos, widgetSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				spectate();
			}
		};
		btnSpectate.setText("Spectate");
		addChild(btnSpectate);

		List<ShipConfig> ships = factionConfig.getShips();

		Vector2f shipPanelSize = new Vector2f(getWidth(), (getHeight() - padding - widgetSize.y) / ships.size());
		float imageSize = shipPanelSize.y - widgetSize.y;
		Vector2f imageDimension = new Vector2f(imageSize, imageSize);
		float shipPanelY = widgetSize.y * 2f;

		for (ShipConfig shipConfig : ships) {
			try {
				EmptyContainer panShip = new EmptyContainer(screen);
				panShip.setDimensions(shipPanelSize);
				panShip.setY(shipPanelY);
				addChild(panShip);

				shipPanelY += shipPanelSize.y;

				Label lblName = new Label(screen, widgetSize);
				lblName.setText(shipConfig.getShipName());
				panShip.addChild(lblName);

				ImageButton icon = new ImageButton(screen, FileConstants.MODELS_PATH + shipConfig.getShipName() + "/selection_image.jpg") {
					@Override
					public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
						selectShip(shipConfig.getShipName());
					}
				};
				icon.setDimensions(imageDimension);
				icon.setY(widgetSize.y);
				panShip.addChild(icon);

				String propFilePath = FileConstants.MODELS_PATH_COMPLETE + shipConfig.getShipName() + FileConstants.MODEL_PROPERTY_FILE;
				EntityProperties properties = JsonHelper.get().toPOJO(propFilePath, EntityProperties.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}