package org.sunless.client.ui.spawnmenu;

import java.util.Set;
import java.util.concurrent.Callable;

import org.sunless.client.event.ToggleSpawnMenuEvent;
import org.sunless.client.event.gameplay.SpectateNextPlayerEvent;
import org.sunless.client.event.spawnmenu.BuyShipEvent;
import org.sunless.client.event.spawnmenu.ChangeSpawnLocationEvent;
import org.sunless.client.event.spawnmenu.WantsRespawnEvent;
import org.sunless.client.network.ClientSyncState;
import org.sunless.shared.NodeConstants;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.event.gameplay.EntityPropertyChangeEvent;
import org.sunless.shared.event.gameplay.EntityPropertyChangeHandler;
import org.sunless.shared.event.gameplay.PlayerPropertyChangeEvent;
import org.sunless.shared.event.gameplay.PlayerPropertyChangeHandler;
import org.sunless.shared.event.gameplay.RespawnPlayerEvent;
import org.sunless.shared.event.gameplay.RespawnPlayerHandler;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.PlayerConstants;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.json.Faction;
import org.sunless.shared.json.MapData;
import org.sunless.shared.loading.EnqueueHelper;
import org.sunless.shared.ui.GUIStateImpl;
import org.sunless.shared.ui.ScreenUtils;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

import tonegod.gui.controls.windows.TabControl;
import tonegod.gui.core.Screen;

@Singleton
public class SpawnMenuStateImpl extends GUIStateImpl implements SpawnMenuState {

	private final ClientSyncState syncState;

	private SpawnMap map;
	private final TeamsContainer teamsContainer;
	private final EnqueueHelper enqueueHelper;
	private TabControl tabControl;
	private SpawnSelectionScreen selectionScreen;

	@Inject
	public SpawnMenuStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode, EnqueueHelper enqueueHelper, ClientSyncState syncState, TeamsContainer teamsContainer) {
		super(screen, appRootNode, appGuiNode);
		this.enqueueHelper = enqueueHelper;
		this.syncState = syncState;
		this.teamsContainer = teamsContainer;
	}

	@Override
	public void init() {

	}

	public void init(MapData mapData, Player localPlayer) {
		addEventHandler(RespawnPlayerEvent.TYPE, new RespawnPlayerHandler() {
			@Override
			public void onRespawnPlayer(RespawnPlayerEvent event) {
				if (event.getPlayer().isLocalPlayer()) {
					setEnabled(false);
				}
			}
		});

		addEventHandler(PlayerPropertyChangeEvent.TYPE, new PlayerPropertyChangeHandler() {
			@Override
			public void onChange(PlayerPropertyChangeEvent event) {
				if (event.getPlayer().isLocalPlayer()) {
					if (event.getPropertyName().equals(PlayerConstants.SPAWN_POINT)) {
						Set<Spatial> activeSpawnPoints = event.getPlayer().getTeam().getActiveSpawnPoints();

						final long id = (long)event.getPropertyValue();

						Vector3f location = activeSpawnPoints.stream().filter(s -> EntityHelper.getEntityId(s) == id).map(s -> s.getLocalTranslation()).findFirst().get();
						map.setActiveSpawnLocation(location);
					} else if (event.getPropertyName().equals(PlayerConstants.TEAM)) {
						selectionScreen.initShips(localPlayer.getTeam().getFactionConfig());
					}
				}
			}
		});

		addEventHandler(EntityPropertyChangeEvent.TYPE, new EntityPropertyChangeHandler() {
			@Override
			public void onChange(EntityPropertyChangeEvent event) {
				if (event.getPropertyName().equals(EntityConstants.faction)) {
					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							Spatial spawnPoint = syncState.getEntity(event.getEntityId());

							Object oldFaction = event.getOldPropertyValue();

							if (oldFaction != null) {
								Team team = teamsContainer.getTeam(Faction.valueOf(oldFaction.toString()));
								team.getActiveSpawnPoints().remove(spawnPoint);
							}

							Object propertyValue = event.getPropertyValue();

							if (propertyValue != null) {
								Faction faction = Faction.valueOf(propertyValue.toString());
								Team team = teamsContainer.getTeam(faction);
								team.getActiveSpawnPoints().add(spawnPoint);
								map.updateSpawnPoint(spawnPoint.getLocalTranslation(), faction == localPlayer.getTeam().getFaction());
							} else {
								map.updateSpawnPoint(spawnPoint.getLocalTranslation(), false);
							}

							return null;
						}
					});
				}
			}
		});

		float padding = ScreenUtils.getPadding(screen);

		float tabPanelWidth = screen.getWidth() - (padding * 2);
		float tabPanelHeight = screen.getHeight() - (padding * 3);

		tabControl = new TabControl(screen, new Vector2f(padding, padding * 2f), new Vector2f(tabPanelWidth, tabPanelHeight)) {
			@Override
			public void onTabSelect(int index) {
			}
		};
		mainContent.addChild(tabControl);

		tabControl.addTab("Selection");
		tabControl.addTab("Spawn map");

		float tabContentWidth = tabPanelWidth - (padding * 2f);
		float tabContentHeight = tabPanelHeight - (padding * 4f);
		Vector2f tabContentSize = new Vector2f(tabContentWidth, tabContentHeight);
		Vector2f tabContentPos = new Vector2f(padding, padding);

		selectionScreen = new SpawnSelectionScreen(screen, tabContentSize) {
			@Override
			protected void selectShip(String name) {
				fireEvent(new BuyShipEvent(name));
			}

			@Override
			protected void respawn() {
				fireEvent(new WantsRespawnEvent());
			}

			@Override
			protected void spectate() {
				fireEvent(new SpectateNextPlayerEvent());
				fireEvent(new ToggleSpawnMenuEvent());
			}
		};
		selectionScreen.initShips(localPlayer.getTeam().getFactionConfig());
		selectionScreen.setPosition(tabContentPos);
		tabControl.addTabChild(0, selectionScreen);

		map = new SpawnMap(screen, tabContentSize, mapData) {
			@Override
			protected void onClickSpawnPoint(Vector3f location) {
				fireEvent(new ChangeSpawnLocationEvent(location));
			}
		};
		map.setPosition(tabContentPos);
		tabControl.addTabChild(1, map);
	}
}