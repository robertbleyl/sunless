package org.sunless.client.ui.profileselection;

import javax.inject.Singleton;

import org.sunless.client.event.main.UsernameSelectedEvent;
import org.sunless.shared.NodeConstants;
import org.sunless.shared.ui.GUIStateImpl;
import org.sunless.shared.ui.ScreenUtils;
import org.sunless.shared.ui.SimpleTextField;
import org.sunless.shared.util.I18N;
import org.sunless.shared.util.I18NUtil;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import com.jme3.scene.Node;

import tonegod.gui.controls.buttons.Button;
import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.text.Label;
import tonegod.gui.controls.text.TextField;
import tonegod.gui.controls.windows.Window;
import tonegod.gui.core.Screen;

@Singleton
public class UsernameInputStateImpl extends GUIStateImpl implements UsernameInputState {

	@Inject
	public UsernameInputStateImpl(Screen screen, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(screen, appRootNode, appGuiNode);

		init();
	}

	@Override
	public void init() {
		float width = screen.getWidth();
		float height = screen.getHeight();

		Vector2f widgetSize = ScreenUtils.createWidgetSize(screen);
		float padding = ScreenUtils.getPadding(screen);

		Vector2f dimensions = new Vector2f(widgetSize.x * 2 + padding * 3, widgetSize.y * 2 + padding * 3);
		Vector2f position = new Vector2f((width / 2) - (dimensions.x / 2), (height / 2) - (dimensions.y / 2));

		Window window = new Window(screen, position, dimensions);
		window.setUseCloseButton(false);
		window.setWindowIsMovable(false);
		mainContent.addChild(window);

		Label label = new Label(screen, new Vector2f(padding, widgetSize.y), widgetSize);
		label.setText(I18NUtil.get(I18N.USERNAME) + ":");
		window.addChild(label);

		final TextField textField = new SimpleTextField(screen, new Vector2f(widgetSize.x + padding * 2, widgetSize.y), widgetSize);
		window.addChild(textField);

		Button button = new ButtonAdapter(screen, new Vector2f(widgetSize.x + padding * 2, widgetSize.y * 2 + padding), widgetSize) {
			@Override
			public void onMouseLeftReleased(MouseButtonEvent evt) {
				fireEvent(new UsernameSelectedEvent(textField.getText()));
			}
		};
		button.setText(I18NUtil.get(I18N.SAVE));
		window.addChild(button);
	}
}