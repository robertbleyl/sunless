package org.sunless.client.gameplay.mode;

import java.util.Map;

import org.sunless.shared.entity.EntityAttacher;
import org.sunless.shared.entity.EntityFactory;
import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.gameplay.mode.MotherShipLoadingStepFactory;
import org.sunless.shared.json.Faction;
import org.sunless.shared.json.FactionConfig;
import org.sunless.shared.json.MapData;

public class ClientMotherShipLoadingStepFactory extends MotherShipLoadingStepFactory {

	public ClientMotherShipLoadingStepFactory(MapData mapData, EntityFactory entityFactory, TeamsContainer gameDataContainer, Map<Faction, FactionConfig> factionConfigs, EntityAttacher entityAttacher,
			GameInfo gameInfo) {
		super(mapData, entityFactory, gameDataContainer, factionConfigs, entityAttacher, gameInfo);
	}
}