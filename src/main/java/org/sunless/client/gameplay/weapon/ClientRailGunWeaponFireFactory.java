package org.sunless.client.gameplay.weapon;

import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.gameplay.weapon.SimpleSpecialWeaponFirePhysics;
import org.sunless.shared.gameplay.weapon.WeaponFireCreationData;

import com.google.inject.Inject;
import com.jme3.asset.AssetManager;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Line;

public class ClientRailGunWeaponFireFactory extends ClientSimpleWeaponFireFactory {

	@Inject
	public ClientRailGunWeaponFireFactory(AssetManager assetManager) {
		super(EntityConstants.special_weaponFirePrefix, assetManager, new SimpleSpecialWeaponFirePhysics(EntityConstants.special_weaponFirePrefix), EntityConstants.special_weaponFireType_railGun);
	}

	@Override
	public Node createWeaponFire(WeaponFireCreationData data) throws Exception {
		Node weaponFire = super.createWeaponFire(data);
		weaponFire.detachAllChildren();

		Vector3f startPosWorld = data.getStartPoint();
		Node parent = data.getFiringShip().getParent();

		Ray ray = new Ray(startPosWorld, data.getDirection());
		CollisionResults results = new CollisionResults();
		parent.collideWith(ray, results);

		CollisionResult closestCollision = results.getClosestCollision();

		Vector3f endPoint = closestCollision != null ? closestCollision.getContactPoint() : startPosWorld.add(data.getDirection().normalize().mult(10000f));

		Line line = new Line(startPosWorld, endPoint);
		Geometry lineGeom = new Geometry("line geom", line);

		Material material = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		material.setColor("Color", ColorRGBA.Yellow);
		material.getAdditionalRenderState().setLineWidth(3f);
		lineGeom.setMaterial(material);

		float lifeTime = data.getFiringShip().getUserData(EntityConstants.special_weaponFireLifeTime);
		lineGeom.addControl(new RailGunWeaponFireLineControl(ColorRGBA.Yellow.clone(), lifeTime, material));

		parent.attachChild(lineGeom);

		return weaponFire;
	}
}