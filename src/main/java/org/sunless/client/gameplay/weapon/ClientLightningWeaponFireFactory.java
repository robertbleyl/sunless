package org.sunless.client.gameplay.weapon;

import java.util.List;

import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.gameplay.weapon.LightningWeaponFirePhysics;
import org.sunless.shared.gameplay.weapon.WeaponFireCreationData;
import org.sunless.shared.gameplay.weapon.WeaponFireFactory;
import org.sunless.shared.util.FileConstants;

import com.google.inject.Inject;
import com.jme3.asset.AssetManager;
import com.jme3.effect.ParticleEmitter;
import com.jme3.effect.ParticleMesh.Type;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class ClientLightningWeaponFireFactory extends WeaponFireFactory {

	@Inject
	public ClientLightningWeaponFireFactory(AssetManager assetManager) {
		super(EntityConstants.special_weaponFirePrefix, assetManager, new LightningWeaponFirePhysics(), EntityConstants.special_weaponFireType_lightning);
	}

	@Override
	public Node createWeaponFire(WeaponFireCreationData data) throws Exception {
		Spatial firingShip = data.getFiringShip();

		Node weaponFire = super.createWeaponFire(data);

		List<Vector3f> lightningSlots = firingShip.getUserData(EntityConstants.lightningSlots);

		weaponFire.setLocalRotation(firingShip.getLocalRotation().clone());

		Vector3f a = lightningSlots.get(0);
		Vector3f b = lightningSlots.get(1);

		Vector3f direction = b.subtract(a);
		float length = firingShip.localToWorld(b, new Vector3f()).subtract(firingShip.localToWorld(a, new Vector3f())).length();
		ColorRGBA color = ColorRGBA.Cyan;

		Material material = new Material(assetManager, "Common/MatDefs/Misc/Particle.j3md");
		material.setTexture("Texture", assetManager.loadTexture(FileConstants.TEXTURES_PATH + "standardWeaponFire.png"));

		Vector3f location = a.clone();
		float size = getSize(firingShip);
		direction.normalizeLocal().multLocal(size);

		int steps = (int)FastMath.ceil(length / direction.length());

		for (int i = 0; i < steps; i++) {
			Node particleNode = new Node("Lightning particle node");
			particleNode.setLocalTranslation(location);
			weaponFire.attachChild(particleNode);
			location = location.add(direction);

			ParticleEmitter particleEmitter = new ParticleEmitter("Weapon fire particle emitter", Type.Triangle, 50);
			particleNode.attachChild(particleEmitter);
			particleEmitter.setImagesX(4);
			particleEmitter.setImagesY(4);
			particleEmitter.getParticleInfluencer().setInitialVelocity(Vector3f.ZERO);
			particleEmitter.getParticleInfluencer().setVelocityVariation(0.0f);
			particleEmitter.setGravity(0, 0, 0);

			particleEmitter.setMaterial(material);
			particleEmitter.setLowLife(0.03f);
			particleEmitter.setHighLife(0.03f);
			particleEmitter.setParticlesPerSec(100f);

			particleEmitter.setStartColor(color);
			particleEmitter.setEndColor(color);
			particleEmitter.setStartSize(size);
			particleEmitter.setEndSize(size);

			if (showPaths()) {
				particleEmitter.setHighLife(99999f);
				particleEmitter.setLowLife(99999f);
			}
		}

		return weaponFire;
	}
}