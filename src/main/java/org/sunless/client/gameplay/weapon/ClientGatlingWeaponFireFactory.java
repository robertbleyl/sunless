package org.sunless.client.gameplay.weapon;

import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.gameplay.weapon.SimpleSpecialWeaponFirePhysics;

import com.google.inject.Inject;
import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;

public class ClientGatlingWeaponFireFactory extends ClientSimpleWeaponFireFactory {

	@Inject
	public ClientGatlingWeaponFireFactory(AssetManager assetManager) {
		super(EntityConstants.special_weaponFirePrefix, assetManager, new SimpleSpecialWeaponFirePhysics(EntityConstants.special_weaponFirePrefix), EntityConstants.special_weaponFireType_gatling);
	}

	@Override
	protected ColorRGBA getColor() {
		return ColorRGBA.Green;
	}
}