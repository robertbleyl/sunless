package org.sunless.client.gameplay.weapon;

import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class RailGunWeaponFireLineControl extends AbstractControl {

	private float lifeTimePercentage = 1f;

	private final ColorRGBA color;
	private final float lifeTime;
	private final Material material;

	public RailGunWeaponFireLineControl(ColorRGBA color, float lifeTime, Material material) {
		this.color = color;
		this.lifeTime = lifeTime;
		this.material = material;
	}

	@Override
	protected void controlUpdate(float tpf) {
		float diff = (tpf * lifeTime) / lifeTime;
		lifeTimePercentage -= diff;

		if (lifeTimePercentage <= 0f) {
			spatial.removeFromParent();
		} else {
			color.a = lifeTimePercentage;
			color.r -= diff;
			color.g -= diff;
			color.b -= diff;
			material.setColor("Color", color);
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}