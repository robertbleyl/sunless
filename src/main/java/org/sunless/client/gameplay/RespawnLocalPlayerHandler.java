package org.sunless.client.gameplay;

import org.sunless.client.input.InputHandler;
import org.sunless.client.network.ClientPlayerSyncControl;
import org.sunless.shared.control.MoveControl;
import org.sunless.shared.control.MovementInfo;
import org.sunless.shared.event.gameplay.RespawnPlayerEvent;
import org.sunless.shared.event.gameplay.RespawnPlayerHandler;
import org.sunless.shared.gameplay.player.Player;

import com.jme3.asset.AssetManager;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.renderer.Camera;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Node;
import com.jme3.scene.control.CameraControl.ControlDirection;

public class RespawnLocalPlayerHandler implements RespawnPlayerHandler {

	private final CameraNode cameraNode;
	private final InputHandler inputHandler;
	private final AssetManager assetManager;

	private Node currentShip;

	public RespawnLocalPlayerHandler(Camera camera, AssetManager assetManager, InputHandler inputHandler) {
		this.assetManager = assetManager;
		this.inputHandler = inputHandler;

		cameraNode = new CameraNode("Camera Node", camera);
		cameraNode.setControlDir(ControlDirection.SpatialToCamera);
	}

	@Override
	public void onRespawnPlayer(RespawnPlayerEvent event) {
		Player player = event.getPlayer();

		if (player.isLocalPlayer()) {
			reset();

			currentShip = player.getShip();

			RigidBodyControl bodyControl = currentShip.getControl(RigidBodyControl.class);
			currentShip.addControl(new MoveControl(bodyControl));
			currentShip.addControl(inputHandler);

			ClientPlayerSyncControl clientPlayerSyncControl = currentShip.getControl(ClientPlayerSyncControl.class);
			MoveControl moveControl = currentShip.getControl(MoveControl.class);
			MovementInfo moveInfo = moveControl.getMoveInfo();
			clientPlayerSyncControl.setMoveInfo(moveInfo);
			inputHandler.setMoveInfo(moveInfo);

			ChaseCameraHelper.setupChaseCamera(currentShip, cameraNode, assetManager);
		}
	}

	public void reset() {
		if (currentShip != null) {
			currentShip.removeControl(inputHandler);
			currentShip.removeControl(MoveControl.class);
			currentShip.removeControl(CameraMovementControl.class);
			currentShip.removeControl(SpaceDustControl.class);
			currentShip.removeControl(EngineAudioControl.class);

			currentShip = null;
		}
	}
}