package org.sunless.client.gameplay;

import javax.inject.Singleton;

import org.sunless.shared.NodeConstants;
import org.sunless.shared.gameplay.IngameStateImpl;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.jme3.light.Light;
import com.jme3.light.LightList;
import com.jme3.scene.Node;

@Singleton
public class ClientIngameStateImpl extends IngameStateImpl implements ClientIngameState {

	@Inject
	public ClientIngameStateImpl(@Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		super(appRootNode, appGuiNode);
	}

	@Override
	public void cleanup() {
		super.cleanup();

		rootNode.detachAllChildren();

		LightList localLightList = rootNode.getLocalLightList().clone();

		for (Light light : localLightList) {
			rootNode.removeLight(light);
		}
	}
}