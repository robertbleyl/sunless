package org.sunless.client.gameplay;

import org.sunless.shared.control.MoveControl;
import org.sunless.shared.control.MovementInfo;

import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.CameraNode;
import com.jme3.scene.control.AbstractControl;

public class CameraMovementControl extends AbstractControl {

	private final CameraNode cameraNode;
	private final Vector3f initCamNodePosition;

	public CameraMovementControl(CameraNode cameraNode) {
		this.cameraNode = cameraNode;
		this.initCamNodePosition = cameraNode.getLocalTranslation().clone();
	}

	@Override
	protected void controlUpdate(float tpf) {
		MoveControl moveControl = spatial.getControl(MoveControl.class);

		if (moveControl != null) {
			MovementInfo moveInfo = moveControl.getMoveInfo();
			float rotationPercentageX = moveInfo.getRotationPercentageX() * 2f;
			float rotationPercentageY = moveInfo.getRotationPercentageY() * 1.5f;
			float forwardSpeedPercentage = moveInfo.getForwardSpeedPercentage() * 3f;

			Vector3f targetLocation = initCamNodePosition.add(new Vector3f(rotationPercentageX, -rotationPercentageY, -forwardSpeedPercentage));
			cameraNode.getLocalTranslation().interpolateLocal(targetLocation, tpf);
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}