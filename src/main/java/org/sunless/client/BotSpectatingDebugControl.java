package org.sunless.client;

import java.util.concurrent.Callable;

import org.sunless.client.event.gameplay.SpectatedPlayerChangedEvent;
import org.sunless.client.event.gameplay.SpectatedPlayerChangedHandler;
import org.sunless.shared.config.DebugConfigConstants;
import org.sunless.shared.control.MoveControl;
import org.sunless.shared.control.MovementInfo;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.config.DebugConfigChangedEvent;
import org.sunless.shared.event.config.DebugConfigChangedHandler;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.loading.EnqueueHelper;
import org.sunless.shared.util.FileConstants;

import com.jme3.asset.AssetManager;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;
import com.jme3.ui.Picture;

public class BotSpectatingDebugControl extends AbstractControl {

	private final EventBus eventBus = EventBus.get();

	private final Camera camera;
	private final Picture picture;

	private Player spectatedPlayer;

	public BotSpectatingDebugControl(Node guiNode, Camera camera, AssetManager assetManager, EnqueueHelper enqueueHelper) {
		this.camera = camera;

		float size = camera.getWidth() / 30f;

		picture = new Picture("Bot Spectate Debug Crosshair");
		picture.setImage(assetManager, FileConstants.HUD_PATH + "botSpectatDebugCrosshair.png", true);
		picture.setWidth(size);
		picture.setHeight(size);

		eventBus.addHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				String commandKey = event.getDebugCommandKey();

				if (commandKey.equals(DebugConfigConstants.dbg_toggle_bot_spectating_data)) {
					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							setEnabled(!isEnabled());

							if (enabled) {
								guiNode.attachChild(picture);
							} else {
								guiNode.detachChild(picture);
							}

							return null;
						}
					});
				}
			}
		});

		eventBus.addHandler(SpectatedPlayerChangedEvent.TYPE, new SpectatedPlayerChangedHandler() {
			@Override
			public void onChange(SpectatedPlayerChangedEvent event) {
				spectatedPlayer = event.getPlayer();
			}
		});
	}

	@Override
	protected void controlUpdate(float tpf) {
		if (spectatedPlayer != null) {
			Node ship = spectatedPlayer.getShip();

			if (ship != null) {
				MoveControl moveControl = ship.getControl(MoveControl.class);

				if (moveControl != null) {
					MovementInfo moveInfo = moveControl.getMoveInfo();

					int width = camera.getWidth();
					int height = camera.getHeight();

					float halfWidth = width / 2f;
					float halfHeight = height / 2f;

					float x = halfWidth - (halfWidth * moveInfo.getRotationPercentageX());
					float y = halfHeight - (halfHeight * moveInfo.getRotationPercentageY());

					if (x == width) {
						x -= picture.getLocalScale().x;
					}

					if (y == height) {
						y -= picture.getLocalScale().y;
					}

					picture.setPosition(x, y);
				}
			}
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}