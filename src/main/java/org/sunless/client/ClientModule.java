package org.sunless.client;

import org.sunless.client.entity.ClientEntityFactory;
import org.sunless.client.entity.ClientEntityIdProvider;
import org.sunless.client.gameplay.ClientIngameState;
import org.sunless.client.gameplay.ClientIngameStateImpl;
import org.sunless.client.gameplay.weapon.ClientGatlingWeaponFireFactory;
import org.sunless.client.gameplay.weapon.ClientLightningWeaponFireFactory;
import org.sunless.client.gameplay.weapon.ClientRailGunWeaponFireFactory;
import org.sunless.client.gameplay.weapon.ClientRocketLauncherWeaponFireFactory;
import org.sunless.client.gameplay.weapon.ClientStandardWeaponFireFactory;
import org.sunless.client.loading.ClientLoadingStepsBuilder;
import org.sunless.client.network.ClientFactory;
import org.sunless.client.network.ClientFactoryImpl;
import org.sunless.client.network.ClientState;
import org.sunless.client.network.ClientStateImpl;
import org.sunless.client.network.ClientSyncState;
import org.sunless.client.network.messagehandler.ClientMessageHandler;
import org.sunless.client.network.messagehandler.DebugConfigChangedMessageHandler;
import org.sunless.client.network.messagehandler.EndTimerUpdateMessageHandler;
import org.sunless.client.network.messagehandler.EntityPropertyChangeMessageHandler;
import org.sunless.client.network.messagehandler.GameInfoInitMessageHandler;
import org.sunless.client.network.messagehandler.GameStateResponseMessageHandler;
import org.sunless.client.network.messagehandler.GameTimeUpdateMessageHandler;
import org.sunless.client.network.messagehandler.PlayerNameRequestMessageHandler;
import org.sunless.client.network.messagehandler.PlayerPropertyChangeMessageHandler;
import org.sunless.client.network.messagehandler.PlayerShipChangeMessageHandler;
import org.sunless.client.network.messagehandler.RemoveWeaponFireMessageHandler;
import org.sunless.client.network.messagehandler.RespawnPlayerMessageHandler;
import org.sunless.client.network.messagehandler.RestartGameMessageHandler;
import org.sunless.client.network.messagehandler.ServerRestartedMessageHandler;
import org.sunless.client.network.messagehandler.SpectatedPlayerUpdateMessageHandler;
import org.sunless.client.network.messagehandler.WeaponFireCreationMessageHandler;
import org.sunless.client.network.messagehandler.WinConditionReachedMessageHandler;
import org.sunless.client.ui.console.ConsoleState;
import org.sunless.client.ui.console.ConsoleStateImpl;
import org.sunless.client.ui.end.EndState;
import org.sunless.client.ui.end.EndStateImpl;
import org.sunless.client.ui.hud.HUDState;
import org.sunless.client.ui.hud.HUDStateImpl;
import org.sunless.client.ui.menu.MenuState;
import org.sunless.client.ui.menu.MenuStateImpl;
import org.sunless.client.ui.profileselection.UsernameInputState;
import org.sunless.client.ui.profileselection.UsernameInputStateImpl;
import org.sunless.client.ui.scoreboard.ScoreBoardState;
import org.sunless.client.ui.scoreboard.ScoreBoardStateImpl;
import org.sunless.client.ui.spawnmenu.SpawnMenuState;
import org.sunless.client.ui.spawnmenu.SpawnMenuStateImpl;
import org.sunless.shared.AbstractGameApplication;
import org.sunless.shared.SharedModule;
import org.sunless.shared.entity.EntityFactory;
import org.sunless.shared.entity.EntityIdProvider;
import org.sunless.shared.gameplay.IngameState;
import org.sunless.shared.gameplay.weapon.WeaponFireFactory;
import org.sunless.shared.loading.LoadingStepsBuilder;
import org.sunless.shared.network.sync.AbstractSyncState;

import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;

public class ClientModule extends SharedModule {

	private final ClientGraphicsUpdater graphicsUpdater;

	public ClientModule(AbstractGameApplication app, ClientGraphicsUpdater graphicsUpdater) {
		super(app);
		this.graphicsUpdater = graphicsUpdater;
	}

	@Override
	protected void configure() {
		super.configure();

		bind(IngameState.class).to(ClientIngameStateImpl.class);
		bind(ClientIngameState.class).to(ClientIngameStateImpl.class);
		bind(ClientFactory.class).to(ClientFactoryImpl.class);
		bind(ClientState.class).to(ClientStateImpl.class);
		bind(EntityFactory.class).to(ClientEntityFactory.class);
		bind(EntityIdProvider.class).to(ClientEntityIdProvider.class);
		bind(AbstractSyncState.class).to(ClientSyncState.class);
		bind(LoadingStepsBuilder.class).to(ClientLoadingStepsBuilder.class);
		bind(UsernameInputState.class).to(UsernameInputStateImpl.class);
		bind(ConsoleState.class).to(ConsoleStateImpl.class);
		bind(MenuState.class).to(MenuStateImpl.class);
		bind(HUDState.class).to(HUDStateImpl.class);
		bind(SpawnMenuState.class).to(SpawnMenuStateImpl.class);
		bind(ScoreBoardState.class).to(ScoreBoardStateImpl.class);
		bind(EndState.class).to(EndStateImpl.class);

		bind(ClientGraphicsUpdater.class).toInstance(graphicsUpdater);

		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(DebugConfigChangedMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(EndTimerUpdateMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(EntityPropertyChangeMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(GameInfoInitMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(GameStateResponseMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(GameTimeUpdateMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(PlayerNameRequestMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(PlayerPropertyChangeMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(PlayerShipChangeMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(RemoveWeaponFireMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(RespawnPlayerMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(RestartGameMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(ServerRestartedMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(SpectatedPlayerUpdateMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(WeaponFireCreationMessageHandler.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), ClientMessageHandler.class).addBinding().to(WinConditionReachedMessageHandler.class).in(Singleton.class);

		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ClientStandardWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ClientLightningWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ClientGatlingWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ClientRailGunWeaponFireFactory.class).in(Singleton.class);
		Multibinder.newSetBinder(binder(), WeaponFireFactory.class).addBinding().to(ClientRocketLauncherWeaponFireFactory.class).in(Singleton.class);
	}
}