package org.sunless.client.entity;

import org.sunless.shared.entity.EntityIdProvider;

public class ClientEntityIdProvider implements EntityIdProvider {
	
	@Override
	public long getNextId() {
		return -1;
	}
}