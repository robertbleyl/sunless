package org.sunless.client.event;

import org.sunless.shared.event.EventHandler;

public interface ClientConfigChangedHandler extends EventHandler {
	
	void onChanged(ClientConfigChangedEvent event);
}