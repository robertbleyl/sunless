package org.sunless.client.event.spawnmenu;

import org.sunless.shared.event.EventHandler;

public interface BuyShipHandler extends EventHandler {
	
	void onBuyShip(BuyShipEvent event);
}