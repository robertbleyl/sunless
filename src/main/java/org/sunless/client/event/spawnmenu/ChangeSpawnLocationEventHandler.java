package org.sunless.client.event.spawnmenu;

import org.sunless.shared.event.EventHandler;

public interface ChangeSpawnLocationEventHandler extends EventHandler {

	void onChangeSpawnLocation(ChangeSpawnLocationEvent event);
}