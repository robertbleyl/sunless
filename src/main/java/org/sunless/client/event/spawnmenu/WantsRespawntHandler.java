package org.sunless.client.event.spawnmenu;

import org.sunless.shared.event.EventHandler;

public interface WantsRespawntHandler extends EventHandler {
	
	void onWantsRespawn(WantsRespawnEvent event);
}