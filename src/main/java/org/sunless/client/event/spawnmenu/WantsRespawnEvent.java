package org.sunless.client.event.spawnmenu;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class WantsRespawnEvent extends Event<WantsRespawntHandler> {
	
	public static final EventType<WantsRespawntHandler> TYPE = new EventType<>();
	
	@Override
	public EventType<WantsRespawntHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(WantsRespawntHandler handler) {
		handler.onWantsRespawn(this);
	}
}