package org.sunless.client.event;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class ToggleScoreBoardEvent extends Event<ToggleScoreBoardHandler> {
	
	public static final EventType<ToggleScoreBoardHandler> TYPE = new EventType<>();
	
	private final boolean show;
	
	public ToggleScoreBoardEvent(boolean show) {
		this.show = show;
	}
	
	@Override
	public EventType<ToggleScoreBoardHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(ToggleScoreBoardHandler handler) {
		handler.onToggleScoreBoard(this);
	}
	
	public boolean isShow() {
		return show;
	}
}