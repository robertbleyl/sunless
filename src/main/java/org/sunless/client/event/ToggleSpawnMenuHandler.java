package org.sunless.client.event;

import org.sunless.shared.event.EventHandler;

public interface ToggleSpawnMenuHandler extends EventHandler {
	
	void onChange(ToggleSpawnMenuEvent event);
}