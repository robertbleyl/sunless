package org.sunless.client.event;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class ClientConfigChangedEvent extends Event<ClientConfigChangedHandler> {
	
	public static final EventType<ClientConfigChangedHandler> TYPE = new EventType<>();
	
	private final String key;
	private final String value;
	
	public ClientConfigChangedEvent(String key, String value) {
		this.key = key;
		this.value = value;
	}
	
	@Override
	public EventType<ClientConfigChangedHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(ClientConfigChangedHandler handler) {
		handler.onChanged(this);
	}
	
	public String getKey() {
		return key;
	}
	
	public String getValue() {
		return value;
	}
}