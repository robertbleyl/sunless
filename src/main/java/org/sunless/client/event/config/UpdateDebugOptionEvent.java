package org.sunless.client.event.config;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class UpdateDebugOptionEvent extends Event<UpdateDebugOptionHandler> {

	public static final EventType<UpdateDebugOptionHandler> TYPE = new EventType<>();

	private final String debugCommandKey;
	private final String[] args;

	public UpdateDebugOptionEvent(String debugCommandKey, String... args) {
		this.debugCommandKey = debugCommandKey;
		this.args = args;
	}

	@Override
	public EventType<UpdateDebugOptionHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(UpdateDebugOptionHandler handler) {
		handler.onUpdate(this);
	}

	public String getDebugCommandKey() {
		return debugCommandKey;
	}

	public String[] getArgs() {
		return args;
	}
}