package org.sunless.client.event.config;

import org.sunless.shared.event.EventHandler;

public interface UpdateDebugOptionHandler extends EventHandler {
	
	void onUpdate(UpdateDebugOptionEvent event);
}