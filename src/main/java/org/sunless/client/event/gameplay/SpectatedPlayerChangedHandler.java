package org.sunless.client.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface SpectatedPlayerChangedHandler extends EventHandler {

	void onChange(SpectatedPlayerChangedEvent event);
}