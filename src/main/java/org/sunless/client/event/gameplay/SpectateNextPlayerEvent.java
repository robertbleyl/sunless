package org.sunless.client.event.gameplay;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class SpectateNextPlayerEvent extends Event<SpectateNextPlayerHandler> {

	public static final EventType<SpectateNextPlayerHandler> TYPE = new EventType<>();

	@Override
	public EventType<SpectateNextPlayerHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(SpectateNextPlayerHandler handler) {
		handler.onChange(this);
	}
}