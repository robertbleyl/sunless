package org.sunless.client.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface SpectateNextPlayerHandler extends EventHandler {

	void onChange(SpectateNextPlayerEvent event);
}