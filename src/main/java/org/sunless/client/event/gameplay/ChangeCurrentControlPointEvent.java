package org.sunless.client.event.gameplay;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

import com.jme3.scene.Spatial;

public class ChangeCurrentControlPointEvent extends Event<ChangeCurrentControlPointEventHandler> {

	public static final EventType<ChangeCurrentControlPointEventHandler> TYPE = new EventType<>();

	private final Spatial controlPoint;

	public ChangeCurrentControlPointEvent(Spatial controlPoint) {
		this.controlPoint = controlPoint;
	}

	@Override
	public EventType<ChangeCurrentControlPointEventHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(ChangeCurrentControlPointEventHandler handler) {
		handler.onChangeCurrentControlPoint(this);
	}

	public Spatial getControlPoint() {
		return controlPoint;
	}
}