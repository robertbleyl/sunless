package org.sunless.client.event.gameplay;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;
import org.sunless.shared.gameplay.player.Player;

public class UpdateNearestTargetEvent extends Event<UpdateNearestTargetHandler> {

	public static final EventType<UpdateNearestTargetHandler> TYPE = new EventType<>();

	private final Player target;

	public UpdateNearestTargetEvent(Player target) {
		this.target = target;
	}

	@Override
	public EventType<UpdateNearestTargetHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(UpdateNearestTargetHandler handler) {
		handler.onUpdateNearestTarget(this);
	}

	public Player getTarget() {
		return target;
	}
}