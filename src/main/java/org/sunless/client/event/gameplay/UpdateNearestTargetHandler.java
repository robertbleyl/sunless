package org.sunless.client.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface UpdateNearestTargetHandler extends EventHandler {
	
	void onUpdateNearestTarget(UpdateNearestTargetEvent event);
}