package org.sunless.client.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface ChangeCurrentControlPointEventHandler extends EventHandler {

	void onChangeCurrentControlPoint(ChangeCurrentControlPointEvent event);
}