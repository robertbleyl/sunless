package org.sunless.client.event;

import org.sunless.shared.event.EventHandler;

public interface ToggleConsoleHandler extends EventHandler {
	
	void onToggleConsole(ToggleConsoleEvent event);
}