package org.sunless.client.event;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class ToggleConsoleEvent extends Event<ToggleConsoleHandler> {
	
	public static final EventType<ToggleConsoleHandler> TYPE = new EventType<>();
	
	@Override
	public EventType<ToggleConsoleHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(ToggleConsoleHandler handler) {
		handler.onToggleConsole(this);
	}
}