package org.sunless.client.event;

import org.sunless.shared.event.EventHandler;

public interface ToggleScoreBoardHandler extends EventHandler {
	
	void onToggleScoreBoard(ToggleScoreBoardEvent event);
}