package org.sunless.client.event;

import org.sunless.shared.event.EventHandler;

public interface ToggleMainMenuHandler extends EventHandler {
	
	void onChange(ToggleMainMenuEvent event);
}