package org.sunless.client.event;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class ToggleSpawnMenuEvent extends Event<ToggleSpawnMenuHandler> {
	
	public static final EventType<ToggleSpawnMenuHandler> TYPE = new EventType<>();
	
	@Override
	public EventType<ToggleSpawnMenuHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(ToggleSpawnMenuHandler handler) {
		handler.onChange(this);
	}
}