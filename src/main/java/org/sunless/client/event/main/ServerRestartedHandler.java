package org.sunless.client.event.main;

import org.sunless.shared.event.EventHandler;

public interface ServerRestartedHandler extends EventHandler {
	
	void onServerRestarted(ServerRestartedEvent event);
}