package org.sunless.client.event.main;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class UsernameSelectedEvent extends Event<UsernameSelectedHandler> {
	
	public static final EventType<UsernameSelectedHandler> TYPE = new EventType<>();
	
	private final String username;
	
	public UsernameSelectedEvent(String username) {
		this.username = username;
	}
	
	@Override
	public EventType<UsernameSelectedHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(UsernameSelectedHandler handler) {
		handler.onUsernameSelected(this);
	}
	
	public String getUsername() {
		return username;
	}
}