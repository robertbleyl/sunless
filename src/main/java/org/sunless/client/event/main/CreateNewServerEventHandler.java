package org.sunless.client.event.main;

import org.sunless.shared.event.EventHandler;

public interface CreateNewServerEventHandler extends EventHandler {

	void onCreateNewServer(CreateNewServerEvent event);
}