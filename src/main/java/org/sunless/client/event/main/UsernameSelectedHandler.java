package org.sunless.client.event.main;

import org.sunless.shared.event.EventHandler;

public interface UsernameSelectedHandler extends EventHandler {
	
	void onUsernameSelected(UsernameSelectedEvent event);
}