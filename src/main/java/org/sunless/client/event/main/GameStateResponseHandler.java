package org.sunless.client.event.main;

import org.sunless.shared.event.EventHandler;

public interface GameStateResponseHandler extends EventHandler {
	
	void onGameStateResponse(GameStateResponseEvent event);
}