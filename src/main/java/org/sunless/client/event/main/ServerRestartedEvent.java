package org.sunless.client.event.main;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;
import org.sunless.shared.gameplay.GameInfoInitData;

public class ServerRestartedEvent extends Event<ServerRestartedHandler> {
	
	public static final EventType<ServerRestartedHandler> TYPE = new EventType<>();
	
	private final GameInfoInitData data;
	
	public ServerRestartedEvent(GameInfoInitData data) {
		this.data = data;
	}
	
	@Override
	public EventType<ServerRestartedHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(ServerRestartedHandler handler) {
		handler.onServerRestarted(this);
	}
	
	public GameInfoInitData getData() {
		return data;
	}
}