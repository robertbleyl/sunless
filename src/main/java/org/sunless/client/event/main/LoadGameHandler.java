package org.sunless.client.event.main;

import org.sunless.shared.event.EventHandler;

public interface LoadGameHandler extends EventHandler {
	
	void onLoadGame(LoadGameEvent event);
}