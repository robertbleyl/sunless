package org.sunless.client.event.main;

import org.sunless.shared.event.EventHandler;

public interface QuitHandler extends EventHandler {
	
	void onQuit(QuitEvent e);
}