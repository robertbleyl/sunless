package org.sunless.client.event.main;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;
import org.sunless.shared.network.messages.toclient.gamestate.GameStateResponseMessage;

public class GameStateResponseEvent extends Event<GameStateResponseHandler> {
	
	public static final EventType<GameStateResponseHandler> TYPE = new EventType<>();
	
	private final GameStateResponseMessage message;
	
	public GameStateResponseEvent(GameStateResponseMessage message) {
		this.message = message;
	}
	
	@Override
	public EventType<GameStateResponseHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(GameStateResponseHandler handler) {
		handler.onGameStateResponse(this);
	}
	
	public GameStateResponseMessage getMessage() {
		return message;
	}
}