package org.sunless.client.event.main;

import org.sunless.shared.event.EventHandler;

public interface UpdateCursorEventHandler extends EventHandler {

	void onUpdateCursor(UpdateCursorEvent event);
}