package org.sunless.client.event.main;

import org.sunless.shared.event.EventHandler;

public interface CloseGameHandler extends EventHandler {
	
	void onCloseGame(CloseGameEvent event);
}