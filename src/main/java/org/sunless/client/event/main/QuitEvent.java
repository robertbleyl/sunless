package org.sunless.client.event.main;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class QuitEvent extends Event<QuitHandler> {
	
	public static final EventType<QuitHandler> TYPE = new EventType<>();
	
	@Override
	public EventType<QuitHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(QuitHandler handler) {
		handler.onQuit(this);
	}
}