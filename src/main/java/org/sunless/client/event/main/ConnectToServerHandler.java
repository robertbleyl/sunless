package org.sunless.client.event.main;

import org.sunless.shared.event.EventHandler;

public interface ConnectToServerHandler extends EventHandler {
	
	void onConnect(ConnectToServerEvent event);
}