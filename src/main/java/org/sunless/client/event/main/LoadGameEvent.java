package org.sunless.client.event.main;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;
import org.sunless.shared.gameplay.GameInfoInitData;

public class LoadGameEvent extends Event<LoadGameHandler> {
	
	public static final EventType<LoadGameHandler> TYPE = new EventType<>();
	
	private final GameInfoInitData data;
	
	public LoadGameEvent(GameInfoInitData data) {
		this.data = data;
	}
	
	@Override
	public EventType<LoadGameHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(LoadGameHandler handler) {
		handler.onLoadGame(this);
	}
	
	public GameInfoInitData getData() {
		return data;
	}
}