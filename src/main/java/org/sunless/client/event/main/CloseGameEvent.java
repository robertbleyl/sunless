package org.sunless.client.event.main;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

public class CloseGameEvent extends Event<CloseGameHandler> {
	
	public static final EventType<CloseGameHandler> TYPE = new EventType<>();
	
	@Override
	public EventType<CloseGameHandler> getType() {
		return TYPE;
	}
	
	@Override
	public void callHandler(CloseGameHandler handler) {
		handler.onCloseGame(this);
	}
}