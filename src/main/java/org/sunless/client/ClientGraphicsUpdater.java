package org.sunless.client;

import org.sunless.client.config.ClientConfigConstants;
import org.sunless.client.config.GameConfig;

import com.jme3.app.SimpleApplication;
import com.jme3.system.AppSettings;

public class ClientGraphicsUpdater {

	private final SimpleApplication app;

	public ClientGraphicsUpdater(SimpleApplication app) {
		this.app = app;
	}

	public void applyGraphicSettings() {
		AppSettings settings = new AppSettings(true);

		String resolutionString = GameConfig.getString(ClientConfigConstants.r_resolution);
		String[] resParts = resolutionString.split("x");
		Integer width = Integer.valueOf(resParts[0]);
		Integer height = Integer.valueOf(resParts[1]);
		settings.setWidth(width);
		settings.setHeight(height);

		settings.setFullscreen(GameConfig.getBoolean(ClientConfigConstants.r_fullscreen));
		settings.setBitsPerPixel(GameConfig.getInt(ClientConfigConstants.r_bits_per_pixel));
		settings.setFrequency(GameConfig.getInt(ClientConfigConstants.r_frequency));
		app.setSettings(settings);
	}
}