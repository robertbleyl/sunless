package org.sunless.client;

import java.io.File;

import org.sunless.client.config.ClientConfigFactory;
import org.sunless.client.config.GameConfig;
import org.sunless.shared.AbstractGameApplication;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.jme3.system.JmeContext;

public class ClientApp extends AbstractGameApplication {

	// private static final Logger log = LoggerFactory.getLogger(ClientApp.class);

	private final boolean clientConfigFileExisted;

	private ClientGraphicsUpdater graphicsUpdater;

	// TODO use
	private final String server;

	private Injector injector;

	public ClientApp(String server, JmeContext.Type jmeContextType) {
		super();
		this.server = server;

		setShowSettings(false);

		File clientConfigFile = new File(GameConfig.getClientConfigFileLocation());
		clientConfigFileExisted = clientConfigFile.exists();

		if (!clientConfigFileExisted) {
			ClientConfigFactory factory = new ClientConfigFactory();
			try {
				factory.initDefaultConfig(jmeContextType);
			} catch (Exception e) {
				// TODO
				e.printStackTrace();
			}
		}

		try {
			GameConfig.loadFromFile();

			graphicsUpdater = new ClientGraphicsUpdater(this);
			graphicsUpdater.applyGraphicSettings();
		} catch (Exception e) {
			// TODO
			e.printStackTrace();
		}
	}

	@Override
	public void simpleInitApp() {
		try {
			injector = Guice.createInjector(new ClientModule(this, graphicsUpdater));

			Client client = injector.getInstance(Client.class);
			client.init(clientConfigFileExisted);
		} catch (Exception e) {
			e.printStackTrace();

			stop();
		}
	}

	@Override
	public void simpleUpdate(float tpf) {
		super.simpleUpdate(tpf);

		if (context.getType() != JmeContext.Type.Headless) {
			listener.setLocation(cam.getLocation());
			listener.setRotation(cam.getRotation());
		}
	}

	public Injector getInjector() {
		return injector;
	}
}