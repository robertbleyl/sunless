package org.sunless.client.network;

import org.sunless.client.network.messagehandler.ClientMessageHandler;
import org.sunless.shared.network.AbstractMessageListener;

import com.jme3.network.Client;

@SuppressWarnings("rawtypes")
public class ClientMessageListener extends AbstractMessageListener<Client, ClientMessageHandler> {
}