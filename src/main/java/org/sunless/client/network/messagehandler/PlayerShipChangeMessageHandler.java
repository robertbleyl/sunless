package org.sunless.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sunless.client.network.ClientPlayerSyncControl;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.network.messages.toclient.gameplay.PlayerShipChangeMessage;

import com.jme3.network.Client;
import com.jme3.scene.Node;

public class PlayerShipChangeMessageHandler extends ClientMessageHandler<PlayerShipChangeMessage> {

	private static final Logger log = LoggerFactory.getLogger(PlayerShipChangeMessageHandler.class);

	@Override
	public Class<PlayerShipChangeMessage> getMessageClass() {
		return PlayerShipChangeMessage.class;
	}

	@Override
	public void handleMessage(Client conn, PlayerShipChangeMessage m) {
		long playerId = m.getPlayerId();
		Player player = playersContainer.getPlayer(playerId);

		if (player != null) {
			String shipName = m.getShipName();

			try {
				enqueueHelper.enqueue(new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						Node ship = entityFactory.createShip(shipName);
						ship.setUserData(EntityConstants.id, m.getEntityId());
						player.setShip(ship);

						if (player.isLocalPlayer()) {
							ship.addControl(new ClientPlayerSyncControl(clientState));
						}

						ship.setUserData(EntityConstants.player, player);

						return null;
					}
				});
			} catch (Exception e) {
				log.error("The ship model " + shipName + " could not be loaded!", e);
			}
		} else {
			log.error("The Player with id " + playerId + " was not found!");
		}
	}
}