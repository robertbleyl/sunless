package org.sunless.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.network.messages.toclient.gameplay.SpectatedPlayerUpdateMessage;

import com.jme3.network.Client;

public class SpectatedPlayerUpdateMessageHandler extends ClientMessageHandler<SpectatedPlayerUpdateMessage> {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Override
	public Class<SpectatedPlayerUpdateMessage> getMessageClass() {
		return SpectatedPlayerUpdateMessage.class;
	}

	@Override
	public void handleMessage(Client source, SpectatedPlayerUpdateMessage m) {
		Player player = playersContainer.getPlayer(m.getSpectatedPlayerId());

		if (player != null) {
			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					Player target = null;
					long targetPlayerId = m.getTargetPlayerId();

					if (targetPlayerId > 0L) {
						target = playersContainer.getPlayer(targetPlayerId);
					}

					player.setTarget(target);

					return null;
				}
			});
		} else {
			log.warn("Player " + m.getSpectatedPlayerId() + " not found!");
		}
	}
}