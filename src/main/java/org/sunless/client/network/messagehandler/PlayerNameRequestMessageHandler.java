package org.sunless.client.network.messagehandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sunless.client.config.ClientConfigConstants;
import org.sunless.client.config.GameConfig;
import org.sunless.shared.network.messages.toclient.main.PlayerNameRequestMessage;
import org.sunless.shared.network.messages.toserver.main.PlayerNameResponseMessage;

import com.jme3.network.Client;

public class PlayerNameRequestMessageHandler extends ClientMessageHandler<PlayerNameRequestMessage> {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Override
	public Class<PlayerNameRequestMessage> getMessageClass() {
		return PlayerNameRequestMessage.class;
	}

	@Override
	public void handleMessage(Client conn, PlayerNameRequestMessage m) {
		log.info("Sending PlayerNameResponseMessage...");
		clientState.sendMessage(new PlayerNameResponseMessage(GameConfig.getString(ClientConfigConstants.c_username)));
	}
}