package org.sunless.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.sunless.shared.event.gameplay.WinConditionReachedEvent;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.network.messages.toclient.gameplay.WinConditionReachedMessage;

import com.jme3.network.Client;

public class WinConditionReachedMessageHandler extends ClientMessageHandler<WinConditionReachedMessage> {

	@Override
	public Class<WinConditionReachedMessage> getMessageClass() {
		return WinConditionReachedMessage.class;
	}

	@Override
	public void handleMessage(Client conn, WinConditionReachedMessage m) {
		short teamId = m.getTeamId();
		final Team winnerTeam = teamsContainer.getTeam(teamId);

		enqueueHelper.enqueue(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				eventBus.fireEvent(new WinConditionReachedEvent(winnerTeam));
				return null;
			}
		});
	}
}