package org.sunless.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sunless.shared.event.gameplay.RespawnPlayerEvent;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.network.messages.toclient.gameplay.RespawnPlayerMessage;

import com.jme3.network.Client;
import com.jme3.scene.Spatial;

public class RespawnPlayerMessageHandler extends ClientMessageHandler<RespawnPlayerMessage> {

	private static final Logger log = LoggerFactory.getLogger(RespawnPlayerMessageHandler.class);

	@Override
	public Class<RespawnPlayerMessage> getMessageClass() {
		return RespawnPlayerMessage.class;
	}

	@Override
	public void handleMessage(Client conn, RespawnPlayerMessage m) {
		long playerId = m.getPlayerId();

		final Player player = playersContainer.getPlayer(playerId);

		if (player != null) {
			Spatial ship = player.getShip();

			if (ship != null) {
				enqueueHelper.enqueue(new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						eventBus.fireEvent(new RespawnPlayerEvent(player));
						return null;
					}
				});
			} else {
				log.error("The player " + playerId + " does not have a ship and thus cannot be respawned!");
			}
		} else {
			log.error("The Player with id " + playerId + " was not found!");
		}
	}
}