package org.sunless.client.network.messagehandler;

import org.sunless.shared.event.config.DebugConfigChangedEvent;
import org.sunless.shared.network.messages.toclient.config.DebugConfigChangedMessage;

import com.jme3.network.Client;

public class DebugConfigChangedMessageHandler extends ClientMessageHandler<DebugConfigChangedMessage> {

	@Override
	public Class<DebugConfigChangedMessage> getMessageClass() {
		return DebugConfigChangedMessage.class;
	}

	@Override
	public void handleMessage(Client source, DebugConfigChangedMessage m) {
		eventBus.fireEvent(new DebugConfigChangedEvent(m.getDebugCommandKey(), m.getArguments()));
	}
}