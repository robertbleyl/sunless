package org.sunless.client.network.messagehandler;

import org.sunless.shared.event.main.GameTimeUpdateEvent;
import org.sunless.shared.network.messages.toclient.main.GameTimeUpdateMessage;

import com.jme3.network.Client;

public class GameTimeUpdateMessageHandler extends ClientMessageHandler<GameTimeUpdateMessage> {

	@Override
	public Class<GameTimeUpdateMessage> getMessageClass() {
		return GameTimeUpdateMessage.class;
	}

	@Override
	public void handleMessage(Client conn, GameTimeUpdateMessage m) {
		eventBus.fireEvent(new GameTimeUpdateEvent(m.getCurrentGameTime()));
	}
}