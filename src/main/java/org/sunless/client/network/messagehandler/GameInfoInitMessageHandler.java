package org.sunless.client.network.messagehandler;

import org.sunless.client.event.main.LoadGameEvent;
import org.sunless.shared.network.messages.toclient.main.GameInfoInitMessage;

import com.jme3.network.Client;

public class GameInfoInitMessageHandler extends ClientMessageHandler<GameInfoInitMessage> {

	@Override
	public Class<GameInfoInitMessage> getMessageClass() {
		return GameInfoInitMessage.class;
	}

	@Override
	public void handleMessage(Client conn, GameInfoInitMessage m) {
		eventBus.fireEvent(new LoadGameEvent(m.getData()));
	}
}