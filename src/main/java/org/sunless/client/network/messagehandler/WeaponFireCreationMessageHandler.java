package org.sunless.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sunless.shared.config.DebugConfigConstants;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.WeaponSlotContainer;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.config.DebugConfigChangedEvent;
import org.sunless.shared.event.config.DebugConfigChangedHandler;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.weapon.WeaponFireControl;
import org.sunless.shared.gameplay.weapon.WeaponFireCreationData;
import org.sunless.shared.network.messages.toclient.gameplay.WeaponFireCreationMessage;

import com.jme3.audio.AudioNode;
import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class WeaponFireCreationMessageHandler extends ClientMessageHandler<WeaponFireCreationMessage> {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final EventBus eventBus = EventBus.get();

	private boolean syncWeaponFireMovement;

	public WeaponFireCreationMessageHandler() {
		syncWeaponFireMovement = true;

		eventBus.addHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				if (event.getDebugCommandKey().equals(DebugConfigConstants.dbg_toggle_weapon_fire_movement)) {
					syncWeaponFireMovement = !syncWeaponFireMovement;
				}
			}
		});
	}

	@Override
	public Class<WeaponFireCreationMessage> getMessageClass() {
		return WeaponFireCreationMessage.class;
	}

	@Override
	public void handleMessage(Client conn, WeaponFireCreationMessage m) {
		enqueueHelper.enqueue(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				long weaponFireId = m.getWeaponFireId();
				Vector3f direction = m.getDirection();

				long shipId = m.getShipId();
				Node ship = (Node)clientSyncState.getEntity(shipId);

				if (ship != null) {
					String weaponSlotsContainerName = m.getWeaponSlotsContainerName();

					Spatial weaponFire = entityFactory.createWeaponFire(new WeaponFireCreationData(m.getStartLocation(), ship, weaponSlotsContainerName, direction));

					if (syncWeaponFireMovement) {
						WeaponSlotContainer weaponSlotContainer = ship.getUserData(weaponSlotsContainerName);

						if (weaponSlotContainer != null) {
							float speed = weaponSlotContainer.getWeaponFireSpeed();
							weaponFire.addControl(new WeaponFireControl(speed, direction));
						} else {
							log.error("The ship with id " + shipId + " has no weapon slot container '" + weaponSlotsContainerName + "'!");
						}
					}

					AudioNode audioNode = (AudioNode)ship.getChild(EntityConstants.weaponFireAudioNode);

					if (audioNode != null) {
						Player player = ship.getUserData(EntityConstants.player);

						if (player != null && player.isLocalPlayer()) {
							audioNode.setPositional(false);
						}

						audioNode.playInstance();
					}

					weaponFire.setUserData(EntityConstants.id, weaponFireId);

					entityAttacher.attachEntity(weaponFire);
				} else {
					log.warn("The ship with id " + shipId + " was not found!");
				}

				return null;
			}
		});
	}
}