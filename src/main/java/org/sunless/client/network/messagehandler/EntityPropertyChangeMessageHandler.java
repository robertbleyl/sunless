package org.sunless.client.network.messagehandler;

import java.util.concurrent.Callable;

import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.event.gameplay.DestroyShipEvent;
import org.sunless.shared.gameplay.EntityPropertyCauseSync;
import org.sunless.shared.gameplay.EntityPropertyChangeCause;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.network.messages.toclient.gameplay.EntityPropertyChangeMessage;

import com.jme3.network.Client;
import com.jme3.scene.Spatial;

public class EntityPropertyChangeMessageHandler extends ClientMessageHandler<EntityPropertyChangeMessage> {

	@Override
	public Class<EntityPropertyChangeMessage> getMessageClass() {
		return EntityPropertyChangeMessage.class;
	}

	@Override
	public void handleMessage(Client conn, EntityPropertyChangeMessage m) {
		long entityId = m.getEntityId();
		String propertyName = m.getPropertyName();
		Object propertyValue = m.getPropertyValue();
		EntityPropertyCauseSync cause = m.getCause();

		final Spatial entity = clientSyncState.getEntity(entityId);

		if (entity != null) {
			EntityPropertyChangeCause c = null;

			if (cause != null) {
				c = cause.createCollisionCause(clientSyncState);
			}

			EntityHelper.changeProperty(entity, propertyName, propertyValue, c);

			if (propertyName.equals(EntityConstants.hitPoints)) {
				int currentHitPoints = (int)propertyValue;

				if (currentHitPoints <= 0) {
					enqueueHelper.enqueue(new Callable<Void>() {
						@Override
						public Void call() throws Exception {
							eventBus.fireEvent(new DestroyShipEvent(entity));
							return null;
						}
					});

					Player player = entity.getUserData(EntityConstants.player);

					if (player != null) {
						player.setWantsRespawn(player.isBot());
						player.setAlive(false);

						if (player.isLocalPlayer()) {

						}
					}
				}
			}
		} else {
			// log.warn("The entity with id " + entityId + " was not found!");
		}
	}
}