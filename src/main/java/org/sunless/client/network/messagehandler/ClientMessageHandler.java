package org.sunless.client.network.messagehandler;

import org.sunless.client.gameplay.ClientPlayersContainer;
import org.sunless.client.network.ClientState;
import org.sunless.client.network.ClientSyncState;
import org.sunless.shared.network.AbstractMessageHandler;

import com.google.inject.Inject;
import com.jme3.network.Client;
import com.jme3.network.Message;

public abstract class ClientMessageHandler<M extends Message> extends AbstractMessageHandler<M, Client> {

	protected ClientState clientState;
	protected ClientPlayersContainer playersContainer;
	protected ClientSyncState clientSyncState;

	@Inject
	public void setClientState(ClientState clientState) {
		this.clientState = clientState;
	}

	@Inject
	public void setPlayersContainer(ClientPlayersContainer playersContainer) {
		this.playersContainer = playersContainer;
	}

	@Inject
	public void setClientSyncState(ClientSyncState clientSyncState) {
		this.clientSyncState = clientSyncState;
	}
}