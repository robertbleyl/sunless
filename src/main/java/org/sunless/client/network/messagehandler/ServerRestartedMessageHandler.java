package org.sunless.client.network.messagehandler;

import org.sunless.client.event.main.LoadGameEvent;
import org.sunless.shared.network.messages.toclient.main.ServerRestartedMessage;

import com.jme3.network.Client;

public class ServerRestartedMessageHandler extends ClientMessageHandler<ServerRestartedMessage> {

	@Override
	public Class<ServerRestartedMessage> getMessageClass() {
		return ServerRestartedMessage.class;
	}

	@Override
	public void handleMessage(Client source, ServerRestartedMessage m) {
		eventBus.fireEvent(new LoadGameEvent(m.getData()));
	}
}