package org.sunless.client.network;

import com.google.inject.Singleton;
import com.jme3.network.Network;
import com.jme3.network.NetworkClient;

@Singleton
public class ClientFactoryImpl implements ClientFactory {

	@Override
	public NetworkClient createClient() {
		return Network.createClient();
	}
}