package org.sunless.client;

import org.sunless.shared.config.DebugConfigConstants;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.config.DebugConfigChangedEvent;
import org.sunless.shared.event.config.DebugConfigChangedHandler;
import org.sunless.shared.util.DebugState;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingBox;
import com.jme3.bounding.BoundingSphere;
import com.jme3.bounding.BoundingVolume;
import com.jme3.bullet.collision.shapes.BoxCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.collision.shapes.SphereCollisionShape;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;
import com.jme3.scene.debug.WireBox;
import com.jme3.scene.debug.WireSphere;

public class BoundingVolumeDisplayControl extends AbstractControl {

	private final EventBus eventBus = EventBus.get();

	private final AssetManager assetManager;
	private final String debugKey;

	private Geometry geom;
	private Mesh mesh;

	public BoundingVolumeDisplayControl(AssetManager assetManager, String debugKey) {
		this.assetManager = assetManager;
		this.debugKey = debugKey;

		enabled = false;

		eventBus.addHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				if (event.getDebugCommandKey().equals(debugKey)) {
					setEnabled(!isEnabled());
				}
			}
		});
	}

	@Override
	protected void controlUpdate(float tpf) {

	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}

	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);

		Node node = (Node)spatial;

		if (enabled) {
			node.attachChild(geom);
		} else {
			node.detachChild(geom);
		}
	}

	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);

		if (debugKey.equals(DebugConfigConstants.dbg_toggle_show_weaponfire_bounding_volumes)) {
			RigidBodyControl control = spatial.getControl(RigidBodyControl.class);

			CollisionShape shape = control.getCollisionShape();

			if (shape instanceof SphereCollisionShape) {
				SphereCollisionShape sphere = (SphereCollisionShape)shape;
				mesh = new WireSphere(sphere.getRadius());
			} else if (shape instanceof BoxCollisionShape) {
				BoxCollisionShape box = (BoxCollisionShape)shape;
				mesh = new WireBox(box.getHalfExtents().x, box.getHalfExtents().y, box.getHalfExtents().z);
			}
		} else {
			BoundingVolume boundingVolume = spatial.getWorldBound();

			if (boundingVolume.getType() == BoundingVolume.Type.AABB) {
				BoundingBox box = (BoundingBox)boundingVolume;
				mesh = new WireBox(box.getXExtent(), box.getYExtent(), box.getZExtent());
			} else if (boundingVolume.getType() == BoundingVolume.Type.Sphere) {
				BoundingSphere sphere = (BoundingSphere)boundingVolume;
				mesh = new WireSphere(sphere.getRadius());
			}
		}

		geom = new Geometry("Geom for bounding volume", mesh);
		Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
		mat.getAdditionalRenderState().setWireframe(true);
		mat.setColor("Color", ColorRGBA.Green);
		geom.setMaterial(mat);

		setEnabled(DebugState.get().isToggled(debugKey));
	}
}