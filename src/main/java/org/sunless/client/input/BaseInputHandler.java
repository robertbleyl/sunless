package org.sunless.client.input;

import org.sunless.client.event.ToggleConsoleEvent;
import org.sunless.client.event.ToggleMainMenuEvent;
import org.sunless.client.event.gameplay.SpectateNextPlayerEvent;
import org.sunless.shared.event.EventBus;

import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;

public class BaseInputHandler implements ActionListener, InputConstants {

	private final EventBus eventBus = EventBus.get();

	public BaseInputHandler(InputManager inputManager) {
		inputManager.addMapping(TOGGLE_MAIN_MENU, new KeyTrigger(KeyInput.KEY_ESCAPE));
		inputManager.addMapping(TOGGLE_CONSOLE, new KeyTrigger(KeyInput.KEY_F12));
		inputManager.addMapping(SPECTATE_NEXT_PLAYER, new KeyTrigger(KeyInput.KEY_RIGHT));
		inputManager.addMapping(SPECTATE_PREVIOUS_PLAYER, new KeyTrigger(KeyInput.KEY_LEFT));

		inputManager.addListener(this, TOGGLE_MAIN_MENU, TOGGLE_CONSOLE, SPECTATE_NEXT_PLAYER);
	}

	@Override
	public void onAction(String name, boolean isPressed, float tpf) {
		switch (name) {
			case TOGGLE_MAIN_MENU:
				if (isPressed) {
					eventBus.fireEvent(new ToggleMainMenuEvent());
				}
				break;
			case TOGGLE_CONSOLE:
				if (isPressed) {
					eventBus.fireEvent(new ToggleConsoleEvent());
				}
				break;
			case SPECTATE_NEXT_PLAYER:
				if (isPressed) {
					eventBus.fireEvent(new SpectateNextPlayerEvent());
				}
				break;
		}
	}
}