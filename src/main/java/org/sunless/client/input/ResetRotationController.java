package org.sunless.client.input;

import org.sunless.shared.control.RotationConstants;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public class ResetRotationController extends AbstractControl {

	private Quaternion targetRotation;
	private RigidBodyControl bodyControl;

	@Override
	protected void controlUpdate(float tpf) {
		if (done()) {
			setEnabled(false);
		} else {
			Quaternion originRotation = bodyControl.getPhysicsRotation();
			originRotation.slerp(targetRotation, tpf * 1.5f);
			bodyControl.setPhysicsRotation(originRotation);
		}
	}

	private boolean done() {
		Quaternion rot = bodyControl.getPhysicsRotation();

		if (!compare(rot.getW(), targetRotation.getW()) || !compare(rot.getX(), targetRotation.getX()) || !compare(rot.getY(), targetRotation.getY()) || !compare(rot.getZ(), targetRotation.getZ())) {
			return false;
		}

		return true;
	}

	public void updateRotation() {
		Vector3f physicsLocation = bodyControl.getPhysicsLocation();

		targetRotation = bodyControl.getPhysicsRotation();
		Vector3f forward = targetRotation.getRotationColumn(RotationConstants.FORWARD);

		Vector3f point = physicsLocation.add(forward);
		point.setY(physicsLocation.getY());
		Vector3f dir = point.subtract(physicsLocation);

		targetRotation.lookAt(dir, Vector3f.UNIT_Y);
	}

	private boolean compare(float a, float b) {
		float tolerance = 0.01f;
		return FastMath.abs(a - b) <= tolerance;
	}

	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);

		if (spatial != null) {
			bodyControl = spatial.getControl(RigidBodyControl.class);
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}

}