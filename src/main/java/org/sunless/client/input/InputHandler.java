package org.sunless.client.input;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sunless.client.config.ClientConfigConstants;
import org.sunless.client.config.GameConfig;
import org.sunless.client.event.ClientConfigChangedEvent;
import org.sunless.client.event.ClientConfigChangedHandler;
import org.sunless.client.event.ToggleScoreBoardEvent;
import org.sunless.client.event.ToggleSpawnMenuEvent;
import org.sunless.client.network.ClientState;
import org.sunless.shared.control.MovementInfo;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.EventHandlersContainer;
import org.sunless.shared.event.gameplay.RespawnPlayerEvent;
import org.sunless.shared.event.gameplay.RespawnPlayerHandler;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.network.messages.toserver.input.FireWeaponsMessage;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.input.InputManager;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.AnalogListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseAxisTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.input.controls.Trigger;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;

@Singleton
public class InputHandler extends AbstractControl implements ActionListener, AnalogListener, InputConstants {

	private final EventBus eventBus = EventBus.get();
	private final EventHandlersContainer eventHandlersContainer = new EventHandlersContainer();

	private final InputManager inputManager;
	private final ClientState clientState;
	private final Camera camera;

	private boolean rotateButtonPressed = true;

	private boolean standardWeaponShootButtonIsPressed;
	private boolean specialWeaponShootButtonIsPressed;

	private final ResetRotationController resetRotationController = new ResetRotationController();

	private final Collection<String> mappingNames = new ArrayList<>();

	private String[] mappingNamesArray;

	private Player player;

	private MovementInfo moveInfo;

	@Inject
	public InputHandler(InputManager inputManager, ClientState clientState, Camera camera) throws IllegalAccessException {
		this.inputManager = inputManager;
		this.clientState = clientState;
		this.camera = camera;

		inputManager.clearMappings();
		init();
	}

	private void init() throws IllegalAccessException {
		setEnabled(false);

		eventHandlersContainer.addHandler(RespawnPlayerEvent.TYPE, new RespawnPlayerHandler() {
			@Override
			public void onRespawnPlayer(RespawnPlayerEvent event) {
				if (event.getPlayer().isLocalPlayer()) {
					respawn(event);
				}
			}
		});

		eventHandlersContainer.addHandler(ClientConfigChangedEvent.TYPE, new ClientConfigChangedHandler() {
			@Override
			public void onChanged(ClientConfigChangedEvent event) {
				String key = event.getKey();

				if (key.startsWith("bind_")) {
					try {
						inputManager.deleteMapping(key);
						inputManager.addMapping(key, createTrigger(key));
					} catch (IllegalAccessException e) {
						// TODO
						e.printStackTrace();
					}
				}
			}
		});

		initMappings();
	}

	private void initMappings() throws IllegalAccessException {
		addMapping(MOVE_MOUSE_LEFT, new MouseAxisTrigger(MouseInput.AXIS_X, true));
		addMapping(MOVE_MOUSE_RIGHT, new MouseAxisTrigger(MouseInput.AXIS_X, false));
		addMapping(MOVE_MOUSE_UP, new MouseAxisTrigger(MouseInput.AXIS_Y, false));
		addMapping(MOVE_MOUSE_DOWN, new MouseAxisTrigger(MouseInput.AXIS_Y, true));

		Collection<String> bindingNames = GameConfig.getBindingNames();

		for (String bindingName : bindingNames) {
			addMapping(bindingName, createTrigger(bindingName));
		}

		mappingNamesArray = mappingNames.toArray(new String[mappingNames.size()]);
	}

	private Trigger createTrigger(String inputName) throws IllegalAccessException {
		String inputBindingString = GameConfig.getString(inputName);

		Class<KeyInput> keyClass = KeyInput.class;

		for (Field field : keyClass.getFields()) {
			if (field.getName().equals(inputBindingString)) {
				int value = field.getInt(null);
				return new KeyTrigger(value);
			}
		}

		Class<MouseInput> mouseClass = MouseInput.class;

		for (Field field : mouseClass.getFields()) {
			if (field.getName().equals(inputBindingString)) {
				int value = field.getInt(null);
				return new MouseButtonTrigger(value);
			}
		}

		return null;
	}

	private void addMapping(String name, Trigger... triggers) {
		inputManager.addMapping(name, triggers);
		mappingNames.add(name);
	}

	private void respawn(RespawnPlayerEvent event) {
		if (player != null) {
			Node ship = player.getShip();

			if (player != null && ship != null) {
				ship.removeControl(resetRotationController);
			}
		}

		player = event.getPlayer();
		Node ship = player.getShip();
		ship.removeControl(resetRotationController);
		resetRotationController.setSpatial(null);

		setEnabled(true);

		inputManager.removeListener(this);
		inputManager.addListener(this, mappingNamesArray);

		resetRotationController.setEnabled(false);
		ship.addControl(resetRotationController);
	}

	@Override
	public void onAnalog(String name, float value, float tpf) {
		float changeAmount = getChangeAmount(tpf);

		switch (name) {
			case ClientConfigConstants.bind_forward:
				moveForward(changeAmount);
				break;
			case ClientConfigConstants.bind_backward:
				moveForward(-changeAmount);
				break;
			case ClientConfigConstants.bind_shoot_standard_weapons:
				if (standardWeaponShootButtonIsPressed) {
					Vector3f mouseDirection = getMouseDirection();
					clientState.sendMessage(new FireWeaponsMessage(mouseDirection, EntityConstants.standardWeaponSlots));
				}
				break;
			case ClientConfigConstants.bind_shoot_special_weapons:
				if (specialWeaponShootButtonIsPressed) {
					Vector3f mouseDirection = getMouseDirection();
					clientState.sendMessage(new FireWeaponsMessage(mouseDirection, EntityConstants.specialWeaponSlots));
				}
				break;
		}
	}

	protected float getChangeAmount(float tpf) {
		return tpf * 0.6f;
	}

	@Override
	public void onAction(String name, boolean isPressed, float tpf) {
		switch (name) {
			case ClientConfigConstants.bind_roll_left:
				moveInfo.setRollLeft(isPressed);
				break;
			case ClientConfigConstants.bind_roll_right:
				moveInfo.setRollRight(isPressed);
				break;
			case ClientConfigConstants.bind_strafe_left:
				moveInfo.setStrafeLeft(isPressed);
				break;
			case ClientConfigConstants.bind_strafe_right:
				moveInfo.setStrafeRight(isPressed);
				break;
			case ClientConfigConstants.bind_boost:
				moveInfo.setBoosting(isPressed);
				break;
			case ClientConfigConstants.bind_shoot_standard_weapons:
				standardWeaponShootButtonIsPressed = isPressed;
				break;
			case ClientConfigConstants.bind_shoot_special_weapons:
				specialWeaponShootButtonIsPressed = isPressed;
				break;
			case ClientConfigConstants.bind_free_mouse:
				// boolean wasPressed = rotateButtonPressed;

				rotateButtonPressed = !isPressed;
				//
				// if (isPressed) {
				// resetRotationController.setEnabled(false);
				// }
				//
				// if (wasPressed && !isPressed) {
				// resetRotationController.updateRotation();
				// resetRotationController.setEnabled(true);
				// }

				break;
			case ClientConfigConstants.bind_nearest_enemy_target:
				if (isPressed) {
					Player target = getNearestTarget();
					player.setTarget(target);
				}

				break;
			case ClientConfigConstants.bind_toggle_score_board:
				eventBus.fireEvent(new ToggleScoreBoardEvent(isPressed));
				break;
			case ClientConfigConstants.bind_toggle_spawn_menu:
				if (isPressed) {
					eventBus.fireEvent(new ToggleSpawnMenuEvent());
				}
				break;
		}
	}

	private Player getNearestTarget() {
		Player target = null;
		float minDist = Float.MAX_VALUE;

		List<Player> players = player.getEnemyTeam().getPlayers();

		Vector3f pos = player.getShip().getLocalTranslation();

		for (Player enemy : players) {
			Node ship = enemy.getShip();

			if (ship != null && enemy.isAlive()) {
				float distance = pos.distance(ship.getLocalTranslation());

				if (distance < minDist) {
					minDist = distance;
					target = enemy;
				}
			}
		}

		return target;
	}

	@Override
	public void controlUpdate(float tpf) {
		if (rotateButtonPressed) {
			Vector2f click2d = inputManager.getCursorPosition();
			float halfWidth = camera.getWidth() / 2f;
			float halfHeight = camera.getHeight() / 2f;
			Vector2f center = new Vector2f(halfWidth, halfHeight);

			float xDiff = center.x - click2d.x;
			float yDiff = center.y - click2d.y;

			moveInfo.setRotationPercentageX(xDiff / halfWidth);
			moveInfo.setRotationPercentageY(yDiff / halfHeight);
		} else {
			moveInfo.setRotationPercentageX(0f);
			moveInfo.setRotationPercentageY(0f);
		}
	}

	private void moveForward(float amount) {
		if (moveInfo.getForwardSpeedPercentage() <= 1f) {
			moveInfo.setForwardSpeedPercentage(moveInfo.getForwardSpeedPercentage() + amount);
		}

		if (moveInfo.getForwardSpeedPercentage() > 1f) {
			moveInfo.setForwardSpeedPercentage(1f);
		} else if (moveInfo.getForwardSpeedPercentage() < 0f) {
			moveInfo.setForwardSpeedPercentage(0f);
		}
	}

	public Vector3f getMouseDirection() {
		Vector2f click2d = inputManager.getCursorPosition().clone();

		Vector3f click3d0 = camera.getWorldCoordinates(click2d, 0f);
		Vector3f click3d1 = camera.getWorldCoordinates(click2d, 1f);

		Vector3f viewDir = click3d1.subtract(click3d0).normalize();
		return viewDir;
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}

	public void setMoveInfo(MovementInfo moveInfo) {
		this.moveInfo = moveInfo;
	}

	public void cleanup() {
		eventHandlersContainer.cleanup();
	}
}