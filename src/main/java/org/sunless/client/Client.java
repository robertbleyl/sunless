package org.sunless.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sunless.client.config.ClientConfigConstants;
import org.sunless.client.config.GameConfig;
import org.sunless.client.event.ClientConfigChangedEvent;
import org.sunless.client.event.ClientConfigChangedHandler;
import org.sunless.client.event.ToggleConsoleEvent;
import org.sunless.client.event.ToggleConsoleHandler;
import org.sunless.client.event.ToggleMainMenuEvent;
import org.sunless.client.event.ToggleMainMenuHandler;
import org.sunless.client.event.ToggleScoreBoardEvent;
import org.sunless.client.event.ToggleScoreBoardHandler;
import org.sunless.client.event.ToggleSpawnMenuEvent;
import org.sunless.client.event.ToggleSpawnMenuHandler;
import org.sunless.client.event.config.UpdateDebugOptionEvent;
import org.sunless.client.event.config.UpdateDebugOptionHandler;
import org.sunless.client.event.gameplay.SpectateNextPlayerEvent;
import org.sunless.client.event.gameplay.SpectateNextPlayerHandler;
import org.sunless.client.event.main.CloseGameEvent;
import org.sunless.client.event.main.CloseGameHandler;
import org.sunless.client.event.main.ConnectToServerEvent;
import org.sunless.client.event.main.CreateNewServerEvent;
import org.sunless.client.event.main.CreateNewServerEventHandler;
import org.sunless.client.event.main.GameStateResponseEvent;
import org.sunless.client.event.main.GameStateResponseHandler;
import org.sunless.client.event.main.LoadGameEvent;
import org.sunless.client.event.main.LoadGameHandler;
import org.sunless.client.event.main.QuitEvent;
import org.sunless.client.event.main.QuitHandler;
import org.sunless.client.event.main.UpdateCursorEvent;
import org.sunless.client.event.main.UpdateCursorEventHandler;
import org.sunless.client.event.main.UsernameSelectedEvent;
import org.sunless.client.event.main.UsernameSelectedHandler;
import org.sunless.client.gameplay.ClientIngameState;
import org.sunless.client.gameplay.ClientPlayersContainer;
import org.sunless.client.gameplay.RespawnLocalPlayerHandler;
import org.sunless.client.input.BaseInputHandler;
import org.sunless.client.input.InputHandler;
import org.sunless.client.loading.ClientLoadingStepsBuilder;
import org.sunless.client.network.ClientMessageListener;
import org.sunless.client.network.ClientState;
import org.sunless.client.network.ClientSyncState;
import org.sunless.client.ui.console.ConsoleState;
import org.sunless.client.ui.end.EndState;
import org.sunless.client.ui.end.EndStateImpl;
import org.sunless.client.ui.hud.HUDState;
import org.sunless.client.ui.menu.MenuState;
import org.sunless.client.ui.profileselection.UsernameInputState;
import org.sunless.client.ui.scoreboard.ScoreBoardState;
import org.sunless.client.ui.spawnmenu.SpawnMenuState;
import org.sunless.shared.ApplicationStopHelper;
import org.sunless.shared.NodeConstants;
import org.sunless.shared.config.DebugConfigConstants;
import org.sunless.shared.control.MoveControl;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityFactory;
import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.config.DebugConfigChangedEvent;
import org.sunless.shared.event.config.UpdateGraphicsConfigEvent;
import org.sunless.shared.event.gameplay.EntityPropertyChangeEvent;
import org.sunless.shared.event.gameplay.RespawnPlayerEvent;
import org.sunless.shared.event.gameplay.RespawnPlayerHandler;
import org.sunless.shared.event.gameplay.WinConditionReachedEvent;
import org.sunless.shared.event.gameplay.WinConditionReachedHandler;
import org.sunless.shared.event.main.RestartGameEvent;
import org.sunless.shared.event.main.RestartGameHandler;
import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.GameInfoInitData;
import org.sunless.shared.gameplay.GameInfoProvider;
import org.sunless.shared.gameplay.PhysicsWrapper;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.json.Faction;
import org.sunless.shared.json.JsonHelper;
import org.sunless.shared.json.MapData;
import org.sunless.shared.loading.EnqueueHelper;
import org.sunless.shared.loading.LoadingState;
import org.sunless.shared.loading.LoadingStateImpl;
import org.sunless.shared.loading.LoadingStep;
import org.sunless.shared.loading.LoadingStepBuildRequest;
import org.sunless.shared.loading.LoadingThread;
import org.sunless.shared.network.messages.toclient.gamestate.GameStateResponseMessage;
import org.sunless.shared.network.messages.toclient.gamestate.PropertyInitEntry;
import org.sunless.shared.network.messages.toclient.sync.ShipSyncMessage;
import org.sunless.shared.network.messages.toclient.sync.WeaponFireSyncMessage;
import org.sunless.shared.network.messages.toserver.GameStateRequestMessage;
import org.sunless.shared.network.messages.toserver.config.DebugConfigChangeRequestMessage;
import org.sunless.shared.state.fps.FPSState;
import org.sunless.shared.ui.ScreenUtils;
import org.sunless.shared.util.DebugState;
import org.sunless.shared.util.FileConstants;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.app.FlyCamAppState;
import com.jme3.app.StatsAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.asset.AssetKey;
import com.jme3.asset.AssetManager;
import com.jme3.asset.StreamAssetInfo;
import com.jme3.asset.TextureKey;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.BulletAppState.ThreadingType;
import com.jme3.bullet.PhysicsSpace;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.cursors.plugins.CursorLoader;
import com.jme3.cursors.plugins.JmeCursor;
import com.jme3.input.InputManager;
import com.jme3.math.Vector3f;
import com.jme3.post.FilterPostProcessor;
import com.jme3.post.filters.CartoonEdgeFilter;
import com.jme3.renderer.Camera;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

import tonegod.gui.core.Screen;

@Singleton
public class Client {

	private static final Logger log = LoggerFactory.getLogger(Client.class);

	private final EventBus eventBus = EventBus.get();

	private EnqueueHelper enqueueHelper;
	private AppStateManager stateManager;
	private AssetManager assetManager;
	private InputManager inputManager;
	private InputHandler inputHandler;
	private Node appRootNode;
	private Node appGuiNode;
	private Screen screen;
	private Camera cam;
	private ViewPort viewPort;
	private GameInfoProvider gameInfoProvider;
	private ClientLoadingStepsBuilder clientLoadingStepsBuilder;
	private UsernameInputState usernameInputState;
	private MenuState menuState;
	private ConsoleState consoleState;
	private ClientState clientState;
	private LoadingState loadingState;
	private ClientIngameState ingameState;
	private FPSState fpsState;
	private SpawnMenuState spawnMenuState;
	private HUDState hudState;
	private EntityFactory entityFactory;
	private ClientSyncState syncState;
	private ClientPlayersContainer playersContainer;
	private ScoreBoardState scoreBoardState;
	private EndState endState;
	private TeamsContainer teamsContainer;
	private PhysicsWrapper physicsWrapper;
	private ApplicationStopHelper applicationStopHelper;
	private ClientGraphicsUpdater graphicsUpdater;
	private ClientMessageListener clientMessageListener;

	private LoadingThread loadingThread;

	private BulletAppState bulletAppState;

	private Player localPlayer;

	private boolean hudStateWasEnabled;

	private RespawnLocalPlayerHandler respawnLocalPlayerHandler;

	private boolean enableDebug;
	private JmeCursor standardCursor;
	private JmeCursor hudCursor;

	private Process serverProcess;

	private BotSpectatingDebugControl botSpectatingDebugControl;

	public void init(boolean clientConfigExisted) throws Exception {
		stateManager.detach(stateManager.getState(FlyCamAppState.class));
		stateManager.detach(stateManager.getState(StatsAppState.class));

		FilterPostProcessor fpp = new FilterPostProcessor(assetManager);
		CartoonEdgeFilter toon = new CartoonEdgeFilter();
		fpp.addFilter(toon);
		viewPort.addProcessor(fpp);

		appGuiNode.detachAllChildren();
		appGuiNode.addControl(screen);

		botSpectatingDebugControl = new BotSpectatingDebugControl(appGuiNode, cam, assetManager, enqueueHelper);
		appGuiNode.addControl(botSpectatingDebugControl);

		stateManager.attach(consoleState);
		consoleState.setEnabled(false);

		stateManager.attach(menuState);

		if (clientConfigExisted) {
			menuState.setEnabled(true);
		} else {
			stateManager.attach(usernameInputState);
			usernameInputState.setEnabled(true);
			menuState.setEnabled(false);
		}

		stateManager.attach(ingameState);
		ingameState.setEnabled(false);

		stateManager.attach(fpsState);
		fpsState.setEnabled(true);

		stateManager.attach(syncState);
		syncState.setEnabled(false);

		bulletAppState = new BulletAppState();
		bulletAppState.setThreadingType(ThreadingType.PARALLEL);
		stateManager.attach(bulletAppState);
		bulletAppState.setEnabled(false);

		PhysicsSpace physicsSpace = bulletAppState.getPhysicsSpace();
		physicsSpace.setGravity(new Vector3f());

		physicsWrapper.setPhysicsSpace(physicsSpace);

		setupNetwork();
		stateManager.attach(clientState);

		respawnLocalPlayerHandler = new RespawnLocalPlayerHandler(cam, assetManager, inputHandler);
		eventBus.addHandler(RespawnPlayerEvent.TYPE, respawnLocalPlayerHandler);

		try (InputStream inputStream = getClass().getResourceAsStream("/" + FileConstants.HUD_PATH + "hudCursor.ico");) {
			CursorLoader loader = new CursorLoader();
			AssetKey<?> key = new TextureKey("hudCursor.ico");

			hudCursor = loader.load(new StreamAssetInfo(assetManager, key, inputStream));
			hudCursor.setxHotSpot(16);
			hudCursor.setyHotSpot(16);
		} catch (IOException e) {
			// TODO
			e.printStackTrace();
		}

		try (InputStream inputStream = getClass().getResourceAsStream("/" + FileConstants.UI_PATH + "standardCursor.ico");) {
			CursorLoader loader = new CursorLoader();
			AssetKey<?> key = new TextureKey("standardCursor.ico");

			standardCursor = loader.load(new StreamAssetInfo(assetManager, key, inputStream));
			standardCursor.setxHotSpot(0);
			standardCursor.setyHotSpot(32);
			inputManager.setMouseCursor(standardCursor);
		} catch (IOException e) {
			// TODO
			e.printStackTrace();
		}

		initInput();

		initEventBus();
	}

	private void setupNetwork() {
		clientState.addMessageListener(clientMessageListener);
		clientState.addMessageListener(syncState, ShipSyncMessage.class, WeaponFireSyncMessage.class);
	}

	private void initInput() throws IllegalAccessException {
		new BaseInputHandler(inputManager);
	}

	private void initEventBus() {
		eventBus.addHandler(UsernameSelectedEvent.TYPE, new UsernameSelectedHandler() {
			@Override
			public void onUsernameSelected(UsernameSelectedEvent event) {
				try {
					GameConfig.setUsername(event.getUsername());
				} catch (IOException e) {
					// TODO
					e.printStackTrace();
				}

				stateManager.detach(usernameInputState);
				menuState.setEnabled(true);
			}
		});

		eventBus.addHandler(QuitEvent.TYPE, new QuitHandler() {
			@Override
			public void onQuit(QuitEvent e) {
				stopClient();
			}
		});

		eventBus.addHandler(CloseGameEvent.TYPE, new CloseGameHandler() {
			@Override
			public void onCloseGame(CloseGameEvent event) {
				closeGame(false);
			}
		});

		eventBus.addHandler(LoadGameEvent.TYPE, new LoadGameHandler() {
			@Override
			public void onLoadGame(final LoadGameEvent event) {
				enqueueHelper.enqueue(new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						loadGame(event.getData());
						return null;
					}
				});
			}
		});

		eventBus.addHandler(GameStateResponseEvent.TYPE, new GameStateResponseHandler() {
			@Override
			public void onGameStateResponse(GameStateResponseEvent event) {
				processGameState(event.getMessage());
			}
		});

		eventBus.addHandler(ToggleMainMenuEvent.TYPE, new ToggleMainMenuHandler() {
			@Override
			public void onChange(ToggleMainMenuEvent event) {
				if (localPlayer != null) {
					if (menuState.isEnabled()) {
						hudState.setEnabled(hudStateWasEnabled);
						spawnMenuState.setEnabled(!hudStateWasEnabled);
						menuState.setEnabled(false);
						ingameState.setEnabled(true);
					} else {
						hudStateWasEnabled = hudState.isEnabled();

						hudState.setEnabled(false);
						spawnMenuState.setEnabled(false);
						menuState.setEnabled(true);
						ingameState.setEnabled(false);
					}
				}

				updateCursor();
			}
		});

		eventBus.addHandler(ToggleConsoleEvent.TYPE, new ToggleConsoleHandler() {
			@Override
			public void onToggleConsole(ToggleConsoleEvent event) {
				consoleState.setEnabled(!consoleState.isEnabled());
			}
		});

		eventBus.addHandler(ToggleSpawnMenuEvent.TYPE, new ToggleSpawnMenuHandler() {
			@Override
			public void onChange(ToggleSpawnMenuEvent event) {
				if (ingameState.isEnabled() && endState == null) {
					hudState.setEnabled(!hudState.isEnabled());
					spawnMenuState.setEnabled(!spawnMenuState.isEnabled());
				}

				updateCursor();
			}
		});

		eventBus.addHandler(RespawnPlayerEvent.TYPE, new RespawnPlayerHandler() {
			@Override
			public void onRespawnPlayer(RespawnPlayerEvent event) {
				event.getPlayer().setRespawned();
			}
		});

		eventBus.addHandler(WinConditionReachedEvent.TYPE, new WinConditionReachedHandler() {
			@Override
			public void onWinConditionReached(WinConditionReachedEvent event) {
				endState = new EndStateImpl(screen, appRootNode, appGuiNode);
				endState.setEnabled(true);
				stateManager.attach(endState);
				hudState.setEnabled(false);
				spawnMenuState.setEnabled(false);

				updateCursor();
			}
		});

		eventBus.addHandler(ToggleScoreBoardEvent.TYPE, new ToggleScoreBoardHandler() {
			@Override
			public void onToggleScoreBoard(ToggleScoreBoardEvent event) {
				if (endState == null && !consoleState.isEnabled()) {
					scoreBoardState.setEnabled(event.isShow());
				}

				updateCursor();
			}
		});

		eventBus.addHandler(RestartGameEvent.TYPE, new RestartGameHandler() {
			@Override
			public void onRestartGame(RestartGameEvent event) {
				closeGame(true);
			}
		});

		eventBus.addHandler(ClientConfigChangedEvent.TYPE, new ClientConfigChangedHandler() {
			@Override
			public void onChanged(ClientConfigChangedEvent event) {
				onUpdateClientConfig(event);
			}
		});

		eventBus.addHandler(UpdateDebugOptionEvent.TYPE, new UpdateDebugOptionHandler() {
			@Override
			public void onUpdate(UpdateDebugOptionEvent event) {
				if (enableDebug) {
					String[] args = event.getArgs();

					if (event.getDebugCommandKey().equals(DebugConfigConstants.dbg_change_entity_property)) {
						String[] newArgs = new String[args.length + 1];
						newArgs[0] = EntityHelper.getEntityId(localPlayer.getShip()) + "";
						newArgs[1] = args[0];
						newArgs[2] = args[1];

						args = newArgs;
					}

					if (event.getDebugCommandKey().equals(DebugConfigConstants.dbg_toggle_draw_collision_bounds)) {
						bulletAppState.setDebugEnabled(!bulletAppState.isDebugEnabled());
					}

					clientState.sendMessage(new DebugConfigChangeRequestMessage(event.getDebugCommandKey(), args));
				}
			}
		});

		eventBus.addHandler(UpdateCursorEvent.TYPE, new UpdateCursorEventHandler() {
			@Override
			public void onUpdateCursor(UpdateCursorEvent event) {
				updateCursor();
			}
		});

		eventBus.addHandler(SpectateNextPlayerEvent.TYPE, new SpectateNextPlayerHandler() {
			@Override
			public void onChange(SpectateNextPlayerEvent event) {
				respawnLocalPlayerHandler.reset();
			}
		});

		eventBus.addHandler(CreateNewServerEvent.TYPE, new CreateNewServerEventHandler() {
			@Override
			public void onCreateNewServer(CreateNewServerEvent event) {
				try {
					ProcessBuilder builder = new ProcessBuilder("java", "-jar", "sunless.jar", "-server");
					builder.redirectErrorStream(true);
					serverProcess = builder.start();

					Thread thread = new Thread() {
						@Override
						public void run() {
							try (InputStream in = serverProcess.getInputStream(); BufferedReader readerIn = new BufferedReader(new InputStreamReader(in));) {
								String line = null;
								while ((line = readerIn.readLine()) != null) {
									log.info(line);
								}
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					};
					thread.start();

					Thread.sleep(1000L);
					eventBus.fireEvent(new ConnectToServerEvent("localhost", 6543));
				} catch (Exception e) {
					e.printStackTrace();
					// TODO
				}
			}
		});
	}

	private void loadGame(GameInfoInitData data) throws Exception {
		final GameInfo gameInfo = data.getGameInfo();
		gameInfoProvider.setGameInfo(gameInfo);

		enableDebug = gameInfo.isEnableDebug();

		final long localPlayerId = data.getPlayerId();
		final Faction localPlayerFaction = data.getFaction();
		final long initMoney = gameInfo.getInitMoney();

		final Faction[] playerFactions = data.getPlayerFactions();
		final String[] playerNames = data.getPlayerNames();
		final String[] playerShipNames = data.getPlayerShipNames();
		final long[] playerShipIds = data.getPlayerShipIds();
		final long[] playerIds = data.getPlayerIds();
		final long[] playerSpawnLocationEntityIds = data.getPlayerSpawnLocationEntityIds();

		syncState.setTickRate(gameInfo.getServerTickRate());

		loadingState = new LoadingStateImpl(screen, appRootNode, appGuiNode);
		stateManager.attach(loadingState);

		final String mapName = gameInfo.getMapName();
		final MapData mapData = JsonHelper.get().toPOJO(FileConstants.MAPS_PATH_COMPLETE + mapName + FileConstants.MAP_DATA_FILE, MapData.class);

		short teamId = 1;
		Team team1 = new Team(teamId, mapData.getFaction1(), playerIds.length);
		teamId++;
		Team team2 = new Team(teamId, mapData.getFaction2(), playerIds.length);

		teamsContainer.setTeam1(team1);
		teamsContainer.setTeam2(team2);

		LoadingStepBuildRequest request = new LoadingStepBuildRequest();
		request.setMapData(mapData);
		final List<LoadingStep> steps = clientLoadingStepsBuilder.createLoadingSteps(request);

		for (int i = 0; i < playerFactions.length; i++) {
			final int a = i;

			steps.add(new LoadingStep(10f, true) {
				@Override
				public void load() throws Exception {
					final long playerId = playerIds[a];

					if (localPlayerId != playerId) {
						final Faction faction = playerFactions[a];
						final String playerName = playerNames[a];
						final String shipName = playerShipNames[a];
						final long entityId = playerShipIds[a];
						final long spawnPointId = playerSpawnLocationEntityIds[a];

						final Team team = teamsContainer.getTeam(faction);
						final Player player = new Player(playerName, playerId, initMoney, false, team.getTeamId(), teamsContainer);
						player.setSpawnPointEntityId(spawnPointId);
						playersContainer.addPlayer(player);
						team.addPlayer(player);

						if (shipName != null) {
							final Node ship = entityFactory.createShip(shipName);
							ship.setUserData(EntityConstants.id, entityId);
							ship.setUserData(EntityConstants.player, player);

							MoveControl moveControl = new MoveControl(ship.getControl(RigidBodyControl.class));
							moveControl.setEnabled(false);
							ship.addControl(moveControl);

							player.setShip(ship);
						}
					}
				}
			});
		}

		steps.add(new LoadingStep(1f, true) {
			@Override
			public void load() throws Exception {
				final Team team = teamsContainer.getTeam(localPlayerFaction);
				localPlayer = new Player(GameConfig.getString(ClientConfigConstants.c_username), localPlayerId, initMoney, true, team.getTeamId(), teamsContainer);
				playersContainer.addPlayer(localPlayer);
				team.addPlayer(localPlayer);

				spawnMenuState.init(mapData, localPlayer);
				stateManager.attach(spawnMenuState);

				Set<Spatial> activeSpawnPoints = team.getActiveSpawnPoints();

				for (Spatial spawn : activeSpawnPoints) {
					long entityId = EntityHelper.getEntityId(spawn);
					eventBus.fireEvent(new EntityPropertyChangeEvent(spawn, entityId, EntityConstants.faction, team.getFaction().name(), null, null));
				}

				hudState.init(localPlayer);
				stateManager.attach(hudState);

				stateManager.attach(scoreBoardState);
				scoreBoardState.init(team, teamsContainer.getEnemyTeam(team));
				scoreBoardState.setEnabled(false);

				clientState.sendMessage(new GameStateRequestMessage());

				Map<Vector3f, Long> objectIds = gameInfo.getObjectIds();
				List<Spatial> children = ingameState.getRootNode().getChildren();

				for (Spatial child : children) {
					Long id = objectIds.get(child.getLocalTranslation());

					if (id != null) {
						child.setUserData(EntityConstants.id, id);
					}
				}
			}
		});

		loadingThread = new LoadingThread(steps, enqueueHelper, loadingState);

		menuState.setEnabled(false);
		loadingState.setEnabled(true);
		loadingThread.start();
	}

	private void processGameState(GameStateResponseMessage message) {
		final Long[] alivePlayerIds = message.getAlivePlayers();
		PropertyInitEntry[] playerProperties = message.getPlayerProperties();
		PropertyInitEntry[] entityProperties = message.getEntityProperties();

		for (final PropertyInitEntry entry : playerProperties) {
			final Player player = playersContainer.getPlayer(entry.getEntityId());

			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					player.setProperty(entry.getKey(), entry.getValue());
					return null;
				}
			});
		}

		for (long playerId : alivePlayerIds) {
			final Player player = playersContainer.getPlayer(playerId);

			if (player != null) {
				enqueueHelper.enqueue(new Callable<Void>() {
					@Override
					public Void call() throws Exception {
						eventBus.fireEvent(new RespawnPlayerEvent(player));
						return null;
					}
				});
			} else {
				log.warn("Player with id " + playerId + " not found!");
			}
		}

		for (final PropertyInitEntry entry : entityProperties) {
			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					Spatial entity = syncState.getEntity(entry.getEntityId());
					EntityHelper.changeProperty(entity, entry.getKey(), entry.getValue(), null);
					return null;
				}
			});
		}

		enqueueHelper.enqueue(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				Map<String, String[]> debugCommands = message.getDebugCommands();

				for (Entry<String, String[]> entry : debugCommands.entrySet()) {
					eventBus.fireEvent(new DebugConfigChangedEvent(entry.getKey(), entry.getValue()));
				}

				Set<String> toggledCommands = message.getToggledCommands();

				for (String command : toggledCommands) {
					eventBus.fireEvent(new DebugConfigChangedEvent(command, null));
				}

				loadingState.setEnabled(false);
				ingameState.setEnabled(true);
				spawnMenuState.setEnabled(true);

				bulletAppState.setEnabled(true);
				// bulletAppState.setDebugEnabled(true);
				syncState.setEnabled(true);

				return null;
			}
		});
	}

	private void closeGame(boolean isServerRestart) {
		try {
			if (!isServerRestart) {
				clientState.cleanup();
				setupNetwork();
			}

			playersContainer.clear();
			syncState.cleanup();

			teamsContainer.clear();

			bulletAppState.setEnabled(false);
			physicsWrapper.cleanup(ingameState.getRootNode());

			ingameState.cleanup();

			initInput();
			stateManager.detach(hudState);
			stateManager.detach(spawnMenuState);
			stateManager.detach(scoreBoardState);

			ingameState.setEnabled(false);
			loadingState.setEnabled(false);
			syncState.setEnabled(false);

			localPlayer = null;

			if (endState != null) {
				stateManager.detach(endState);
				endState = null;
			}

			DebugState.get().clear();

			menuState.setEnabled(!isServerRestart);
			updateCursor();
		} catch (IllegalAccessException e) {
			// TODO
			e.printStackTrace();
		}
	}

	private void onUpdateClientConfig(ClientConfigChangedEvent event) {
		String key = event.getKey();

		if (key.startsWith("r_")) {
			graphicsUpdater.applyGraphicSettings();

			if (key.equals(ClientConfigConstants.r_resolution)) {
				String resolutionString = GameConfig.getString(ClientConfigConstants.r_resolution);
				String[] resParts = resolutionString.split("x");
				Integer width = Integer.valueOf(resParts[0]);
				Integer height = Integer.valueOf(resParts[1]);

				cam.resize(width, height, true);

				ScreenUtils.reset();

				eventBus.fireEvent(new UpdateGraphicsConfigEvent(key, event.getValue()));
			}
		}
	}

	private void updateCursor() {
		if (hudState.isEnabled()) {
			inputManager.setMouseCursor(hudCursor);
		} else {
			inputManager.setMouseCursor(standardCursor);
		}
	}

	private void stopClient() {
		clientState.disconnectFromServer();

		if (serverProcess != null && serverProcess.isAlive()) {
			serverProcess.destroy();
		}

		applicationStopHelper.stop();
	}

	@Inject
	public void setEnqueueHelper(EnqueueHelper enqueueHelper) {
		this.enqueueHelper = enqueueHelper;
	}

	@Inject
	public void setStateManager(AppStateManager stateManager) {
		this.stateManager = stateManager;
	}

	@Inject
	public void setAssetManager(AssetManager assetManager) {
		this.assetManager = assetManager;
	}

	@Inject
	public void setInputManager(InputManager inputManager) {
		this.inputManager = inputManager;
	}

	@Inject
	public void setAppRootNode(@Named(NodeConstants.ROOT_NODE) Node appRootNode) {
		this.appRootNode = appRootNode;
	}

	@Inject
	public void setAppGuiNode(@Named(NodeConstants.GUI_NODE) Node appGuiNode) {
		this.appGuiNode = appGuiNode;
	}

	@Inject
	public void setScreen(Screen screen) {
		this.screen = screen;
	}

	@Inject
	public void setCam(Camera cam) {
		this.cam = cam;
	}

	@Inject
	public void setViewPort(ViewPort viewPort) {
		this.viewPort = viewPort;
	}

	@Inject
	public void setGameInfoProvider(GameInfoProvider gameInfoProvider) {
		this.gameInfoProvider = gameInfoProvider;
	}

	@Inject
	public void setClientLoadingStepsBuilder(ClientLoadingStepsBuilder clientLoadingStepsBuilder) {
		this.clientLoadingStepsBuilder = clientLoadingStepsBuilder;
	}

	@Inject
	public void setUsernameInputState(UsernameInputState usernameInputState) {
		this.usernameInputState = usernameInputState;
	}

	@Inject
	public void setMenuState(MenuState menuState) {
		this.menuState = menuState;
	}

	@Inject
	public void setConsoleState(ConsoleState consoleState) {
		this.consoleState = consoleState;
	}

	@Inject
	public void setClientState(ClientState clientState) {
		this.clientState = clientState;
	}

	@Inject
	public void setLoadingState(LoadingState loadingState) {
		this.loadingState = loadingState;
	}

	@Inject
	public void setIngameState(ClientIngameState ingameState) {
		this.ingameState = ingameState;
	}

	@Inject
	public void setFpsState(FPSState fpsState) {
		this.fpsState = fpsState;
	}

	@Inject
	public void setSpawnMenuState(SpawnMenuState spawnMenuState) {
		this.spawnMenuState = spawnMenuState;
	}

	@Inject
	public void setHudState(HUDState hudState) {
		this.hudState = hudState;
	}

	@Inject
	public void setEntityFactory(EntityFactory entityFactory) {
		this.entityFactory = entityFactory;
	}

	@Inject
	public void setSyncState(ClientSyncState syncState) {
		this.syncState = syncState;
	}

	@Inject
	public void setClientMessageListener(ClientMessageListener clientMessageListener) {
		this.clientMessageListener = clientMessageListener;
	}

	@Inject
	public void setPlayersContainer(ClientPlayersContainer playersContainer) {
		this.playersContainer = playersContainer;
	}

	@Inject
	public void setInputHandler(InputHandler inputHandler) {
		this.inputHandler = inputHandler;
	}

	@Inject
	public void setScoreBoardState(ScoreBoardState scoreBoardState) {
		this.scoreBoardState = scoreBoardState;
	}

	@Inject
	public void setEndState(EndState endState) {
		this.endState = endState;
	}

	@Inject
	public void setTeamsContainer(TeamsContainer teamsContainer) {
		this.teamsContainer = teamsContainer;
	}

	@Inject
	public void setPhysicsWrapper(PhysicsWrapper physicsWrapper) {
		this.physicsWrapper = physicsWrapper;
	}

	@Inject
	public void setApplicationStopHelper(ApplicationStopHelper applicationStopHelper) {
		this.applicationStopHelper = applicationStopHelper;
	}

	@Inject
	public void setGraphicsUpdater(ClientGraphicsUpdater graphicsUpdater) {
		this.graphicsUpdater = graphicsUpdater;
	}
}