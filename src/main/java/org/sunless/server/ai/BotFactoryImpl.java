package org.sunless.server.ai;

import java.util.List;
import java.util.Random;

import org.sunless.server.gameplay.ServerPlayerFactory;
import org.sunless.server.gameplay.control.BoostControl;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityFactory;
import org.sunless.shared.gameplay.GameInfoProvider;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.json.ShipConfig;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.scene.Node;
import com.jme3.scene.control.AbstractControl;

@Singleton
public class BotFactoryImpl implements BotFactory {

	private final Random random = new Random();

	private GameInfoProvider gameInfoProvider;
	private ServerPlayerFactory playerFactory;
	private TeamsContainer teamsContainer;
	private EntityFactory entityFactory;

	@Override
	public Player createBot(String name, Team team) throws Exception {
		Team enemyTeam = teamsContainer.getEnemyTeam(team);
		Player botPlayer = playerFactory.createPlayer(name, team, false, enemyTeam);
		botPlayer.setBot(true);

		List<ShipConfig> ships = team.getFactionConfig().getShips();
		ShipConfig config = ships.get(random.nextInt(ships.size()));

		Node ship = entityFactory.createShip(config.getShipName());

		ship.setUserData(EntityConstants.player, botPlayer);

		botPlayer.setShip(ship);
		botPlayer.setWantsRespawn(true);

		AbstractControl aiFlightControl = null;

		switch (gameInfoProvider.getGameInfo().getAiLevel()) {
			case EASY:
				aiFlightControl = new AIEasyFlightControl(botPlayer);
				break;
			case MEDIUM:
				aiFlightControl = new AIMediumFlightControl(botPlayer);
				break;
			case HARD:
				aiFlightControl = new AIHardFlightControl(botPlayer);
				break;
		}

		ship.addControl(aiFlightControl);
		botPlayer.addPlayerControl(aiFlightControl);

		AIMoveControl aiMoveControl = new AIMoveControl(ship.getControl(RigidBodyControl.class));
		ship.addControl(aiMoveControl);
		botPlayer.addPlayerControl(aiMoveControl);

		BoostControl boostControl = new BoostControl();
		ship.addControl(boostControl);
		botPlayer.addPlayerControl(boostControl);

		return botPlayer;
	}

	@Inject
	public void setGameInfoProvider(GameInfoProvider gameInfoProvider) {
		this.gameInfoProvider = gameInfoProvider;
	}

	@Inject
	public void setPlayerFactory(ServerPlayerFactory playerFactory) {
		this.playerFactory = playerFactory;
	}

	@Inject
	public void setTeamsContainer(TeamsContainer teamsContainer) {
		this.teamsContainer = teamsContainer;
	}

	@Inject
	public void setEntityFactory(EntityFactory entityFactory) {
		this.entityFactory = entityFactory;
	}
}