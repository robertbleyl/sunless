package org.sunless.server.ai;

import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.Team;

public interface BotFactory {
	
	Player createBot(String name, Team team) throws Exception;
}