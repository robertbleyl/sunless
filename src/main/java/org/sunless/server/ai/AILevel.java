package org.sunless.server.ai;

public enum AILevel {

	EASY,

	MEDIUM,

	HARD
}