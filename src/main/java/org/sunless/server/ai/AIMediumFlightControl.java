package org.sunless.server.ai;

import java.util.Optional;
import java.util.stream.Stream;

import org.apache.commons.lang3.BooleanUtils;
import org.sunless.shared.control.RotationConstants;
import org.sunless.shared.entity.CollisionGroups;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.util.TargetLeadCalculator;

import com.jme3.bounding.BoundingSphere;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.collision.CollisionResults;
import com.jme3.math.FastMath;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.scene.BatchNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class AIMediumFlightControl extends AIEasyFlightControl {

	protected float evadeTimer;
	protected float attackTimer;

	protected float objectiveTimer;
	protected boolean isInteractingWithObjective;

	protected AIEvadeContactPointComparator resultComparator;

	public AIMediumFlightControl(Player botPlayer) {
		super(botPlayer);

		resultComparator = new AIEvadeContactPointComparator(ship);
	}

	@Override
	protected boolean focusesOnObjective(float tpf) {
		float objectiveDist = objective.getLocalTranslation().distance(bodyControl.getPhysicsLocation());
		boolean isNearObjective = objectiveDist <= getMinObjectiveDist();

		if (isReturningToObjective) {
			if (isNearObjective) {
				isReturningToObjective = false;
				objectiveTimer = 0f;
				return true;
			}

			returnToObjective(tpf);
			return true;
		}

		if (isNearObjective) {
			objectiveTimer += tpf;

			if (objectiveTimer >= getMaxObjectiveTime()) {
				return false;
			}

			interactWithObjective(tpf);
		} else if (objectiveDist > getMaxObjectiveDist()) {
			returnToObjective(tpf);
			return true;
		}

		return false;
	}

	protected void interactWithObjective(float tpf) {
		Vector3f targetDir = objective.getLocalTranslation().subtract(spatial.getLocalTranslation());
		int index = random.nextInt(3);
		targetDir.set(index, targetDir.get(index));
		targetDir.normalizeLocal();
		steerToLocation(targetDir, tpf);

		Boolean isMotherShip = objective.getUserData(EntityConstants.isMotherShip);

		if (BooleanUtils.isTrue(isMotherShip)) {
			Vector3f viewDir = bodyControl.getPhysicsRotation().getRotationColumn(RotationConstants.FORWARD).normalize();

			float angle = FastMath.abs(viewDir.angleBetween(targetDir));

			if (angle <= FastMath.PI / 2f) {
				shoot(objective);
			}
		}
	}

	@Override
	protected void evade(float tpf, Vector3f targetDirection) {
		super.evade(tpf, targetDirection);
		evadeTimer += tpf;
	}

	@Override
	protected void checkAttacking(float tpf, Vector3f targetLocation, float targetDist) {
		if (targetDist >= getMinChaseDist() && attackTimer <= getMaxAttackTime()) {
			attackTargetShip(tpf, targetLocation);
		} else {
			Vector3f evadeLocation = bodyControl.getPhysicsLocation().add(bodyControl.getPhysicsLocation().subtract(targetLocation));
			evade(tpf, evadeLocation);
		}
	}

	@Override
	protected void checkEvading(float tpf, Vector3f targetDirection, float targetDist) {
		if (targetDist <= getMaxEvadeDist() && evadeTimer < getMaxEvadeTime()) {
			evade(tpf, targetDirection);
		} else {
			attackTargetShip(tpf, targetDirection);
		}
	}

	@Override
	protected void attackTargetShip(float tpf, Vector3f targetDirection) {
		super.attackTargetShip(tpf, targetDirection);
		evadeTimer = 0f;
		attackTimer += tpf;
	}

	@Override
	protected Vector3f getShootDirection(Spatial target, String weaponFireSpeedPropertyKey, String containerKey) {
		float weaponFireSpeed = spatial.getUserData(weaponFireSpeedPropertyKey);
		Vector3f targetLeadPosition = TargetLeadCalculator.getTargetLeadPosition(bodyControl.getPhysicsLocation(), target, weaponFireSpeed);

		return targetLeadPosition.subtract(bodyControl.getPhysicsLocation()).normalizeLocal();
	}

	@Override
	protected void steerToLocation(Vector3f targetLocation, float tpf) {
		Node rootNode = spatial.getParent();
		Vector3f location = spatial.getLocalTranslation();

		float threshold = 30f;
		Vector3f forward = spatial.getLocalRotation().getRotationColumn(RotationConstants.FORWARD).normalize();
		final Ray ray = new Ray(location, forward);

		Stream<Spatial> stream = rootNode.getChildren().parallelStream();
		Optional<Vector3f> resultOptional = checkChildrenForEvade(location, threshold, ray, stream);

		if (resultOptional.isPresent()) {
			Vector3f result = resultOptional.get();

			Vector3f direction = location.subtract(result);

			float angle = direction.normalize().angleBetween(forward);
			float l = direction.length() / FastMath.cos(angle);

			Vector3f forwardPoint = spatial.getLocalTranslation().add(forward.mult(l));
			Vector3f orthogonal = forwardPoint.subtract(result);
			Vector3f evadePoint = forwardPoint.add(orthogonal);

			super.steerToLocation(evadePoint, tpf);
		} else {
			super.steerToLocation(targetLocation, tpf);
		}
	}

	private Optional<Vector3f> checkChildrenForEvade(Vector3f location, float threshold, Ray ray, Stream<Spatial> stream) {
		Optional<Vector3f> resultOptional = stream
				.map(c -> {
					if (c instanceof BatchNode) {
						BatchNode bn = (BatchNode)c;

						if (bn.getWorldBound().collideWith(ray) > 0) {
							Optional<Vector3f> result = checkChildrenForEvade(location, threshold, ray, bn.getChildren().stream());

							if (result.isPresent()) {
								return result.get();
							}
						}
					} else {
						RigidBodyControl control = c.getControl(RigidBodyControl.class);

						if (control != null) {
							int collisionGroup = control.getCollisionGroup();

							if (collisionGroup == CollisionGroups.LARGE_OBJECTS) {
								BoundingSphere sphere = (BoundingSphere)c.getWorldBound();
								float distance = c.getLocalTranslation().distance(location) - sphere.getRadius();

								if (distance <= threshold) {
									CollisionResults results = new CollisionResults();
									c.collideWith(ray, results);

									if (results.size() > 0) {
										return results.getCollision(0).getGeometry().getWorldTranslation();
									}
								}
							}
						}
					}

					return null;
				})
				.filter(c -> c != null)
				.min(resultComparator);
		return resultOptional;
	}

	protected float getMaxEvadeTime() {
		return 2f;
	}

	protected float getMaxAttackTime() {
		return 60f;
	}

	protected float getMaxObjectiveTime() {
		return 10f;
	}

	@Override
	protected float getMinChaseDist() {
		return 20f;
	}

	@Override
	protected float getMaxEvadeDist() {
		return 70f;
	}

	@Override
	protected float getMaxObjectiveDist() {
		return 900f;
	}

	@Override
	protected float getMinObjectiveDist() {
		return 90f;
	}

	@Override
	protected float getMaxShootDist() {
		return 120f;
	}
}