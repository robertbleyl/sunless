package org.sunless.server.ai;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.json.MapData;
import org.sunless.shared.network.sync.AbstractSyncState;

import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

public class AIEasyThread extends Thread {

	protected final Random random = new Random();

	protected final List<Player> players;
	protected final Team enemyTeam;
	protected final GameInfo gameInfo;
	protected final MapData mapData;
	protected final AbstractSyncState syncState;

	protected final Map<Player, Float> checkTimes;

	protected boolean isAlive;

	public AIEasyThread(List<Player> players, Team enemyTeam, GameInfo gameInfo, MapData mapData, AbstractSyncState syncState) {
		super();
		this.players = players;
		this.enemyTeam = enemyTeam;
		this.gameInfo = gameInfo;
		this.mapData = mapData;
		this.syncState = syncState;

		checkTimes = new HashMap<>(players.size());

		for (Player player : players) {
			checkTimes.put(player, 0f);
		}

		isAlive = true;
	}

	@Override
	public void run() {
		long timer = 0l;

		boolean firstRun = true;

		while (isAlive) {
			for (int i = 0; i < players.size(); i++) {
				Player player = players.get(i);
				Node ship = player.getShip();

				if (ship != null) {
					AIEasyFlightControl flightControl = ship.getControl(AIEasyFlightControl.class);

					if (flightControl != null) {
						Float lastCheckTime = checkTimes.get(player);

						if (lastCheckTime != null && (lastCheckTime == 0f || timer - lastCheckTime > getCheckTime())) {
							Spatial newTarget = getNewTarget(player);

							if (newTarget != null) {
								flightControl.setTargetShip(newTarget);
								player.setTarget(newTarget.getUserData(EntityConstants.player));
							}

							Spatial newObjective = getNewObjective(player);

							if (newObjective != null) {
								flightControl.setObjective(newObjective);
							}
						}

						if (!firstRun) {
							try {
								sleep(getSleepTime());
							} catch (InterruptedException e) {
								e.printStackTrace();
							}

							timer += getSleepTime();
						}
					}
				}

				firstRun = false;
			}
		}
	}

	protected Spatial getNewTarget(Player player) {
		Vector3f localTranslation = player.getShip().getLocalTranslation();

		List<Player> enemyPlayers = enemyTeam.getPlayers();

		Spatial nearestShip = null;
		float minDist = Float.MAX_VALUE;

		for (Player enemyPlayer : enemyPlayers) {
			Node ship = enemyPlayer.getShip();

			if (ship != null) {
				float dist = ship.getLocalTranslation().distance(localTranslation);

				if (dist < minDist) {
					minDist = dist;
					nearestShip = ship;
				}
			}
		}

		return nearestShip;
	}

	protected Spatial getNewObjective(Player player) {
		List<Vector3f> objectiveLocations = mapData.retrieveObjectives(gameInfo.getGameMode());

		int i = random.nextInt(objectiveLocations.size());
		Vector3f entityLocation = objectiveLocations.get(i);
		Long entityId = gameInfo.getObjectIds().get(entityLocation);

		return syncState.getEntity(entityId);
	}

	protected long getCheckTime() {
		return 5000;
	}

	protected long getSleepTime() {
		return 1000;
	}

	public void stopThread() {
		isAlive = false;
	}
}