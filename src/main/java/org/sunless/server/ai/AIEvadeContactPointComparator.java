package org.sunless.server.ai;

import java.util.Comparator;

import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

public class AIEvadeContactPointComparator implements Comparator<Vector3f> {

	private final Spatial ship;

	public AIEvadeContactPointComparator(Spatial ship) {
		this.ship = ship;
	}

	@Override
	public int compare(Vector3f o1, Vector3f o2) {
		float d1 = o1.distance(ship.getLocalTranslation());
		float d2 = o2.distance(ship.getLocalTranslation());
		return Float.compare(d1, d2);
	}
}