package org.sunless.server.ai;

import java.util.List;

import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.json.MapData;
import org.sunless.shared.network.sync.AbstractSyncState;

public class AIHardThread extends AIMediumThread {

	public AIHardThread(List<Player> players, Team enemyTeam, GameInfo gameInfo, MapData mapData, AbstractSyncState syncState) {
		super(players, enemyTeam, gameInfo, mapData, syncState);
	}
}