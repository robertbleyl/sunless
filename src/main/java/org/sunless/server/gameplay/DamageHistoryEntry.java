package org.sunless.server.gameplay;

import org.sunless.shared.gameplay.EntityPropertyChangeCause;

public class DamageHistoryEntry {

	private final EntityPropertyChangeCause cause;
	private int damage;

	public DamageHistoryEntry(EntityPropertyChangeCause cause, int damage) {
		this.cause = cause;
		this.damage = damage;
	}

	public EntityPropertyChangeCause getCause() {
		return cause;
	}

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}
}