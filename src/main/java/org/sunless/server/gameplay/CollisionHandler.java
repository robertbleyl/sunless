package org.sunless.server.gameplay;

import java.util.concurrent.Callable;

import org.sunless.server.event.gameplay.WeaponFireCollisionPredictedEvent;
import org.sunless.server.event.gameplay.WeaponFireCollisionPredictedHandler;
import org.sunless.server.network.ServerState;
import org.sunless.server.network.ServerSyncState;
import org.sunless.shared.config.DebugConfigConstants;
import org.sunless.shared.entity.EntityAttacher;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.config.DebugConfigChangedEvent;
import org.sunless.shared.event.config.DebugConfigChangedHandler;
import org.sunless.shared.gameplay.CollisionCause;
import org.sunless.shared.gameplay.EntityPropertyChangeCause;
import org.sunless.shared.gameplay.WeaponFireCause;
import org.sunless.shared.gameplay.weapon.WeaponFireControl;
import org.sunless.shared.loading.EnqueueHelper;
import org.sunless.shared.network.messages.toclient.gameplay.RemoveWeaponFireMessage;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.bullet.collision.PhysicsCollisionEvent;
import com.jme3.bullet.collision.PhysicsCollisionListener;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;

@Singleton
public class CollisionHandler implements PhysicsCollisionListener {

	protected final EventBus eventBus = EventBus.get();

	protected final EntityAttacher entityAttacher;
	protected final EnqueueHelper enqueueHelper;
	protected final ServerState serverState;
	protected final ServerSyncState syncState;

	protected boolean enableDamageCollision;
	protected boolean enableDamageWeapons;

	@Inject
	public CollisionHandler(EntityAttacher entityAttacher, EnqueueHelper enqueueHelper, ServerState serverState, ServerSyncState syncState) {
		this.entityAttacher = entityAttacher;
		this.enqueueHelper = enqueueHelper;
		this.serverState = serverState;
		this.syncState = syncState;

		enableDamageCollision = true;
		enableDamageWeapons = true;

		eventBus.addHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				String debugCommandKey = event.getDebugCommandKey();

				if (debugCommandKey.equals(DebugConfigConstants.dbg_toggle_damage_collision)) {
					enableDamageCollision = !enableDamageCollision;
				} else if (debugCommandKey.equals(DebugConfigConstants.dbg_toggle_damage_weapons)) {
					enableDamageWeapons = !enableDamageWeapons;
				}
			}
		});

		eventBus.addHandler(WeaponFireCollisionPredictedEvent.TYPE, new WeaponFireCollisionPredictedHandler() {
			@Override
			public void onCollisionPredicted(WeaponFireCollisionPredictedEvent event) {
				handleCollision(event.getA(), event.getB(), 0.01f);
			}
		});
	}

	@Override
	public void collision(PhysicsCollisionEvent event) {
		Spatial entityA = event.getNodeA();
		Spatial entityB = event.getNodeB();

		handleCollision(entityA, entityB, event.getAppliedImpulse());
	}

	protected void handleCollision(Spatial entityA, Spatial entityB, float impulseAmount) {
		Long entityIdA = entityA.getUserData(EntityConstants.id);
		Long entityIdB = entityB.getUserData(EntityConstants.id);

		if (entityIdA != null && isWeaponFire(entityA)) {
			if (entityIdB == null || !isOwnWeaponFire(entityA, entityIdB)) {
				handleWeaponFire(entityIdA, entityA, entityB, impulseAmount);
			}
		} else if (entityIdB != null && isWeaponFire(entityB)) {
			if (entityIdA == null || !isOwnWeaponFire(entityB, entityIdA)) {
				handleWeaponFire(entityIdB, entityB, entityA, impulseAmount);
			}
		} else {
			handleCollisionDamage(entityA, entityB);
			handleCollisionDamage(entityB, entityA);
		}
	}

	protected void handleCollisionDamage(Spatial entity, Spatial other) {
		if (enableDamageCollision) {
			Integer hitPoints = entity.getUserData(EntityConstants.hitPoints);

			if (hitPoints != null) {
				EntityPropertyChangeCause cause = new CollisionCause(other);

				queueReceiveDamage(entity, 1, cause);
			}
		}
	}

	protected boolean isWeaponFire(Spatial entity) {
		WeaponFireControl control = entity.getControl(WeaponFireControl.class);
		return control != null;
	}

	protected boolean isOwnWeaponFire(Spatial weaponFire, long otherEntityId) {
		Spatial ship = weaponFire.getUserData(EntityConstants.weaponFireFiringShip);
		return otherEntityId == EntityHelper.getEntityId(ship);
	}

	protected void handleWeaponFire(final long weaponFireId, Spatial weaponFire, Spatial otherEntity, float impulseAmount) {
		Vector3f weaponFirePosition = weaponFire.getLocalTranslation();

		WeaponFireControl control = weaponFire.getControl(WeaponFireControl.class);
		weaponFire.removeControl(control);

		final Vector3f pos = weaponFirePosition;

		queueRemoveWeaponFire(weaponFireId, weaponFire, pos);

		if (enableDamageWeapons && otherEntity.getUserData(EntityConstants.maxHitPoints) != null) {
			int damage = weaponFire.getUserData(EntityConstants.weaponFireDamage);

			EntityPropertyChangeCause cause = new WeaponFireCause(weaponFire);

			queueReceiveDamage(otherEntity, damage, cause);
		}
	}

	protected void queueRemoveWeaponFire(final long weaponFireId, final Spatial weaponFire, final Vector3f pos) {
		enqueueHelper.enqueue(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				removeWeaponFire(weaponFireId, weaponFire, pos);
				return null;
			}
		});
	}

	protected void removeWeaponFire(long weaponFireId, Spatial weaponFire, Vector3f pos) {
		entityAttacher.removeEntity(weaponFire);
		serverState.broadcast(new RemoveWeaponFireMessage(true, weaponFireId, pos));
		syncState.removeEntity(weaponFireId);
	}

	protected void queueReceiveDamage(final Spatial entity, final int originalDamage, final EntityPropertyChangeCause cause) {
		enqueueHelper.enqueue(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				receiveDamage(entity, originalDamage, cause);
				return null;
			}
		});
	}

	protected void receiveDamage(Spatial entity, int originalDamage, EntityPropertyChangeCause cause) {
		DamageHistory damageHistory = entity.getUserData(EntityConstants.damageHistory);

		damageHistory.add(new DamageHistoryEntry(cause, originalDamage));

		int currentShields = entity.getUserData(EntityConstants.shields);

		int damage = originalDamage;

		if (currentShields > 0) {
			currentShields -= damage;

			if (currentShields < 0) {
				damage = -currentShields;
				currentShields = 0;
			} else {
				damage = 0;
			}

			EntityHelper.changeProperty(entity, EntityConstants.shields, currentShields, cause);
		}

		if (damage > 0) {
			int currentHitPoints = entity.getUserData(EntityConstants.hitPoints);

			currentHitPoints -= damage;

			if (currentHitPoints < 0) {
				currentHitPoints = 0;
			}

			EntityHelper.changeProperty(entity, EntityConstants.hitPoints, currentHitPoints, cause);
		}
	}
}