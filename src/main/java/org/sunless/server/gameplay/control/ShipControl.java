package org.sunless.server.gameplay.control;

import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityHelper;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.control.AbstractControl;

public class ShipControl extends AbstractControl {

	private float currentRegenStep;

	@Override
	protected void controlUpdate(float tpf) {
		int currentShields = spatial.getUserData(EntityConstants.shields);
		int maxShields = spatial.getUserData(EntityConstants.maxShields);

		if (currentShields < maxShields) {
			float regenRate = spatial.getUserData(EntityConstants.shieldRegeneration);

			currentRegenStep += regenRate * tpf;

			if (currentRegenStep >= 1f) {
				currentRegenStep -= 1f;

				int newShields = currentShields + 1;

				EntityHelper.changeProperty(spatial, EntityConstants.shields, newShields, null);
			}
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}