package org.sunless.server.gameplay.control;

import java.util.Optional;
import java.util.stream.Stream;

import org.sunless.server.event.gameplay.WeaponFireCollisionPredictedEvent;
import org.sunless.shared.entity.CollisionGroups;
import org.sunless.shared.event.EventBus;

import com.jme3.bounding.BoundingSphere;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.collision.CollisionResults;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;
import com.jme3.scene.BatchNode;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.control.AbstractControl;

public class WeaponFireCCDControl extends AbstractControl {

	private final EventBus eventBus = EventBus.get();

	private final int physicsTickRate;

	private WeaponFireCCDResultComparator resultComparator;

	public WeaponFireCCDControl(int physicsTickRate) {
		this.physicsTickRate = physicsTickRate;
	}

	@Override
	protected void controlUpdate(float tpf) {
		Node rootNode = spatial.getParent();
		RigidBodyControl bodyControl = spatial.getControl(RigidBodyControl.class);

		Vector3f linearVelocity = bodyControl.getLinearVelocity();
		float speed = linearVelocity.length();
		final Vector3f direction = linearVelocity.normalize();

		final Vector3f weaponFireLocation = spatial.getLocalTranslation();
		final Ray ray = new Ray(weaponFireLocation, direction);
		final float threshold = 3f * (speed / physicsTickRate);

		Stream<Spatial> stream = rootNode.getChildren().parallelStream();
		Optional<WeaponFireCCDCollisionResult> resultOptional = checkChildren(weaponFireLocation, ray, threshold, stream);

		if (resultOptional.isPresent()) {
			setEnabled(false);
			WeaponFireCCDCollisionResult result = resultOptional.get();

			Vector3f contactPoint = result.getContactPoint();

			spatial.setLocalTranslation(contactPoint);
			bodyControl.setPhysicsLocation(contactPoint);
			bodyControl.setLinearVelocity(Vector3f.ZERO);

			eventBus.fireEvent(new WeaponFireCollisionPredictedEvent(spatial, result.getObject()));
		}
	}

	private Optional<WeaponFireCCDCollisionResult> checkChildren(final Vector3f weaponFireLocation, final Ray ray, final float threshold, Stream<Spatial> stream) {
		Optional<WeaponFireCCDCollisionResult> resultOptional = stream
				.map(c -> {
					if (c instanceof BatchNode) {
						BatchNode bn = (BatchNode)c;

						if (bn.getWorldBound().collideWith(ray) > 0) {
							Optional<WeaponFireCCDCollisionResult> result = checkChildren(weaponFireLocation, ray, threshold, bn.getChildren().stream());

							if (result.isPresent()) {
								return result.get();
							}
						}
					} else {
						RigidBodyControl control = c.getControl(RigidBodyControl.class);

						if (control != null) {
							int collisionGroup = control.getCollisionGroup();

							if (collisionGroup == CollisionGroups.LARGE_OBJECTS || collisionGroup == CollisionGroups.SMALL_SHIPS) {
								BoundingSphere sphere = (BoundingSphere)c.getWorldBound();
								float distance = c.getLocalTranslation().distance(weaponFireLocation) - sphere.getRadius();

								if (distance <= threshold) {
									CollisionResults results = new CollisionResults();

									if (collisionGroup == CollisionGroups.LARGE_OBJECTS) {
										c.collideWith(ray, results);
									} else if (collisionGroup == CollisionGroups.SMALL_SHIPS) {
										c.getWorldBound().collideWith(ray, results);
									}

									if (results.size() > 0) {
										Vector3f contactPoint = results.getCollision(0).getContactPoint();

										if (contactPoint.distance(weaponFireLocation) <= threshold) {
											return new WeaponFireCCDCollisionResult(c, contactPoint);
										}
									}
								}
							}
						}
					}

					return null;
				})
				.filter(c -> c != null)
				.min(resultComparator);

		return resultOptional;
	}

	@Override
	public void setSpatial(Spatial spatial) {
		super.setSpatial(spatial);
		resultComparator = new WeaponFireCCDResultComparator(spatial);
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}