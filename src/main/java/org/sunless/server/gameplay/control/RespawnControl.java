package org.sunless.server.gameplay.control;

import org.sunless.shared.control.DelayedControl;
import org.sunless.shared.event.gameplay.RespawnPlayerEvent;
import org.sunless.shared.gameplay.player.Player;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;

public class RespawnControl extends DelayedControl {

	private final Player player;

	public RespawnControl(Player player, float respawnDelay) {
		super(respawnDelay);
		this.player = player;
	}

	@Override
	protected void onUpdate(float elapsedTime) {
		if (player.getShip() != null && !player.isAlive() && player.isWantsRespawn()) {
			eventBus.fireEvent(new RespawnPlayerEvent(player));
		}
	}

	@Override
	protected void controlRender(RenderManager rm, ViewPort vp) {

	}
}