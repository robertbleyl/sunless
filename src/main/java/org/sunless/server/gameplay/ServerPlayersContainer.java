package org.sunless.server.gameplay;

import java.util.HashMap;
import java.util.Map;

import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.json.ServerConfig;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ServerPlayersContainer {

	private final Map<Integer, Player> players;
	private final Map<Long, Integer> connections;

	@Inject
	public ServerPlayersContainer(ServerConfig config) {
		players = new HashMap<>(config.getMaxPlayers());
		connections = new HashMap<>(config.getMaxPlayers());
	}

	public void addPlayer(int connectionId, Player player) {
		players.put(connectionId, player);
		connections.put(player.getPlayerId(), connectionId);
	}

	public Player getPlayer(int connectionId) {
		return players.get(connectionId);
	}

	public Integer getConnectionId(long playerId) {
		return connections.get(playerId);
	}

	public Player removePlayer(int connectionId) {
		Player player = players.remove(connectionId);
		connections.remove(player.getPlayerId());
		return player;
	}

	public int getPlayerCount() {
		return players.size();
	}
}