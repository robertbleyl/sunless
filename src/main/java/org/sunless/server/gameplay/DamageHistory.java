package org.sunless.server.gameplay;

import java.io.IOException;
import java.util.LinkedList;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.export.Savable;

public class DamageHistory extends LinkedList<DamageHistoryEntry> implements Savable {
	
	private static final long serialVersionUID = 4261116991350734882L;
	
	@Override
	public void write(JmeExporter ex) throws IOException {
		
	}
	
	@Override
	public void read(JmeImporter im) throws IOException {
		
	}
}