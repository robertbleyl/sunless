package org.sunless.server.gameplay;

import org.sunless.shared.gameplay.player.PlayerIdProvider;

import com.google.inject.Singleton;

@Singleton
public class ServerPlayerIdProvider implements PlayerIdProvider {

	private int lastId = 0;

	@Override
	public int generatePlayerId() {
		lastId++;
		return lastId;
	}
}