package org.sunless.server.gameplay.weapon;

import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.gameplay.weapon.SimpleSpecialWeaponFirePhysics;
import org.sunless.shared.gameplay.weapon.WeaponFireFactory;

import com.google.inject.Inject;
import com.jme3.asset.AssetManager;

public class ServerStandardWeaponFireFactory extends WeaponFireFactory {

	@Inject
	public ServerStandardWeaponFireFactory(AssetManager assetManager) {
		super(EntityConstants.standard_weaponFirePrefix, assetManager, new SimpleSpecialWeaponFirePhysics(EntityConstants.standard_weaponFirePrefix), EntityConstants.standard_weaponFirePrefix);
	}
}