package org.sunless.server.gameplay.weapon;

import org.sunless.server.network.ServerState;
import org.sunless.shared.entity.EntityAttacher;
import org.sunless.shared.gameplay.weapon.WeaponFireControl;
import org.sunless.shared.network.messages.toclient.gameplay.RemoveWeaponFireMessage;

import com.jme3.math.Vector3f;

public class ServerWeaponFireControl extends WeaponFireControl {

	private final float lifeTime;
	private final EntityAttacher entityAttacher;
	private final ServerState serverState;
	private final long weaponFireId;

	private float timer;

	public ServerWeaponFireControl(float lifeTime, float speed, Vector3f direction, long weaponFireId, ServerState serverState, EntityAttacher entityAttacher) {
		super(speed, direction);
		this.weaponFireId = weaponFireId;
		this.entityAttacher = entityAttacher;
		this.serverState = serverState;
		this.lifeTime = lifeTime;
	}

	@Override
	protected void controlUpdate(float tpf) {
		super.controlUpdate(tpf);

		timer += tpf;

		if (timer > lifeTime) {
			Vector3f localTranslation = spatial.getLocalTranslation();
			entityAttacher.removeEntity(spatial);
			spatial.removeControl(this);

			serverState.broadcast(new RemoveWeaponFireMessage(false, weaponFireId, localTranslation));
		}
	}
}