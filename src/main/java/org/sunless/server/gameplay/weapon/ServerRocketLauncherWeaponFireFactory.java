package org.sunless.server.gameplay.weapon;

import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.gameplay.weapon.SimpleSpecialWeaponFirePhysics;
import org.sunless.shared.gameplay.weapon.WeaponFireFactory;

import com.google.inject.Inject;
import com.jme3.asset.AssetManager;

public class ServerRocketLauncherWeaponFireFactory extends WeaponFireFactory {

	@Inject
	public ServerRocketLauncherWeaponFireFactory(AssetManager assetManager) {
		super(EntityConstants.special_weaponFirePrefix, assetManager, new SimpleSpecialWeaponFirePhysics(EntityConstants.special_weaponFirePrefix), EntityConstants.special_weaponFireType_rocketLauncher);
	}
}