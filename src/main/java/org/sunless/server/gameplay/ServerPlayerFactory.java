package org.sunless.server.gameplay;

import org.sunless.server.gameplay.control.RespawnControl;
import org.sunless.shared.NodeConstants;
import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.GameInfoProvider;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.PlayerIdProvider;
import org.sunless.shared.gameplay.player.Team;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

@Singleton
public class ServerPlayerFactory {

	private final PlayerIdProvider idProvider;
	private final Node sceneNode;
	private final GameInfoProvider gameInfoProvider;
	private final TeamsContainer teamsContainer;

	@Inject
	public ServerPlayerFactory(PlayerIdProvider idProvider, @Named(NodeConstants.ROOT_NODE) Node rootNode, GameInfoProvider gameInfoProvider, TeamsContainer teamsContainer) {
		this.idProvider = idProvider;
		this.sceneNode = rootNode;
		this.gameInfoProvider = gameInfoProvider;
		this.teamsContainer = teamsContainer;
	}

	public Player createPlayer(String name, Team team, boolean isLocalPlayer, Team enemyTeam) {
		GameInfo gameInfo = gameInfoProvider.getGameInfo();

		long playerId = idProvider.generatePlayerId();
		Player player = new Player(name, playerId, gameInfo.getInitMoney(), isLocalPlayer, team.getTeamId(), teamsContainer);
		Spatial firstSpawnPoint = team.getActiveSpawnPoints().iterator().next();
		player.setSpawnPointEntityId(EntityHelper.getEntityId(firstSpawnPoint));
		team.addPlayer(player);

		RespawnControl control = new RespawnControl(player, gameInfo.getSpawnDelay());
		player.addPlayerControl(control);
		sceneNode.addControl(control);

		return player;
	}
}