package org.sunless.server.gameplay;

import org.sunless.shared.control.DelayedControl;
import org.sunless.shared.event.main.EndGameTimeUpdateEvent;

import com.jme3.renderer.RenderManager;
import com.jme3.renderer.ViewPort;

public class EndTimerControl extends DelayedControl {
	
	private int secondsLeft;
	
	public EndTimerControl(int secondsLeft) {
		super(1f);
		this.secondsLeft = secondsLeft;
	}
	
	@Override
	protected void onUpdate(float elapsedTime) {
		secondsLeft--;
		eventBus.fireEvent(new EndGameTimeUpdateEvent(secondsLeft));
		
		if (secondsLeft <= 0) {
			setEnabled(false);
			spatial.removeControl(this);
		}
	}
	
	@Override
	protected void controlRender(RenderManager arg0, ViewPort arg1) {
	
	}
}