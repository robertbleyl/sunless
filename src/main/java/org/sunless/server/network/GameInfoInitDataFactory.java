package org.sunless.server.network;

import java.util.List;

import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.gameplay.GameInfoInitData;
import org.sunless.shared.gameplay.GameInfoProvider;
import org.sunless.shared.gameplay.TeamsContainer;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.json.Faction;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.scene.Node;

@Singleton
public class GameInfoInitDataFactory {

	private final TeamsContainer teamsContainer;
	private final GameInfoProvider gameInfoProvider;

	@Inject
	public GameInfoInitDataFactory(TeamsContainer teamsContainer, GameInfoProvider gameInfoProvider) {
		this.teamsContainer = teamsContainer;
		this.gameInfoProvider = gameInfoProvider;
	}

	public GameInfoInitData createGameInfoInitData(Player player) {
		Team team1 = teamsContainer.getTeam1();
		Team team2 = teamsContainer.getTeam2();

		int team1PlayerCount = team1.getPlayerCount();
		int team2PlayerCount = team2.getPlayerCount();

		int playerCount = team1PlayerCount;
		playerCount += team2.getPlayerCount();

		String[] playerNames = new String[playerCount];
		String[] playerShipNames = new String[playerCount];
		Faction[] playerFactions = new Faction[playerCount];
		long[] playerShipIds = new long[playerCount];
		long[] playerIds = new long[playerCount];
		long[] playerSpawnLocationEntityIds = new long[playerCount];

		GameInfoInitData data = new GameInfoInitData(gameInfoProvider.getGameInfo(), player.getTeam().getFaction(), playerNames, playerShipNames, playerFactions, playerShipIds, playerIds, playerSpawnLocationEntityIds);

		int i = 0;
		addPlayersData(data, i, team1PlayerCount, team1);

		i = team1PlayerCount;
		addPlayersData(data, i, team2PlayerCount, team2);

		data.setPlayerId(player.getPlayerId());
		return data;
	}

	protected void addPlayersData(GameInfoInitData data, int i, int playerCount, Team team) {
		String[] playerNames = data.getPlayerNames();
		String[] playerShipNames = data.getPlayerShipNames();
		Faction[] playerFactions = data.getPlayerFactions();
		long[] playerShipIds = data.getPlayerShipIds();
		long[] playerIds = data.getPlayerIds();
		long[] playerSpawnLocationEntityIds = data.getPlayerSpawnLocationEntityIds();

		List<Player> players = team.getPlayers();

		for (int j = 0; j < playerCount; i++, j++) {
			Player player = players.get(j);
			long playerId = player.getPlayerId();

			Node ship = player.getShip();
			long shipId = 0l;
			String shipName = null;

			if (ship != null) {
				shipId = EntityHelper.getEntityId(ship);
				shipName = ship.getName();
			}

			playerNames[i] = player.getName();
			playerShipNames[i] = shipName;
			playerFactions[i] = team.getFaction();
			playerShipIds[i] = shipId;
			playerIds[i] = playerId;
			playerSpawnLocationEntityIds[i] = player.getSpawnPointEntityId();
		}
	}
}