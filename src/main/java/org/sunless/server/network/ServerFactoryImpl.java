package org.sunless.server.network;

import java.io.IOException;

import com.google.inject.Singleton;
import com.jme3.network.Network;
import com.jme3.network.Server;

@Singleton
public class ServerFactoryImpl implements ServerFactory {

	@Override
	public Server createServer() throws IOException {
		return Network.createServer(6543);
	}
}