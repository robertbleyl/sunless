package org.sunless.server.network.messagehandler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sunless.shared.control.MoveControl;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.network.messages.toserver.input.MoveShipMessage;

import com.jme3.network.HostedConnection;
import com.jme3.scene.Node;

public class MoveShipMessageHandler extends ServerMessageHandler<MoveShipMessage> {

	private static final Logger log = LoggerFactory.getLogger(MoveShipMessageHandler.class);

	@Override
	public Class<MoveShipMessage> getMessageClass() {
		return MoveShipMessage.class;
	}

	@Override
	public void handleMessage(HostedConnection conn, MoveShipMessage m) {
		int connectionId = conn.getId();
		Player player = playerManager.getPlayer(connectionId);

		if (player != null) {
			Node ship = player.getShip();

			if (ship != null) {
				MoveControl moveControl = ship.getControl(MoveControl.class);

				if (moveControl != null) {
					moveControl.setMoveInfo(m.getMovementInfo());
				} else {
					log.error("The player's " + player.getPlayerId() + " ship does not have a MoveControl!");
				}
			} else {
				log.error("The player " + player.getPlayerId() + " does not have a ship!");
			}
		} else {
			noPlayerForConnection(connectionId);
		}
	}
}