package org.sunless.server.network.messagehandler;

import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.network.messages.toclient.gameplay.ChangePlayerSpawnLocationRequestMessage;

import com.jme3.math.Vector3f;
import com.jme3.network.HostedConnection;
import com.jme3.scene.Spatial;

public class ChangePlayerSpawnLocationRequestMessageHandler extends ServerMessageHandler<ChangePlayerSpawnLocationRequestMessage> {

	private final Logger log = LoggerFactory.getLogger(getClass());

	@Override
	public Class<ChangePlayerSpawnLocationRequestMessage> getMessageClass() {
		return ChangePlayerSpawnLocationRequestMessage.class;
	}

	@Override
	public void handleMessage(HostedConnection source, ChangePlayerSpawnLocationRequestMessage m) {
		Vector3f requestedSpawnLocation = m.getRequestedSpawnLocation();
		Player player = playerManager.getPlayer(source.getId());

		if (player != null) {
			if (requestedSpawnLocation != null) {
				Team team = player.getTeam();

				Set<Spatial> activeSpawnPoints = team.getActiveSpawnPoints();

				if (activeSpawnPoints != null) {
					Optional<Spatial> first = activeSpawnPoints.stream().filter(s -> s.getLocalTranslation().equals(requestedSpawnLocation)).findFirst();

					if (first.isPresent()) {
						Spatial spawn = first.get();
						long spawnId = EntityHelper.getEntityId(spawn);
						player.setSpawnPointEntityId(spawnId);
					} else {
						log.warn("Player " + player.getPlayerId() + " has requested a spawn location (" + requestedSpawnLocation + " that is not present in his team!");
					}
				} else {
					log.error("Team " + team.getTeamId() + " has no activeSpawnPoints!");
				}
			} else {
				log.warn("Player " + player.getPlayerId() + " has requested a null spawn location!");
			}
		} else {
			noPlayerForConnection(source.getId());
		}
	}
}