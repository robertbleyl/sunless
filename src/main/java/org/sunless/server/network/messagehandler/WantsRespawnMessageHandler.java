package org.sunless.server.network.messagehandler;

import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.network.messages.toserver.gameplay.WantsRespawnMessage;

import com.jme3.network.HostedConnection;

public class WantsRespawnMessageHandler extends ServerMessageHandler<WantsRespawnMessage> {

	@Override
	public Class<WantsRespawnMessage> getMessageClass() {
		return WantsRespawnMessage.class;
	}

	@Override
	public void handleMessage(HostedConnection conn, WantsRespawnMessage m) {
		int connectionId = conn.getId();
		Player player = playerManager.getPlayer(connectionId);

		if (player != null) {
			player.setWantsRespawn(true);
		} else {
			noPlayerForConnection(connectionId);
		}
	}
}