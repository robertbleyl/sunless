package org.sunless.server.network.messagehandler;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sunless.server.gameplay.control.BoostControl;
import org.sunless.shared.control.MoveControl;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.json.Faction;
import org.sunless.shared.json.FactionConfig;
import org.sunless.shared.json.ShipConfig;
import org.sunless.shared.network.messages.toclient.gameplay.PlayerShipChangeMessage;
import org.sunless.shared.network.messages.toserver.gameplay.BuyShipRequestMessage;

import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.network.HostedConnection;
import com.jme3.scene.Node;

public class BuyShipRequestMessageHandler extends ServerMessageHandler<BuyShipRequestMessage> {

	private static final Logger log = LoggerFactory.getLogger(BuyShipRequestMessageHandler.class);

	@Override
	public Class<BuyShipRequestMessage> getMessageClass() {
		return BuyShipRequestMessage.class;
	}

	@Override
	public void handleMessage(HostedConnection conn, BuyShipRequestMessage m) {
		int connectionId = conn.getId();
		String shipName = m.getShipName();

		if (StringUtils.isNotBlank(shipName)) {
			Player player = playerManager.getPlayer(connectionId);

			if (player != null) {
				Faction faction = player.getTeam().getFaction();
				FactionConfig factionConfig = factionConfigs.get(faction);
				List<ShipConfig> ships = factionConfig.getShips();

				for (ShipConfig config : ships) {
					if (config.getShipName().equals(shipName)) {
						try {
							Node ship = entityFactory.createShip(shipName);
							player.setShip(ship);

							RigidBodyControl bodyControl = ship.getControl(RigidBodyControl.class);
							ship.addControl(new MoveControl(bodyControl));
							ship.addControl(new BoostControl());

							long playerId = player.getPlayerId();
							ship.setUserData(EntityConstants.player, player);

							long shipId = EntityHelper.getEntityId(ship);
							serverState.broadcast(new PlayerShipChangeMessage(playerId, shipName, shipId));
						} catch (Exception e) {
							throw new RuntimeException("An error occured while loading the ship " + shipName, e);
						}

						return;
					}
				}

				log.error("No ship config was found for shipName " + shipName + "!");
			} else {
				noPlayerForConnection(connectionId);
			}
		} else {
			log.error("The given shipName of the BuyShipRequestMessage was empty!");
		}
	}
}