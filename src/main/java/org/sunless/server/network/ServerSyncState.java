package org.sunless.server.network;

import java.util.Map.Entry;

import org.sunless.server.entity.SyncMessageFactory;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.json.ServerConfig;
import org.sunless.shared.loading.EnqueueHelper;
import org.sunless.shared.network.messages.toclient.sync.AbstractSyncMessage;
import org.sunless.shared.network.sync.AbstractSyncState;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.scene.Spatial;

@Singleton
public class ServerSyncState extends AbstractSyncState {

	// private static final Logger log = LoggerFactory.getLogger(ServerSyncState.class);

	private final ServerState serverState;

	@Inject
	public ServerSyncState(EnqueueHelper enqueueHelper, ServerState serverState, ServerConfig serverConfig) {
		super(enqueueHelper, serverConfig.getNetworkTickRate());
		this.serverState = serverState;
	}

	@Override
	protected void onUpdate(float tpf) {
		syncTimer += tpf;

		if (syncTimer >= syncFrequency) {
			sendSyncMessages();
			syncTimer = 0;
		}
	}

	protected void sendSyncMessages() {
		for (Entry<Long, Spatial> entry : syncEntities.entrySet()) {
			sendSyncMessageForEntity(entry.getValue());
		}
	}

	protected void sendSyncMessageForEntity(Spatial entity) {
		SyncMessageFactory factory = entity.getUserData(EntityConstants.syncMessageFactory);

		if (factory != null) {
			AbstractSyncMessage message = factory.create(entity);
			serverState.broadcast(message);
		}
	}
}