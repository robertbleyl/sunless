package org.sunless.server.network;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.sunless.server.ai.BotFactory;
import org.sunless.server.gameplay.DamageHistory;
import org.sunless.server.gameplay.DamageHistoryEntry;
import org.sunless.server.gameplay.EndTimerControl;
import org.sunless.server.gameplay.ServerPlayersContainer;
import org.sunless.shared.NodeConstants;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.event.EventBus;
import org.sunless.shared.event.config.DebugConfigChangedEvent;
import org.sunless.shared.event.config.DebugConfigChangedHandler;
import org.sunless.shared.event.gameplay.DestroyShipEvent;
import org.sunless.shared.event.gameplay.EntityPropertyChangeEvent;
import org.sunless.shared.event.gameplay.EntityPropertyChangeHandler;
import org.sunless.shared.event.gameplay.PlayerPropertyChangeEvent;
import org.sunless.shared.event.gameplay.PlayerPropertyChangeHandler;
import org.sunless.shared.event.gameplay.RespawnPlayerEvent;
import org.sunless.shared.event.gameplay.RespawnPlayerHandler;
import org.sunless.shared.event.gameplay.WinConditionReachedEvent;
import org.sunless.shared.event.gameplay.WinConditionReachedHandler;
import org.sunless.shared.event.main.EndGameTimeUpdateEvent;
import org.sunless.shared.event.main.EndGameTimeUpdateHandler;
import org.sunless.shared.event.main.GameTimeUpdateEvent;
import org.sunless.shared.event.main.GameTimeUpdateHandler;
import org.sunless.shared.event.main.RemovePlayerEvent;
import org.sunless.shared.event.main.RemovePlayerHandler;
import org.sunless.shared.event.main.RestartGameEvent;
import org.sunless.shared.gameplay.EntityPropertyCauseSync;
import org.sunless.shared.gameplay.EntityPropertyChangeCause;
import org.sunless.shared.gameplay.GameInfoProvider;
import org.sunless.shared.gameplay.WeaponFireCause;
import org.sunless.shared.gameplay.player.Player;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.loading.EnqueueHelper;
import org.sunless.shared.network.messages.MessageRegistry;
import org.sunless.shared.network.messages.toclient.RestartGameMessage;
import org.sunless.shared.network.messages.toclient.config.DebugConfigChangedMessage;
import org.sunless.shared.network.messages.toclient.gameplay.EntityPropertyChangeMessage;
import org.sunless.shared.network.messages.toclient.gameplay.PlayerPropertyChangeMessage;
import org.sunless.shared.network.messages.toclient.gameplay.RespawnPlayerMessage;
import org.sunless.shared.network.messages.toclient.gameplay.SpectatedPlayerUpdateMessage;
import org.sunless.shared.network.messages.toclient.gameplay.WinConditionReachedMessage;
import org.sunless.shared.network.messages.toclient.main.EndTimerUpdateMessage;
import org.sunless.shared.network.messages.toclient.main.GameTimeUpdateMessage;
import org.sunless.shared.network.messages.toclient.main.PlayerNameRequestMessage;
import org.sunless.shared.state.AppStateImpl;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.jme3.network.ConnectionListener;
import com.jme3.network.HostedConnection;
import com.jme3.network.Message;
import com.jme3.network.Server;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;

@Singleton
public class ServerStateImpl extends AppStateImpl implements ServerState, ConnectionListener {

	private final Logger log = LoggerFactory.getLogger(getClass());

	private final EventBus eventBus = EventBus.get();

	private Server server;

	private final GameInfoProvider gameInfoProvider;
	private final ServerPlayersContainer playersContainer;
	private final BotFactory botFactory;
	private final EnqueueHelper enqueueHelper;
	private final ServerFactory serverFactory;

	private ServerMessageListener serverMessageListener;

	private final Map<Long, Long> spectatingPlayerData = new HashMap<>();

	@Inject
	public ServerStateImpl(GameInfoProvider gameInfoProvider, ServerPlayersContainer playersContainer, BotFactory botFactory, EnqueueHelper enqueueHelper, ServerFactory serverFactory, @Named(NodeConstants.ROOT_NODE) Node appRootNode, @Named(NodeConstants.GUI_NODE) Node appGuiNode) throws IOException {
		super(appRootNode, appGuiNode);
		this.gameInfoProvider = gameInfoProvider;
		this.playersContainer = playersContainer;
		this.botFactory = botFactory;
		this.enqueueHelper = enqueueHelper;
		this.serverFactory = serverFactory;

		init();
	}

	private void init() throws IOException {
		MessageRegistry.registerMessages();

		server = serverFactory.createServer();
		server.addConnectionListener(this);

		addEventHandler(RespawnPlayerEvent.TYPE, new RespawnPlayerHandler() {
			@Override
			public void onRespawnPlayer(RespawnPlayerEvent event) {
				respawn(event.getPlayer());
			}
		});

		addEventHandler(EntityPropertyChangeEvent.TYPE, new EntityPropertyChangeHandler() {
			@Override
			public void onChange(EntityPropertyChangeEvent event) {
				handleEntityPropertyChange(event);
			}
		});

		addEventHandler(RemovePlayerEvent.TYPE, new RemovePlayerHandler() {
			@Override
			public void onRemovePlayer(RemovePlayerEvent event) {
				removePlayer(event.getPlayer());
			}
		});

		addEventHandler(PlayerPropertyChangeEvent.TYPE, new PlayerPropertyChangeHandler() {
			@Override
			public void onChange(PlayerPropertyChangeEvent event) {
				server.broadcast(new PlayerPropertyChangeMessage(event.getPlayer().getPlayerId(), event.getPropertyName(), event.getPropertyValue()));
			}
		});

		addEventHandler(WinConditionReachedEvent.TYPE, new WinConditionReachedHandler() {
			@Override
			public void onWinConditionReached(WinConditionReachedEvent event) {
				server.broadcast(new WinConditionReachedMessage(event.getWinnerTeam().getTeamId()));
				rootNode.addControl(new EndTimerControl(5));
			}
		});

		addEventHandler(EndGameTimeUpdateEvent.TYPE, new EndGameTimeUpdateHandler() {
			@Override
			public void onUpdateGameTime(EndGameTimeUpdateEvent event) {
				final int secondsLeft = event.getSecondsLeft();
				server.broadcast(new EndTimerUpdateMessage(secondsLeft));

				if (secondsLeft == 0) {
					server.broadcast(new RestartGameMessage());
					eventBus.fireEvent(new RestartGameEvent());
				}
			}
		});

		addEventHandler(GameTimeUpdateEvent.TYPE, new GameTimeUpdateHandler() {
			@Override
			public void onGameTimeUpdate(GameTimeUpdateEvent event) {
				server.broadcast(new GameTimeUpdateMessage(event.getCurrentTime()));
			}
		});

		addEventHandler(DebugConfigChangedEvent.TYPE, new DebugConfigChangedHandler() {
			@Override
			public void onChanged(DebugConfigChangedEvent event) {
				server.broadcast(new DebugConfigChangedMessage(event.getDebugCommandKey(), event.getArguments()));
			}
		});
	}

	private void respawn(Player player) {
		player.setRespawned();

		final Node ship = player.getShip();
		final int maxHitPoints = ship.getUserData(EntityConstants.maxHitPoints);
		EntityHelper.changeProperty(ship, EntityConstants.hitPoints, maxHitPoints, null);

		final int maxShields = ship.getUserData(EntityConstants.maxShields);
		EntityHelper.changeProperty(ship, EntityConstants.shields, maxShields, null);

		broadcast(new RespawnPlayerMessage(player.getPlayerId()));
	}

	private void removePlayer(Player player) {
		final Team team = player.getTeam();
		team.removePlayer(player);
		player.detachPlayerControls();
	}

	private void handleEntityPropertyChange(EntityPropertyChangeEvent event) {
		final Spatial entity = event.getEntity();
		final long entityId = event.getEntityId();
		final String propertyName = event.getPropertyName();
		final Object propertyValue = event.getPropertyValue();
		final EntityPropertyChangeCause cause = event.getCause();

		EntityPropertyCauseSync createCauseForMessage = null;

		if (cause != null) {
			createCauseForMessage = cause.createCauseForMessage();
		}

		server.broadcast(new EntityPropertyChangeMessage(entityId, propertyName, propertyValue, createCauseForMessage));

		if (propertyName.equals(EntityConstants.hitPoints)) {
			final int currentHitPoints = (int)propertyValue;
			final int oldHitPoints = (int)event.getOldPropertyValue();

			if (oldHitPoints < currentHitPoints) {
				updateDamageHistory(entity, currentHitPoints - oldHitPoints);
			}

			if (oldHitPoints > 0 && currentHitPoints <= 0) {
				eventBus.fireEvent(new DestroyShipEvent(entity));

				final Player player = entity.getUserData(EntityConstants.player);

				if (player != null) {
					player.setWantsRespawn(player.isBot());
					player.setAlive(false);
					player.addDeath();

					if (cause != null && cause instanceof WeaponFireCause) {
						final WeaponFireCause weaponFireCause = (WeaponFireCause)cause;
						final Spatial firingShip = weaponFireCause.getWeaponFire().getUserData(EntityConstants.weaponFireFiringShip);

						final Player firingPlayer = firingShip.getUserData(EntityConstants.player);

						if (firingPlayer != null) {
							firingPlayer.addKill();
						}
					}
				}
			}
		} else if (propertyName.equals(EntityConstants.shields)) {
			final int currentShields = (int)propertyValue;
			final int oldShields = (int)event.getOldPropertyValue();

			if (oldShields < currentShields) {
				updateDamageHistory(entity, currentShields - oldShields);
			}
		}
	}

	private void updateDamageHistory(final Spatial entity, final int diff) {
		enqueueHelper.enqueue(new Callable<Void>() {
			@Override
			public Void call() throws Exception {
				final DamageHistory damageHistory = entity.getUserData(EntityConstants.damageHistory);

				int d = diff;

				while (!damageHistory.isEmpty()) {
					final DamageHistoryEntry first = damageHistory.getFirst();

					final int damage = first.getDamage();

					if (damage > d) {
						first.setDamage(damage - d);
						break;
					}

					d -= damage;
					damageHistory.removeFirst();
				}

				return null;
			}
		});
	}

	@Override
	public void startServer() {
		server.start();
	}

	@Override
	public void connectionAdded(Server server, HostedConnection conn) {
		log.info("New player connecting...");
		conn.send(new PlayerNameRequestMessage());
	}

	@Override
	public void connectionRemoved(Server server, HostedConnection conn) {
		final int connectionId = conn.getId();
		final Player player = playersContainer.removePlayer(connectionId);

		if (player != null) {
			synchronized (spectatingPlayerData) {
				spectatingPlayerData.remove(player.getPlayerId());
			}

			enqueueHelper.enqueue(new Callable<Void>() {
				@Override
				public Void call() throws Exception {
					eventBus.fireEvent(new RemovePlayerEvent(player));

					if (gameInfoProvider.getGameInfo().isEnableBots()) {
						final Team team = player.getTeam();

						try {
							botFactory.createBot("Bot" + team.getPlayerCount(), team);
						} catch (final Exception e) {
							e.printStackTrace();
						}
					}

					return null;
				}
			});
		} else {
			log.warn("No player found for connection " + connectionId + "!");
		}
	}

	@Override
	public void setServerMessageListener(ServerMessageListener serverMessageListener) {
		if (this.serverMessageListener != null) {
			server.removeMessageListener(serverMessageListener);
		}

		this.serverMessageListener = serverMessageListener;
		server.addMessageListener(serverMessageListener);
	}

	@Override
	public void shutDown() {
		if (server.isRunning()) {
			server.close();
		}
	}

	@Override
	public void broadcast(Message message) {
		server.broadcast(message);
	}

	@Override
	public void sendPlayerNameRequestMessages() {
		Collection<HostedConnection> connections = server.getConnections();

		for (HostedConnection conn : connections) {
			conn.send(new PlayerNameRequestMessage());
		}
	}

	@Override
	public void update(float tpf) {
		super.update(tpf);

		synchronized (spectatingPlayerData) {
			for (Entry<Long, Long> entry : spectatingPlayerData.entrySet()) {
				Long playerId = entry.getKey();
				Long spectatedPlayerId = entry.getValue();

				Integer connectionId = playersContainer.getConnectionId(playerId);

				if (connectionId != null) {
					HostedConnection connection = server.getConnection(connectionId);
					Player player = playersContainer.getPlayer(connectionId);

					Player spectatedPlayer = player.getTeam().getPlayer(spectatedPlayerId);

					if (spectatedPlayer != null) {
						Player target = spectatedPlayer.getTarget();
						long targetPlayerId = 0L;

						if (target != null) {
							targetPlayerId = target.getPlayerId();
						}

						connection.send(new SpectatedPlayerUpdateMessage(spectatedPlayerId, targetPlayerId));
					}
				} else {
					log.error("No connection found for player " + playerId + "!");
				}
			}
		}
	}

	@Override
	public void changeSpectatedPlayer(long playerId, long spectatedPlayerId) {
		spectatingPlayerData.put(playerId, spectatedPlayerId);
	}
}