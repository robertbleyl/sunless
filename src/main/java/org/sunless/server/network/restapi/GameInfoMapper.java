package org.sunless.server.network.restapi;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.sunless.shared.gameplay.GameInfo;

@Mapper
public interface GameInfoMapper {

	GameInfoMapper INSTANCE = Mappers.getMapper(GameInfoMapper.class);

	GameInfoResponseJSON newGameInfo(GameInfo request);
}