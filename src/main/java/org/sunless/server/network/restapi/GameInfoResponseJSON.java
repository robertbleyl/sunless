package org.sunless.server.network.restapi;

import java.io.Serializable;

import org.sunless.server.ai.AILevel;
import org.sunless.shared.json.GameMode;

public class GameInfoResponseJSON implements Serializable {

	private static final long serialVersionUID = 3295448694716922771L;

	private String mapName;
	private int maxPlayers;
	private long initMoney;
	private GameMode gameMode;
	private int serverTickRate;
	private int gameTime;
	private boolean enableDebug;
	private boolean enableBots;
	private AILevel aiLevel;
	private float spawnDelay;

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public int getMaxPlayers() {
		return maxPlayers;
	}

	public void setMaxPlayers(int maxPlayers) {
		this.maxPlayers = maxPlayers;
	}

	public long getInitMoney() {
		return initMoney;
	}

	public void setInitMoney(long initMoney) {
		this.initMoney = initMoney;
	}

	public GameMode getGameMode() {
		return gameMode;
	}

	public void setGameMode(GameMode gameMode) {
		this.gameMode = gameMode;
	}

	public int getServerTickRate() {
		return serverTickRate;
	}

	public void setServerTickRate(int serverTickRate) {
		this.serverTickRate = serverTickRate;
	}

	public int getGameTime() {
		return gameTime;
	}

	public void setGameTime(int gameTime) {
		this.gameTime = gameTime;
	}

	public boolean isEnableDebug() {
		return enableDebug;
	}

	public void setEnableDebug(boolean enableDebug) {
		this.enableDebug = enableDebug;
	}

	public boolean isEnableBots() {
		return enableBots;
	}

	public void setEnableBots(boolean enableBots) {
		this.enableBots = enableBots;
	}

	public AILevel getAiLevel() {
		return aiLevel;
	}

	public void setAiLevel(AILevel aiLevel) {
		this.aiLevel = aiLevel;
	}

	public float getSpawnDelay() {
		return spawnDelay;
	}

	public void setSpawnDelay(float spawnDelay) {
		this.spawnDelay = spawnDelay;
	}
}