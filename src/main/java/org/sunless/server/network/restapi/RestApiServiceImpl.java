package org.sunless.server.network.restapi;

import org.sunless.shared.gameplay.GameInfoProvider;

import com.google.inject.Inject;
import com.google.inject.Singleton;

import io.javalin.Context;
import io.javalin.Javalin;

@Singleton
public class RestApiServiceImpl implements RestApiService {

	private final GameInfoProvider gameInfoProvider;
	private final Javalin javalin;
	private final GameInfoMapper gameInfoMapper;

	@Inject
	public RestApiServiceImpl(GameInfoProvider gameInfoProvider, Javalin javalin, GameInfoMapper gameInfoMapper) {
		this.gameInfoProvider = gameInfoProvider;
		this.javalin = javalin;
		this.gameInfoMapper = gameInfoMapper;

		init();
	}

	private void init() {
		javalin.get("api/game-info", this::getGameInfo);
	}

	public void getGameInfo(Context context) {
		context.json(gameInfoMapper.newGameInfo(gameInfoProvider.getGameInfo()));
	}
}