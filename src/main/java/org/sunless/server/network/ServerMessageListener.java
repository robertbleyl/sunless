package org.sunless.server.network;

import org.sunless.server.network.messagehandler.ServerMessageHandler;
import org.sunless.shared.network.AbstractMessageListener;

import com.jme3.network.HostedConnection;

@SuppressWarnings("rawtypes")
public class ServerMessageListener extends AbstractMessageListener<HostedConnection, ServerMessageHandler> {

}