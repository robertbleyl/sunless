package org.sunless.server.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface ShootHandler extends EventHandler {
	
	void onShoot(ShootEvent event);
}