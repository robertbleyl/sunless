package org.sunless.server.event.gameplay;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;
import org.sunless.shared.gameplay.player.Player;

import com.jme3.math.Vector3f;

public class ShootEvent extends Event<ShootHandler> {

	public static final EventType<ShootHandler> TYPE = new EventType<>();

	private final Vector3f direction;
	private final Player player;
	private final String weaponSlotsContainerName;

	public ShootEvent(Vector3f direction, Player player, String weaponSlotsContainerName) {
		this.direction = direction;
		this.player = player;
		this.weaponSlotsContainerName = weaponSlotsContainerName;
	}

	@Override
	public EventType<ShootHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(ShootHandler handler) {
		handler.onShoot(this);
	}

	public Vector3f getDirection() {
		return direction;
	}

	public Player getPlayer() {
		return player;
	}

	public String getWeaponSlotsContainerName() {
		return weaponSlotsContainerName;
	}
}