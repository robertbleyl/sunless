package org.sunless.server.event.gameplay;

import org.sunless.shared.event.Event;
import org.sunless.shared.event.EventType;

import com.jme3.scene.Spatial;

public class WeaponFireCollisionPredictedEvent extends Event<WeaponFireCollisionPredictedHandler> {

	public static final EventType<WeaponFireCollisionPredictedHandler> TYPE = new EventType<>();

	private final Spatial a;
	private final Spatial b;

	public WeaponFireCollisionPredictedEvent(Spatial a, Spatial b) {
		this.a = a;
		this.b = b;
	}

	@Override
	public EventType<WeaponFireCollisionPredictedHandler> getType() {
		return TYPE;
	}

	@Override
	public void callHandler(WeaponFireCollisionPredictedHandler handler) {
		handler.onCollisionPredicted(this);
	}

	public Spatial getA() {
		return a;
	}

	public Spatial getB() {
		return b;
	}
}