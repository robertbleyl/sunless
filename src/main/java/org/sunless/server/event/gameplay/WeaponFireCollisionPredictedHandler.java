package org.sunless.server.event.gameplay;

import org.sunless.shared.event.EventHandler;

public interface WeaponFireCollisionPredictedHandler extends EventHandler {

	void onCollisionPredicted(WeaponFireCollisionPredictedEvent event);
}