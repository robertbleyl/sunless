package org.sunless.server.loading;

import java.util.List;

import org.sunless.server.ai.BotFactory;
import org.sunless.server.gameplay.mode.ServerMotherShipLoadingStepFactory;
import org.sunless.shared.gameplay.GameInfo;
import org.sunless.shared.gameplay.mode.MotherShipWinCondition;
import org.sunless.shared.gameplay.mode.TimeWinCondition;
import org.sunless.shared.gameplay.player.Team;
import org.sunless.shared.loading.GameModeLoadingStepFactory;
import org.sunless.shared.loading.LoadingStep;
import org.sunless.shared.loading.LoadingStepBuildRequest;
import org.sunless.shared.loading.LoadingStepsBuilder;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class ServerLoadingStepsBuilder extends LoadingStepsBuilder {

	private BotFactory botFactory;

	@Override
	protected void addSteps(List<LoadingStep> steps, LoadingStepBuildRequest request) {
		if (gameInfoProvider.getGameInfo().isEnableBots()) {
			addTeamBotLoadingSteps(true, steps);
			addTeamBotLoadingSteps(false, steps);
		}

		steps.add(new LoadingStep(5f, false) {
			@Override
			public void load() throws Exception {
				createWinCondition();
			}
		});
	}

	private void addTeamBotLoadingSteps(final boolean team1, List<LoadingStep> steps) {
		int maxPlayersForTeam = gameInfoProvider.getGameInfo().getMaxPlayers() / 2;

		for (int i = 0; i < maxPlayersForTeam; i++) {
			final int a = i;

			steps.add(new LoadingStep(5f, true) {
				@Override
				public void load() throws Exception {
					Team team = team1 ? teamsContainer.getTeam1() : teamsContainer.getTeam2();
					initBot(a, team);
				}
			});
		}
	}

	private void initBot(int i, Team team) throws Exception {
		botFactory.createBot("Bot" + i, team);
	}

	private void createWinCondition() {
		GameInfo gameInfo = gameInfoProvider.getGameInfo();

		switch (gameInfo.getGameMode()) {
			case CONQUEST:
				new TimeWinCondition(teamsContainer, gameInfo);
				break;
			case MOTHER_SHIP:
				new MotherShipWinCondition();
				new TimeWinCondition(teamsContainer, gameInfo);
				break;
			case TEAM_DEATH_MATCH:
				new TimeWinCondition(teamsContainer, gameInfo);
				break;
		}
	}

	@Override
	protected GameModeLoadingStepFactory createMotherShipLoadingStepFactory(LoadingStepBuildRequest request) {
		GameModeLoadingStepFactory factory = new ServerMotherShipLoadingStepFactory(request.getMapData(), entityFactory, teamsContainer, factionConfigs, entityAttacher, gameInfoProvider.getGameInfo());
		return factory;
	}

	@Inject
	public void setBotFactory(BotFactory botFactory) {
		this.botFactory = botFactory;
	}
}