package org.sunless.server;

import org.sunless.shared.AbstractGameApplication;
import org.sunless.shared.event.EventBus;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Stage;

public class ServerApp extends AbstractGameApplication {

	// private static final Logger log = LoggerFactory.getLogger(ServerApp.class);

	private final EventBus eventBus = EventBus.get();

	private Server server;

	public ServerApp() {
		super();
	}

	@Override
	public void simpleInitApp() {
		try {
			Injector injector = Guice.createInjector(Stage.PRODUCTION, new ServerModule(this));

			Server server = injector.getInstance(Server.class);
			init(server);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void fullRestart() throws Exception {
		server.cleanupActiveGame();
		eventBus.clear();

		stateManager.cleanup();
		assetManager.clearAssetEventListeners();
		rootNode.detachAllChildren();
		guiNode.detachAllChildren();

		simpleInitApp();
	}

	public void init(Server server) {
		this.server = server;
		server.init();
	}

	@Override
	public void stop() {
		super.stop();
		server.stopServer();
	}
}