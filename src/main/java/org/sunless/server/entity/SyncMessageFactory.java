package org.sunless.server.entity;

import org.sunless.shared.network.messages.toclient.sync.AbstractSyncMessage;

import com.jme3.export.Savable;
import com.jme3.scene.Spatial;

public interface SyncMessageFactory extends Savable {
	
	AbstractSyncMessage create(Spatial entity);
}