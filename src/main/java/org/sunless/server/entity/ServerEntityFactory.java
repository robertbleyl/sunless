package org.sunless.server.entity;

import org.sunless.server.gameplay.DamageHistory;
import org.sunless.server.gameplay.control.ControlPointControl;
import org.sunless.server.gameplay.control.ShipControl;
import org.sunless.server.gameplay.control.WeaponFireCCDControl;
import org.sunless.shared.entity.EntityConstants;
import org.sunless.shared.entity.EntityFactoryImpl;
import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.gameplay.GameInfoProvider;
import org.sunless.shared.gameplay.weapon.WeaponFireCreationData;
import org.sunless.shared.json.EntityProperties;
import org.sunless.shared.json.JsonHelper;
import org.sunless.shared.json.ServerConfig;
import org.sunless.shared.json.WorldObjectData;
import org.sunless.shared.util.FileConstants;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.jme3.scene.Node;

@Singleton
public class ServerEntityFactory extends EntityFactoryImpl {

	@Inject
	private GameInfoProvider gameInfoProvider;

	@Inject
	private ServerConfig serverConfig;

	@Override
	public Node createObject(WorldObjectData object) throws Exception {
		Node node = super.createObject(object);

		String controlPointType = node.getUserData(EntityConstants.controlPointType);

		if (controlPointType != null) {
			node.addControl(new ControlPointControl(teamsContainer.getTeam1(), teamsContainer.getTeam2()));
		}

		String name = object.getName();
		String propFilePath = FileConstants.MODELS_PATH_COMPLETE + name + FileConstants.MODEL_PROPERTY_FILE;
		EntityProperties properties = JsonHelper.get().toPOJO(propFilePath, EntityProperties.class);

		if (properties != null) {
			long entityId = EntityHelper.getEntityId(node);
			gameInfoProvider.getGameInfo().getObjectIds().put(node.getLocalTranslation(), entityId);
		}

		return node;
	}

	@Override
	protected void prepareMotherShip(EntityProperties properties, Node motherShip) {
		super.prepareMotherShip(properties, motherShip);
		motherShip.addControl(new ShipControl());
	}

	@Override
	public Node createShip(String fileName) throws Exception {
		Node ship = super.createShip(fileName);

		ship.setUserData(EntityConstants.damageHistory, new DamageHistory());
		ship.setUserData(EntityConstants.syncMessageFactory, new ShipSyncMessageFactory());
		ship.addControl(new ShipControl());

		return ship;
	}

	@Override
	public Node createWeaponFire(WeaponFireCreationData data) throws Exception {
		Node weaponFire = super.createWeaponFire(data);

		weaponFire.addControl(new WeaponFireCCDControl(serverConfig.getPhysicsTickRate()));

		return weaponFire;
	}
}