package org.sunless.server.entity;

import java.io.IOException;

import org.sunless.shared.entity.EntityHelper;
import org.sunless.shared.network.messages.toclient.sync.AbstractSyncMessage;
import org.sunless.shared.network.messages.toclient.sync.WeaponFireSyncMessage;

import com.jme3.export.JmeExporter;
import com.jme3.export.JmeImporter;
import com.jme3.scene.Spatial;

public class WeaponFireSyncMessageFactory implements SyncMessageFactory {
	
	@Override
	public AbstractSyncMessage create(Spatial entity) {
		return new WeaponFireSyncMessage(EntityHelper.getEntityId(entity), entity.getLocalTranslation());
	}
	
	@Override
	public void write(JmeExporter ex) throws IOException {
		
	}
	
	@Override
	public void read(JmeImporter im) throws IOException {
		
	}
}