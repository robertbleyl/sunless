package org.sunless.server.entity;

import org.sunless.shared.entity.EntityIdProvider;

import com.google.inject.Singleton;

@Singleton
public class ServerEntityIdProvider implements EntityIdProvider {

	private long lastId;

	@Override
	public long getNextId() {
		lastId++;
		return lastId;
	}
}